#define ROOTDEVICES_IMPL_SOURCE
#define WANT_SNEKS_FILE_LABELS
#include <stdlib.h>
#include <stdint.h>
#include <alloca.h>
#include <errno.h>
#include <muidl.h>
#include <sys/sysmacros.h>
#include <ccan/htable/htable.h>
#include <ccan/str/str.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <l4/ipc.h>
#include <sneks/hash.h>
#include <sneks/devno.h>
#include <sneks/cookie.h>
#include <sneks/api/file-defs.h>
#include "defs.h"
#include "root-impl-defs.h"

struct dev_entry {
	short type, major, minor, timeouts;
	L4_ThreadId_t server;
	char name[];
};

static size_t rehash_entry_by_devno(const void *key, void *priv);
static size_t rehash_entry_by_name(const void *key, void *priv);

L4_ThreadId_t devices_tid;

static struct htable devno_ht = HTABLE_INITIALIZER(devno_ht, &rehash_entry_by_devno, NULL),
	devname_ht = HTABLE_INITIALIZER(devname_ht, &rehash_entry_by_name, NULL);

static size_t rehash_entry_by_devno(const void *key, void *priv) {
	const struct dev_entry *ent = key;
	return int_hash(ent->major << 15 | ent->minor);
}

static size_t rehash_entry_by_name(const void *key, void *priv) {
	const struct dev_entry *ent = key;
	return hash_string(ent->name);
}

static bool cmp_devno(const void *cand, void *key) {
	const struct dev_entry *c = cand, *k = key;
	return c->type == k->type && c->major == k->major && c->minor == k->minor;
}

static struct dev_entry *get_entry(int type, int major, int minor) {
	struct dev_entry k = { .type = type, .major = major, .minor = minor };
	return htable_get(&devno_ht, rehash_entry_by_devno(&k, NULL), &cmp_devno, &k);
}

static int resolve_device(char *sbuf, size_t sbuflen, unsigned object)
{
	/* TODO: load from file, use htable */
	const char *serv;
	switch(object) {
		default: return -ENODEV;
		case DEV_OBJECT(2, 1, 3): /* null */
		case DEV_OBJECT(2, 1, 5): /* zero */
		case DEV_OBJECT(2, 1, 7): /* full */
			serv = "nullserv";
			break;
	}
	return snprintf(sbuf, sbuflen, "/initrd/lib/sneks-0.0p0/%s", serv); /* TODO: remove /initrd */
}

static bool propagate_devnode_open(bool *live_p, struct dev_entry *cand, unsigned object, L4_Word_t cookie, int flags)
{
	L4_MsgTag_t tag = { .X.label = SNEKS_FILE_OPEN_LABEL, .X.u = 4 };
	L4_Set_Propagation(&tag);
	L4_Set_VirtualSender(muidl_get_sender());
	L4_LoadMR(0, tag.raw);
	L4_LoadMR(1, SNEKS_FILE_OPEN_SUBLABEL);
	L4_LoadMR(2, object);
	L4_LoadMR(3, cookie);
	L4_LoadMR(4, flags);
	tag = L4_Send_Timeout(cand->server, L4_TimePeriod(COOKIE_PERIOD_US * 2));
	if(L4_IpcSucceeded(tag)) return true;
	bool dead = false;
	if(L4_ErrorCode() == 2) {
		/* send-phase timeout */
		if(++cand->timeouts >= 5) dead = true;
	} else if(L4_ErrorCode() == 4) {
		/* "non-existing partner" */
		dead = true;
	} else {
		dead = true;
	}
	if(!dead) *live_p = true;
	else {
		htable_del(&devno_ht, rehash_entry_by_devno(cand, NULL), cand);
		htable_del(&devname_ht, rehash_entry_by_name(cand, NULL), cand);
		free(cand);
	}
	return false;
}

static int devices_open(int *handle_p, unsigned object, L4_Word_t cookie, int flags)
{
	int type = object >> 30;
	if(type != 2 && type != 0) return -ENODEV;
	if(!validate_cookie(cookie, &device_cookie_key, L4_SystemClock(), object, pidof_NP(muidl_get_sender()))) {
		/* nfg */
		return -EINVAL;
	}
	cookie = 0x12345678;	/* replace w/ obvious dummy */

	bool live = false;
	struct dev_entry *ent = get_entry(type, major(object), minor(object));
	if(ent != NULL && propagate_devnode_open(&live, ent, object, cookie, flags)) {
		muidl_raise_no_reply();
		return 0;
	}
	/* lookup by name. */
	char sbuf[64], *spawnpath = sbuf;
	int n = resolve_device(sbuf, sizeof sbuf, object);
	if(n > sizeof sbuf - 1) {
		/* (GCC has a warning about alloca parameter being too large, when
		 * this certainly isn't. fuck them; remove the cast once sense has
		 * returned.)
		 */
		spawnpath = alloca((unsigned short)n + 1);
		int m = resolve_device(spawnpath, n + 1, object);
		assert(m <= n);
		n = m;
	}
	if(n < 0) return n;
	size_t name_hash = hash_string(spawnpath);
	struct htable_iter it;
	for(struct dev_entry *cand = htable_firstval(&devname_ht, &it, name_hash); cand != NULL; cand = htable_nextval(&devname_ht, &it, name_hash)) {
		if(!streq(cand->name, spawnpath)) continue;
		if(propagate_devnode_open(&live, cand, object, cookie, flags)) {
			muidl_raise_no_reply();
			return 0;
		}
	}
	if(live) {
		/* there were candidate devices but none of them responded, which
		 * means they must be wedged somehow as devices are wont to do.
		 * suggest the client resolve its path again and give it another
		 * whirl, since the cookie would have aged off already anyway.
		 * NB: this logic is completely untested and seems fanciful.
		 */
		return -ETIMEDOUT;
	}
	/* not found; start anew. */
	assert(n == strlen(spawnpath));
	if(ent = malloc(sizeof *ent + n + 1), ent == NULL) return -ENOMEM;
	*ent = (struct dev_entry){ .type = type, .minor = minor(object), .major = major(object), .server = spawn_systask(0, spawnpath, NULL) };
	if(L4_IsNilThread(ent->server)) { free(ent); return -ENODEV; }
	memcpy(ent->name, spawnpath, n + 1);
	size_t devno_hash = rehash_entry_by_devno(ent, NULL);
	assert(name_hash == rehash_entry_by_name(ent, NULL));
	bool ok = htable_add(&devno_ht, devno_hash, ent);
	if(ok) ok = htable_add(&devname_ht, name_hash, ent);
	if(!ok) { htable_del(&devno_ht, devno_hash, ent); free(ent); return -ENOMEM; }
	if(propagate_devnode_open(&live, ent, object, cookie, flags)) {
		muidl_raise_no_reply();
		return 0;
	} else {
		return -ETIMEDOUT;
	}
}

static int impl_devnoctl(int kind, dev_t major, dev_t minor, L4_Word_t *server_raw_p)
{
	struct dev_entry *ent = get_entry(kind, major, minor);
	if(*server_raw_p == L4_nilthread.raw) {
		/* deletion */
		if(ent != NULL) {
			htable_del(&devno_ht, rehash_entry_by_devno(ent, NULL), ent);
			if(ent->name[0] != '\0') htable_del(&devname_ht, rehash_entry_by_name(ent, NULL), ent);
			free(ent);
		}
	} else if(*server_raw_p == L4_anythread.raw) {
		/* query */
		*server_raw_p = ent != NULL ? ent->server.raw : L4_nilthread.raw;
	} else {
		/* association */
		if(ent = malloc(sizeof *ent + 1), ent == NULL) return -ENOMEM;
		*ent = (struct dev_entry){ .major = major, .minor = minor, .server.raw = *server_raw_p };
		switch(kind) {
			case 'b': ent->type = 0; break;
			case 'c': ent->type = 2; break;
			default: free(ent); return -EINVAL;
		}
		ent->name[0] = '\0';
		if(!htable_add(&devno_ht, rehash_entry_by_devno(ent, NULL), ent)) { free(ent); return -ENOMEM; }
	}
	return 0;
}

static int enosys() { return -ENOSYS; }

int devices_loop(void *param_ptr)
{
	devices_tid = L4_MyGlobalId();
	static const struct root_devices_vtable vtab = {
		/* Sneks::File */
		.open = &devices_open, .seek = &enosys,
		/* Sneks::DeviceRegistry */
		.devnoctl = &impl_devnoctl,
	};
	for(;;) {
		L4_Word_t st = _muidl_root_devices_dispatch(&vtab);
		if(st == MUIDL_UNKNOWN_LABEL) {
			/* fie! */
			L4_LoadMR(0, (L4_MsgTag_t){ .X.label = 1, .X.u = 1 }.raw);
			L4_LoadMR(1, ENOSYS);
			L4_Reply(muidl_get_sender());
		} else if(st != 0 && !MUIDL_IS_L4_ERROR(st)) {
			printf("%s: dispatch status %#lx (last tag %#lx)\n", __func__, st, muidl_get_tag().raw);
		}
	}
}
