#include <assert.h>
#include <sys/types.h>
#include <sys/sysmacros.h>
#include <sneks/devno.h>

unsigned _sneks_major(dev_t dev) {
	return (dev >> 15) & 0x7fff;
}

unsigned _sneks_minor(dev_t dev) {
	return dev & 0x7fff;
}

dev_t _sneks_makedev(unsigned major, unsigned minor) {
	dev_t ret = (major & 0x7fff) << 15 | (minor & 0x7fff);
	assert(ret == DEV_OBJECT(0, (dev_t)major, (dev_t)minor));
	return ret;
}
