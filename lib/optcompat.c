/* satisfy CCAN opt's deps unimplemented elsewhere */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <limits.h>
#include <errno.h>

double strtod(const char *nptr, char **endptr) {
	fprintf(stderr, "%s: not implemented!\n", __func__);
	abort();
}

int ioctl(int fd, unsigned long request, ...) {
	errno = ENOSYS;
	return -1;
}

/* CCAN, i... */
int sprintf(char *str, const char *fmt, ...)
{
	va_list al;
	va_start(al, fmt);
	int n = vsnprintf(str, INT_MAX, fmt, al);
	va_end(al);
	return n;
}
