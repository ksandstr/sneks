/* setting stdio_portable.c up to print thru con_putstr(). that interface is
 * old and lame and should be fixed to take a length parameter.
 */
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sneks/console.h>

ssize_t sneks_con_write(void *cookie, const char *buf, size_t size) {
	char copy[size + 1]; memcpy(copy, buf, size); copy[size] = '\0';
	con_putstr(copy);
	return size;
}

int sneks_setup_console_stdio(void)
{
	FILE *out = fopencookie(NULL, "wb", (cookie_io_functions_t){ .write = &sneks_con_write });
	if(out == NULL) return -ENOMEM;
	if(stdin != NULL) fclose(stdin);
	if(stdout != NULL) fclose(stdout);
	if(stderr != NULL) fclose(stderr);
	stdin = out; stdout = out; stderr = out;
	return 0;
}
