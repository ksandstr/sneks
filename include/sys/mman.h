#ifndef _SYS_MMAN_H
#define _SYS_MMAN_H

#include <sys/types.h>

#define PROT_NONE	0
#define PROT_READ	1
#define PROT_WRITE	2
#define PROT_EXEC	4

#define MAP_SHARED	1
#define MAP_PRIVATE	2

#define MAP_FILE	0
#define MAP_FAILED (void *)-1
#define MAP_FIXED 0x10
#define MAP_ANONYMOUS 0x20
#define MAP_ANON MAP_ANONYMOUS

extern void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
extern int munmap(void *addr, size_t length);

extern int mlock(const void *, size_t);
extern int munlock(const void *, size_t);

#endif
