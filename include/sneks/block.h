#ifndef _SNEKS_BLOCK_H
#define _SNEKS_BLOCK_H

#include <stdint.h>
#include <sys/types.h>

struct blk_class {
	int priv_size, sec_size; /* TODO: change to sec_size_log2 for faster u64 shifts */
	const char *name;
	/* meta */
	int (*close)(void *priv);
	void (*destroy)(void *priv); /* optional */
	/* synchronous operation */
	int (*read)(void *buf, uint64_t first_sector, int count, void *priv);
};

struct blk_inst;
typedef struct blk_inst blk_t;

extern blk_t *blk_new(struct blk_class *, off_t);
extern void blk_undo_new(blk_t *);
extern const struct blk_class *blk_class_of(blk_t *);
extern void *blk_priv_of(blk_t *);
extern blk_t *blk_of_priv(void *);
extern int blk_publish(blk_t *, dev_t);
extern void blk_detach(blk_t *);

#endif
