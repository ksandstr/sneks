#ifndef _SNEKS_DEVNO_H
#define _SNEKS_DEVNO_H

#include <l4/types.h>
#include <sys/types.h>

/* not w/ makedev() so it's case-label compatible, doesn't need <sys/sysmacros.h>. */
#define DEV_OBJECT(t, major, minor) ((((t) & 3) << 30) | ((major) & 0x7fff) << 15 | ((minor) & 0x7fff))

extern int devnoctl(char kind, dev_t major, dev_t minor, L4_ThreadId_t tid);

#endif
