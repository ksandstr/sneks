/* pagecache for tracking pages backed in a regular file on a filesystem. */
#include <stdio.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <nbsl.h>
#include <epoch.h>
#include <sys/mman.h>
#include <ccan/container_of/container_of.h>
#include <sneks/hash.h>
#include <sneks/systask.h>
#include <sneks/api/io-defs.h>
#include "space.h"
#include "defs.h"

static uint32_t pc_salt[4];

struct nbsl *pc_buckets = NULL;
unsigned n_pc_buckets_log2;

#ifdef DEBUG_ME_HARDER
#include <sneks/invariant.h>

static int cmp_vp_by_status(const void *a, const void *b) {
	return (long)(*(const struct vp **)a)->status - (long)(*(const struct vp **)b)->status;
}

bool pc_invariants(INVCTX_ARG, struct vp *const *all_vps, size_t n_all_vps)
{
	struct vp **vps = malloc(sizeof *vps * n_all_vps);
	memcpy(vps, all_vps, sizeof *vps * n_all_vps);
	qsort(vps, n_all_vps, sizeof *vps, &cmp_vp_by_status);
	inv_ok1(n_all_vps < 2 || vps[0]->status <= vps[1]->status);

	for(int i=0; i < n_pc_buckets; i++) {
		struct nbsl *list = &pc_buckets[i];
		struct nbsl_iter it;
		for(struct nbsl_node *cur = nbsl_first(list, &it);
			cur != NULL;
			cur = nbsl_next(list, &it))
		{
			struct pl *link = container_of(cur, struct pl, nn);
			if(atomic_load(&link->status) == PL_DEAD) continue;
			size_t hash = int_hash(link->page_num);
			inv_push("link=%p: ->page_num=%#x [pl]->owner=%p",
				link, link->page_num, pl2pp(link)->owner);

			struct vp key = { .status = link->page_num }, *keyptr = &key,
				**vpp = bsearch(&keyptr, vps, n_all_vps, sizeof *vps,
					&cmp_vp_by_status);
			inv_imply1(vpp == NULL, pl2pp(link)->owner == NULL);
			inv_imply1(vpp == NULL, !has_shares(hash, link->page_num, 1));
			inv_imply1(vpp != NULL,
				pl2pp(link)->owner == *vpp
					|| has_shares(hash, link->page_num, 1));
			if(vpp != NULL) {
				/* wind it back. */
				struct vp *virt = *vpp;
				while(vpp > vps && vpp[-1]->status == virt->status) {
					virt = *--vpp;
				}

				/* then iterate them all. */
				int nth = 0;
				do {
					inv_log("virt[%d]=%p, ->vaddr=%#x, ->status=%#x",
						nth++, virt, virt->vaddr, virt->status);
					inv_ok1(!VP_IS_ANON(virt));
				} while(vpp < vps + n_all_vps
					&& (virt = *++vpp, vpp[-1]->status == virt->status));
			}

			inv_pop();
		}
	}

	free(vps);
	return true;

inv_fail:
	free(vps);
	return false;
}
#endif

/* shooting a fly with a cannon, here */
size_t hash_cached_page(uint32_t a, uint32_t b, uint32_t c) {
	size_t first = bob96bitmix(pc_salt[0], pc_salt[1], a), second = bob96bitmix(pc_salt[2], pc_salt[3], b);
	return bob96bitmix(first, second, c);
}

/* insert @page. @top is the pointer from that find_cached_page() result which
 * didn't find a copy of @page, or NULL. return @page if a copy wasn't found, or
 * the copy if it was.
 */
static struct pl *push_cached_page(struct nbsl_node *top, struct pl *page)
{
	size_t hash = hash_cached_page(page->offset, page->fsid_ino >> 32, page->fsid_ino & 0xffffffffu);
	struct nbsl *list = &pc_buckets[hash & (n_pc_buckets - 1)];
	do {
		struct nbsl_node *newtop = nbsl_top(list);
		if(newtop != top) {
			/* examine new nodes between first and @top. */
			struct nbsl_iter it;
			for(struct nbsl_node *cur = nbsl_first(list, &it); cur != NULL && cur != top; cur = nbsl_next(list, &it)) {
				struct pl *cand = container_of(cur, struct pl, nn);
				if(atomic_load_explicit(&cand->status, memory_order_relaxed) == PL_DEAD) continue;
				if(cand->fsid_ino == page->fsid_ino && cand->offset == page->offset) return cand;
			}
			top = newtop;
		}
	} while(!nbsl_push(list, top, &page->nn));
	atomic_store(&pl2pp(page)->link, page);
	return page;
}

struct pl *find_cached_page(struct nbsl_node **top_p, const struct lazy_mmap *mm, int bump)
{
	assert(top_p != NULL);
	uint64_t fsid_ino = (uint64_t)pidof_NP(mm->fd_serv) << 48 | mm->ino;
	size_t hash = hash_cached_page(mm->offset + bump, fsid_ino >> 32, fsid_ino & 0xffffffff);
	struct nbsl *bucket = &pc_buckets[hash & (n_pc_buckets - 1)];
	struct nbsl_iter it;
	for(struct nbsl_node *cur = *top_p = nbsl_first(bucket, &it); cur != NULL; cur = nbsl_next(bucket, &it)) {
		struct pl *cand = container_of(cur, struct pl, nn);
		if(atomic_load_explicit(&cand->status, memory_order_relaxed) == PL_DEAD) continue;
		if(cand->fsid_ino == fsid_ino && cand->offset == mm->offset + bump) return cand;
	}
	return NULL;
}

static ssize_t read_at(const struct lazy_mmap *mm, void *buf, unsigned length, size_t offset) {
	assert(offset <= INT_MAX);
	int n = __io_read(mm->fd_serv, mm->ino, length, offset, buf, &length);
	if(n != 0) return n > 0 ? -EIO : n; else return length;
}

/* finds the cached page, or reads it from @mm if it's not in cache.
 * @bump is # of pages from @mm start. @vp is the virtual page that'll take
 * ownership of a freshly-loaded page. return value is negative errno, or 0
 * when found, or 1 when loaded. *@cached_p will be filled in.
 *
 * TODO: in the future this function will do some sort of continuation
 * business to allow vm to respond while waiting for IO. for now, not so much.
 * this'll also require some way to wait on ongoing fetches, so the actual
 * item that lands in the page cache may also be a placeholder.
 */
int fetch_cached_page(struct pl **cached_p, const struct lazy_mmap *mm, int bump, struct vp *vp)
{
	assert(e_inside());
	struct nbsl_node *top;
	struct pl *cached = find_cached_page(&top, mm, bump);
	if(cached != NULL) {
		*cached_p = cached;
		return 0;
	} else {
		struct pl *link = get_free_pl();
		ssize_t n = 0;
		if(mm->flags & MAP_ANONYMOUS) memset(pl2ptr(link), '\0', PAGE_SIZE);
		else {
			if(n = read_at(mm, pl2ptr(link), PAGE_SIZE, (mm->offset + bump) * PAGE_SIZE), n < 0) { unget_free_pl(link); return n; }
			if(n < PAGE_SIZE) {
				log_info("short read (got %zd bytes)", n);
				memset(pl2ptr(link) + n, '\0', PAGE_SIZE - n);
			}
		}
		struct pl *nl = malloc(sizeof *nl); if(nl == NULL) { unget_free_pl(link); return -ENOMEM; }
		*nl = (struct pl){ .page_num = link->page_num, .status = PL_ALIVE, .offset = mm->offset + bump,
			.fsid_ino = (uint64_t)pidof_NP(mm->fd_serv) << 48 | (mm->ino & ~(0xffffull << 48)) };
		assert(PL_FSID(nl) == pidof_NP(mm->fd_serv)); assert(PL_INO(nl) == mm->ino);
		atomic_store(&pl2pp(nl)->owner, vp);
		cached = push_cached_page(top, nl);
		if(cached == nl) {
			assert(pl2pp(cached)->owner == vp);
			e_free(link);
			n = 1; /* actual miss and insert */
		} else {
			unget_free_pl(link);
			assert(cached != NULL);
			n = 0; /* latent cache hit */
		}
		*cached_p = cached;
		return n;
	}
}

void init_pagecache(size_t total_pages)
{
	/* pagecache bucket heads cost two words apiece, so we'll allocate one for
	 * every 12 physical pages -- rounding up to nearest power of two.
	 */
	n_pc_buckets_log2 = size_to_shift(total_pages / 12);
	pc_buckets = malloc(sizeof *pc_buckets * n_pc_buckets);
	if(pc_buckets == NULL) {
		log_crit("can't allocate pagecache buckets???");
		abort();
	}
	for(int i=0; i < n_pc_buckets; i++) nbsl_init(&pc_buckets[i]);

	/* TODO: grab these off a random number generator when NDEBUG is defined,
	 * but use stupid constants otherwise.
	 */
	pc_salt[0] = 0xdeadbeef;
	pc_salt[1] = 0xcafebabe;
	pc_salt[2] = 0xfeedface;
	pc_salt[3] = 0x12345678;

	log_info("2**%d (= %u) buckets, %zu pages per bucket", n_pc_buckets_log2, n_pc_buckets, total_pages / n_pc_buckets);
}
