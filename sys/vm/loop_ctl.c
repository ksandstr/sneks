#define LOOPBACKIMPL_IMPL_SOURCE
#include <stddef.h>
#include <stdbool.h>
#include <errno.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/sysmacros.h>
#include <muidl.h>
#include <ccan/array_size/array_size.h>
#include <ccan/compiler/compiler.h>
#include <ccan/htable/htable.h>
#include <l4/types.h>
#include <l4/ipc.h>
#include <l4/thread.h>
#include <sneks/hash.h>
#include <sneks/devno.h>
#include <sneks/process.h>
#include <sneks/systask.h>
#include <sneks/rollback.h>
#include <sneks/api/loopback-defs.h>
#include <sneks/api/path-defs.h>
#include <sneks/api/io-defs.h>
#include "loopback-impl-defs.h"
#include "defs.h"

#define CTL_MAJOR 10
#define CTL_MINOR 237
#define LOOP_MAJOR 7

struct ctl_auth {
	pid_t pid;
	int count, fd;
};

static size_t rehash_auth(const void *, void *);

blk_t *devs[MAX_LOOPDEV + 1];
int max_attached = -1;	/* TODO: invariants() that devs[max_attached + 1 .. MAX_LOOPDEV] == NULL */

static struct htable auth = HTABLE_INITIALIZER(auth, &rehash_auth, NULL);

static size_t rehash_auth(const void *item, void *priv UNUSED) {
	const struct ctl_auth *c = item;
	return int_hash(c->pid);
}

static bool cmp_ctl_to_pid(const void *cand, void *key) {
	const struct ctl_auth *c = cand;
	return c->pid == *(const pid_t *)key;
}

static PURE_FUNCTION struct ctl_auth *get_auth(void) {
	pid_t caller = pidof_NP(muidl_get_sender());
	return htable_get(&auth, int_hash(caller), &cmp_ctl_to_pid, &caller);
}

static bool authorized(void) { return get_auth() != NULL; }

static void auth_dock(struct ctl_auth *c) {
	if(--c->count > 0) return;
	htable_del(&auth, rehash_auth(c, NULL), c);
	free(c);
}

bool loop_valid(int id) { return id >= 0 && id < ARRAY_SIZE(devs) && devs[id] != NULL; }

static bool lc_get_info(int query_id, int *idmax_p, struct sneks_loop_control_cfg *cfg)
{
	sync_confirm();
	if(!authorized()) return false; /* TODO: allow queries of unprivileged data */
	if(!loop_valid(query_id)) return false;
	struct loopdev *d = blk_priv_of(devs[query_id]);
	*cfg = (struct sneks_loop_control_cfg){
		.offset = d->offset, .sizelimit = d->sizelimit, .dio = d->dio, .log_sec = d->log_sec,
		.autoclear = d->autoclear, .ro = d->ro,
	};
	*idmax_p = max_attached;
	return true;
}

static bool lc_get_back_file(int query_id, char *val)
{
	sync_confirm();
	if(!authorized()) return false; /* FIXME: instead, return false when non-root caller doesn't own backing file */
	if(!loop_valid(query_id)) return false;
	struct loopdev *d = blk_priv_of(devs[query_id]);
	strscpy(val, d->backing_path, SNEKS_PATH_PATH_MAX);
	return true;
}

static off_t get_size(L4_ThreadId_t server, int handle) {
	struct sneks_path_statbuf st;
	int n = __path_stat_handle(server, handle, &st);
	return n == 0 ? st.st_size : n;
}

/* TODO: make robust */
static int lc_attach(int *res_p, int req_id, int flags, const struct sneks_loop_control_cfg *cfg, L4_Word_t server_raw, int handle)
{
	sync_confirm();
	if(!authorized()) return -EACCES;
	if(req_id < -1 || req_id > MAX_LOOPDEV) return -EINVAL;
	if(req_id >= 0 && devs[req_id] != NULL) return -EEXIST;
	for(int i=0; req_id < 0 && i <= MAX_LOOPDEV; i++) {
		if(devs[i] == NULL) req_id = i;
	}
	if(req_id < 0) return -ENOENT;
	L4_ThreadId_t server = { .raw = server_raw };
	struct loopdev *d = NULL; blk_t *blk = NULL;
	int n = __io_touch(server, handle); if(n != 0) goto fail;
	off_t img_size = get_size(server, handle); if(img_size < 0) return img_size;
	if(blk = blk_new(&loop_class, img_size), blk == NULL) return -ENOMEM;
	d = blk_priv_of(blk);
	*d = (struct loopdev){ .server = server, .handle = handle, .minor = req_id,
#define _(n, N, d) .n = flags & SNEKS_LOOP_CONTROL_CFG_##N ? max_t(off_t, 0, cfg->n) : (d)
		_(offset, OFFSET, 0), _(sizelimit, SIZELIMIT, 0), _(dio, DIO, 0), _(autoclear, AUTOCLEAR, false), _(ro, RO, false), _(log_sec, LOG_SEC, 512),
#undef _
	};
	if(d->backing_path = malloc(SNEKS_PATH_PATH_MAX + 2), d->backing_path == NULL) { blk_undo_new(blk); return -ENOMEM; }
	d->backing_path[0] = '/';
	if(n = __path_get_path(d->server, d->backing_path + 1, d->handle, ""), n != 0) goto fail;
	void *re = realloc(d->backing_path, strnlen(d->backing_path, SNEKS_PATH_PATH_MAX) + 1);
	if(re != NULL) d->backing_path = re; else { n = -ENOMEM; goto fail; }
	if(d->log_sec & (d->log_sec - 1)) goto Einval; /* not power of two */
	if(d->offset & (d->log_sec - 1)) goto Einval; /* not multiple of log_sec */
	if(d->sizelimit & (d->log_sec - 1)) goto Einval; /* same */
	if(d->dio != 0) goto Einval; /* not supported */
	if(n = blk_publish(blk, makedev(LOOP_MAJOR, req_id)), n != 0) goto fail;
	devs[req_id] = blk;
	if(req_id > max_attached) max_attached = req_id;
	*res_p = req_id;
	return 0;
Einval: n = -EINVAL; /* FALL THRU */
fail: free(d->backing_path); blk_undo_new(blk); return n < 0 ? n : -EIO;
}

static int lc_detach(int req_id)
{
	sync_confirm();
	if(!authorized()) return -EACCES;
	if(!loop_valid(req_id)) return -ENOENT;
	blk_detach(devs[req_id]);
	devs[req_id] = NULL;
	while(max_attached > 0 && devs[max_attached] == NULL) max_attached--;
	return 0;
}

static void undo_ctl_open(L4_Word_t cpid, int *fd) {
	struct ctl_auth *c = htable_get(&auth, int_hash(cpid), &cmp_ctl_to_pid, &(pid_t){ cpid });
	assert(c != NULL && c->fd == *fd);
	auth_dock(c);
}

static int lc_open(int *handle_p, unsigned object, L4_Word_t cookie, int flags)
{
	sync_confirm();
	if(object != DEV_OBJECT(2, CTL_MAJOR, CTL_MINOR)) return -ENODEV;
	struct ctl_auth *c = get_auth();
	if(c != NULL) c->count++;
	else {
		if(c = malloc(sizeof *c), c == NULL) return -ENOMEM;
		*c = (struct ctl_auth){ .pid = pidof_NP(muidl_get_sender()), .count = 1 };
		if(!htable_add(&auth, rehash_auth(c, NULL), c)) { free(c); return -ENOMEM; }
	}
	c->fd = object ^ 0xfaced007;
	set_rollback(&undo_ctl_open, c->pid, &c->fd);
	*handle_p = c->fd;
	return 0;
}

static int lc_seek(int handle, off_t *off_p, int whence) {
	sync_confirm();
	return -EINVAL; /* do not seek loop-control on the battlefield */
}

static int lc_close(int fd)
{
	sync_confirm();
	struct ctl_auth *c = get_auth();
	if(c != NULL) auth_dock(c);
	else if(pidof_NP(muidl_get_sender()) >= SNEKS_MIN_SYSID) {
		struct htable_iter it; /* kludge for lifecycle events */
		for(c = htable_first(&auth, &it); c != NULL; c = htable_next(&auth, &it)) {
			if(c->fd == fd) auth_dock(c);
		}
	}
	return 0;
}

static int lc_isatty(int handle) {
	sync_confirm();
	return 0;
}

int loop_ctl_main(void *unused UNUSED /* /// \\\ xXx 420 üNu$€D 360 XxX /// \\\ */)
{
	int n = devnoctl('c', CTL_MAJOR, CTL_MINOR, L4_Myself());
	if(n < 0) { log_crit("can't register /dev/loop-control: %s", stripcerr(n)); abort(); }
	static const struct loopback_impl_vtable vtab = {
		/* Sneks::LoopControl */
		.get_info = &lc_get_info, .get_back_file = &lc_get_back_file,
		.attach = &lc_attach, .detach = &lc_detach,
		/* Sneks::File */
		.open = &lc_open, .seek = &lc_seek,
		/* Sneks::IO */
		.close = &lc_close, .write = &enosys, .read = &enosys,
		.dup = &enosys, .dup_to = &enosys, .touch = &enosys,
		.isatty = &lc_isatty,
	};
	for(;;) {
		L4_Word_t status = _muidl_loopback_impl_dispatch(&vtab);
		if(status != 0 && !MUIDL_IS_L4_ERROR(status)) {
			log_info("dispatch status %#lx (last tag %#lx)\n", status, muidl_get_tag().raw);
		}
	}
}
