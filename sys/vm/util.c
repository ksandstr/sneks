#include <errno.h>
#include <sys/types.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <sneks/sys/devno-defs.h>
#include <sneks/sys/info-defs.h>
#include "defs.h"

int enosys() { return -ENOSYS; }

/* TODO: put this into a sys/crt util.c or some such */
int devnoctl(char kind, dev_t major, dev_t minor, L4_ThreadId_t tid)
{
	static L4_ThreadId_t reg_tid = { .raw = 0 };
	if(L4_IsNilThread(reg_tid)) {
		L4_ThreadId_t sysinfo_tid;
		int n = __info_lookup(L4_Pager(), &sysinfo_tid.raw);
		if(n != 0) return n;
		struct sneks_sysapi_info sii;
		if(n = __info_sysapi_block(sysinfo_tid, &sii), n != 0) return n;
		reg_tid.raw = sii.devno;
		if(L4_IsNilThread(reg_tid)) return -ESRCH;	/* TODO: use a "no server" errno */
	}
	return __devno_devnoctl(reg_tid, kind, major, minor, &tid.raw);
}
