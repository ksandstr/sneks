#ifndef _SYS_VM_SPACE_H
#define _SYS_VM_SPACE_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <l4/types.h>
#include <ccan/htable/htable.h>
#include <sneks/rbtree.h>

struct rangealloc;
struct lazy_mmap;
struct vp;

typedef int (*map_fn)(struct vp *, const struct lazy_mmap *, size_t, int);

/* set of mmap(2) parameters for lazy pagefault repair. `prot' gets translated
 * to the L4_Rights() mask to test fault access and stored in bits 16..18 of
 * ->flags, i.e. `(flags >> 16) & 7'.
 */
struct lazy_mmap
{
	struct rb_node rb;	/* in vm_space.maps */
	uintptr_t addr;
	size_t length;
	map_fn repair_fn;
	long flags;
	/* for pagecache access.
	 *
	 * when backed by a file, the three reference a filesystem service, an
	 * inode number, and a page offset within that file.
	 * (TODO: actually "ino" is a file descriptor until the fs-vm-block IPC
	 * chain stuff gets written. as such the descriptor isn't properly closed
	 * when the map is removed, or duplicated when the map is divided such as
	 * in munmap().)
	 *
	 * when anonymous, ->fd_serv is VM's sysid, ->ino is zero for private maps
	 * and an anonymous mapping ID when the map is shared. offset is nonzero
	 * in tail fragment mmaps caused by munmap().
	 */
	L4_ThreadId_t fd_serv;
	uint64_t ino;
	size_t offset;
	unsigned short tailsz; /* clear PAGE_SIZE-tailsz bytes at end of last page */
};

#define IS_ANON_MMAP(mm) (((mm)->flags & MAP_ANONYMOUS) && ((mm)->flags & MAP_SHARED))
#define MMAP_RIGHTS(mm) (((mm)->flags >> 16) & 7)

#define MAP_SYSTEM 0x80000000 /* forbidden range (SIP etc) */

/* address space. PID implied. valid when ->kip_area.raw != 0. uninitialized
 * when L4_IsNilFpage(->utcb_area).
 */
struct vm_space
{
	L4_Fpage_t kip_area, utcb_area, sysinfo_area;
	struct htable vpgs;		/* <struct vp_group *> with hash_vpg() */
	struct rb_root maps;	/* lazy_mmap per range of addr and length */
	struct lazy_mmap *last_mmap;
	struct rb_root as_free;	/* as_free per non-overlapping ->fp */
	uintptr_t brk;
	L4_Word_t mmap_bot;		/* bottom of as_free range */
};

extern struct rangealloc *vm_space_ra;
extern size_t user_addr_max;
/* no-match for vm_space.last_mmap. */
extern struct lazy_mmap no_last_mmap;

#define space_to_pid(sp) (pid_t)ra_ptr2id(vm_space_ra, (sp))
extern void init_space(void);
extern struct vm_space *get_space(pid_t);
extern void free_space(struct vm_space *);
extern void munmap_space(struct vm_space *sp, L4_Word_t addr, size_t size);
extern int fork_maps(struct vm_space *, struct vm_space *);
extern int fault_map(L4_Fpage_t *, struct vm_space *, size_t, int);

extern struct lazy_mmap *find_lazy_mmap(struct vm_space *, uintptr_t);
extern int reserve_mmap(struct lazy_mmap *, struct vm_space *, L4_Word_t, size_t, bool);
extern size_t hash_lazy_mmap_by_ino(const void *, void *);

#ifdef DEBUG_ME_HARDER
#include <sneks/invariant.h>
extern bool all_space_invariants(INVCTX_ARG, struct vp *const *, size_t);
#else
#define all_space_invariants(ctx, allvps, nallvps) true
#endif

#endif
