#include <stddef.h>
#include <errno.h>
#include <sneks/systask.h>
#include <sneks/api/io-defs.h>
#include "defs.h"

static int loop_dev_close(void *);
static void loop_dev_destroy(void *);
static int loop_dev_read(void *, uint64_t, int, void *);

struct blk_class loop_class = {
	.priv_size = sizeof(struct loopdev), .sec_size = 4096, .name = "loopback",
	.close = &loop_dev_close, .destroy = &loop_dev_destroy, .read = &loop_dev_read,
};

static int loop_dev_close(void *priv) {
	struct loopdev *d = priv;
	return __io_close(d->server, d->handle);
}

static void loop_dev_destroy(void *priv) {
	struct loopdev *d = priv;
	free(d->backing_path);
}

static int loop_dev_read(void *buf, uint64_t block, int count, void *priv) {
	struct loopdev *d = priv;
	unsigned n_read = count * loop_class.sec_size;
	int n = __io_read(d->server, d->handle, count * loop_class.sec_size, block * loop_class.sec_size, buf, &n_read);
	if(n == 0) return n_read / loop_class.sec_size; else return n < 0 ? n : -EIO;
}
