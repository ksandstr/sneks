#include <stdlib.h>
#include <stdint.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <assert.h>
#include <errno.h>
#include <nbsl.h>
#include <epoch.h>
#include <ccan/array_size/array_size.h>
#include <ccan/container_of/container_of.h>
#include <ccan/htable/htable.h>
#include <l4/types.h>
#include <l4/space.h>
#include <sneks/mm.h>
#include <sneks/hash.h>
#include <sneks/systask.h>
#include "defs.h"
#include "space.h"

static size_t hash_vp_by_phys(const void *ptr, void *priv); /* TODO: rename to hash_shared_vp() */

struct nbsl page_free_list = NBSL_LIST_INIT(page_free_list),
	page_active_list = NBSL_LIST_INIT(page_active_list);

/* multiset of vp by physical address when that physical page has been
 * referenced from more than one vp. a physical page's primary reference
 * (pp->owner) is always omitted, and may be NULL if it was removed before all
 * secondaries.
 */
struct htable share_table = HTABLE_INITIALIZER(share_table, &hash_vp_by_phys, NULL);

/* by physical page number in share_table. */
static size_t hash_vp_by_phys(const void *ptr, void *priv) {
	const struct vp *v = ptr;
	assert(v->status != 0);
	/* TODO: assert that high bit is clear in v->status, i.e. that we're
	 * hashing a physical frame number here.
	 */
	return int_hash(v->status);
}

size_t hash_vpg(const void *ptr, void *priv) {
	const struct vp_group *g = ptr;
	return int_hash(g->addr & ~VPG_MASK);
}

/* counts the number of occurrences of @page_num in share_table, returning true
 * if it's at least @count and false otherwise. @hash is int_hash(@page_num),
 * likely computed elsewhere already.
 */
bool has_shares(size_t hash, uint32_t page_num, int count)
{
	assert(count > 0);
	struct htable_iter it;
	for(struct vp *cur = htable_firstval(&share_table, &it, hash); cur != NULL; cur = htable_nextval(&share_table, &it, hash)) {
		if(cur->status == page_num && --count <= 0) return true;
	}
	return false;
}

#ifdef DEBUG_ME_HARDER
#include <setjmp.h>
#include <ccan/darray/darray.h>
#include <ccan/bitmap/bitmap.h>
#include <sneks/invariant.h>

typedef darray(struct vp *) vp_array;

static int cmp_ptrs(const void *a, const void *b) {
	return *(const void **)a - *(const void **)b;
}

struct nbsl *next_page_list(int *iter)
{
	int ix = (*iter)++;
	if(ix == 0) return &page_free_list;
	else if(ix == 1) return &page_active_list;
	else if(ix - 2 < n_pc_buckets) return &pc_buckets[ix - 2];
	else if(ix - 2 - n_pc_buckets < bufcache_size) return &bufcache[ix - 2 - n_pc_buckets];
	else return NULL;
}

static void get_all_vps(vp_array *dest)
{
	struct htable_iter hh;
	for(struct vp *share = htable_first(&share_table, &hh); share != NULL; share = htable_next(&share_table, &hh)) {
		darray_push(*dest, share);
	}
	int it = 0;
	struct nbsl *list;
	while(list = next_page_list(&it), list != NULL) {
		struct nbsl_iter jt;
		for(struct nbsl_node *cur = nbsl_first(list, &jt); cur != NULL; cur = nbsl_next(list, &jt)) {
			struct pl *link = container_of(cur, struct pl, nn);
			struct vp *vp = atomic_load(&pl2pp(link)->owner);
			if(vp != NULL) darray_push(*dest, vp);
		}
	}
}

static bool vp_invariants(INVCTX_ARG, struct vp *virt)
{
	inv_ok(~virt->status & 0x80000000, "vp is resident");
	inv_imply1(VP_IS_COW(virt), ~VP_RIGHTS(virt) & L4_Writable);
	inv_imply1(VP_RIGHTS(virt) & L4_Writable, !VP_IS_COW(virt));
	inv_ok1(virt->status == get_pp(virt->status)->link->page_num);
	return true;
inv_fail: return false;
}

static bool share_table_invariants(INVCTX_ARG, struct vp *const *all_vps, size_t n_all_vps)
{
	/* all <struct vp *> found through share_table, including the ones that're
	 * actually the sole owner (i.e. where pp->owner == NULL).
	 */
	darray(struct vp *) share_vps = darray_new(),
		owner_vps = darray_new();	/* ones discovered thru pp->owner */

	struct htable_iter it;
	for(const struct vp *share = htable_first(&share_table, &it);
		share != NULL;
		share = htable_next(&share_table, &it))
	{
		inv_push("share=%p: ->vaddr=%#x, ->status=%#x", share, share->vaddr, share->status);

		inv_ok1(~share->status & 0x80000000);
		if(all_vps != NULL) {
			inv_ok(bsearch(&share, all_vps, n_all_vps, sizeof(struct vp *),
				&cmp_ptrs) != NULL, "share present in all_vps");
		}

		const struct pp *phys = get_pp(share->status);
		inv_log("phys=%p: ->link=%p, ->owner=%p",
			phys, phys->link, phys->owner);
		inv_ok1(phys->link->page_num == share->status);
		inv_imply1(phys->owner != NULL,
			phys->owner->status == share->status);
		inv_ok1(share != phys->owner);
		if(all_vps != NULL) {
			inv_imply1(phys->owner != NULL,
				bsearch(&phys->owner, all_vps, n_all_vps,
					sizeof(struct vp *), &cmp_ptrs) != NULL);
		}

		if(phys->owner != NULL) darray_push(owner_vps, phys->owner);
		darray_push(share_vps, (struct vp *)share);

		inv_pop();
	}

	/* sort and uniq owner_vps, since that'll have as many duplicates of a
	 * primary owner as there are matching secondaries in share_table. then
	 * add it to share_vps to top that one up.
	 *
	 * also check that owner_vps and share_table are disjoint.
	 */
	qsort(owner_vps.item, owner_vps.size, sizeof(void *), &cmp_ptrs);
	qsort(share_vps.item, share_vps.size, sizeof(void *), &cmp_ptrs);
	size_t o = 0;
	for(size_t i=1; i < owner_vps.size; i++) {
		assert(o < i);
		if(o == 0 || owner_vps.item[o - 1] != owner_vps.item[i]) {
			owner_vps.item[o++] = owner_vps.item[i];
			inv_ok(bsearch(&owner_vps.item[i],
				share_vps.item, share_vps.size, sizeof(struct vp *),
				&cmp_ptrs) == NULL, "owner_vps and share_vps are disjoint");
		}
		assert(o < owner_vps.alloc || i == owner_vps.size - 1);
	}
	darray_resize(owner_vps, o);
	darray_append_items(share_vps, owner_vps.item, owner_vps.size);

	/* sort share_vps and check against duplicates. */
	qsort(share_vps.item, share_vps.size, sizeof(void *), &cmp_ptrs);
	inv_imply1(all_vps != NULL, share_vps.size <= n_all_vps);
	for(size_t i=1; i < share_vps.size; i++) {
		inv_push("share_vps[%d]=%p ∧ share_vps[i=%d]=%p",
			i-1, share_vps.item[i-1], i, share_vps.item[i]);
		inv_log("->status=%#x", share_vps.item[i]->status);
		inv_ok1(share_vps.item[i - 1] != share_vps.item[i]);
		inv_pop();
	}

	darray_free(share_vps);
	darray_free(owner_vps);
	return true;

inv_fail:
	darray_free(share_vps);
	darray_free(owner_vps);
	return false;
}

bool page_invariants(INVCTX_ARG)
{
	int eck = e_begin();
	vp_array all_vps = darray_new();
	get_all_vps(&all_vps);
	qsort(all_vps.item, all_vps.size, sizeof(void *), &cmp_ptrs);
	for(size_t i=1; i < all_vps.size; i++) {
		inv_push("all_vps[i=%d]=%p", i, all_vps.item[i]);
		inv_log("->status=%#x", all_vps.item[i]->status);
		inv_ok1(all_vps.item[i - 1] != all_vps.item[i]); /* present once in share_table, pp->owner */
		inv_ok1(vp_invariants(INV_CHILD, all_vps.item[i]));
		inv_pop();
	}
	inv_ok1(share_table_invariants(INV_CHILD, all_vps.item, all_vps.size));
	inv_ok1(vm_space_ra == NULL || all_space_invariants(INV_CHILD, all_vps.item, all_vps.size));
	inv_ok1(pc_buckets == NULL || pc_invariants(INV_CHILD, all_vps.item, all_vps.size));
	e_end(eck);
	darray_free(all_vps);
	return true;
inv_fail: e_end(eck); darray_free(all_vps); return false;
}
#endif

void push_page(struct nbsl *list, struct pl *oldlink)
{
	struct pl *nl = malloc(sizeof *nl);
	*nl = *oldlink;
	atomic_store_explicit(&nl->status, PL_ALIVE, memory_order_relaxed);
	atomic_store_explicit(&pl2pp(oldlink)->link, nl, memory_order_relaxed);
	struct nbsl_node *top;
	do top = nbsl_top(list); while(!nbsl_push(list, top, &nl->nn));
}

struct pl *get_free_pl(void) {
	struct nbsl_node *nod = nbsl_pop(&page_free_list);
	if(nod == NULL) { log_crit("out of memory!"); abort(); }
	return container_of(nod, struct pl, nn);
}

void unget_free_pl(struct pl *link) {
	push_page(&page_free_list, link);
	e_free(link);
}

/* discards @dead, adds its page to the free page list. */
void free_page(struct pl *dead) {
	push_page(&page_free_list, dead);
	atomic_store_explicit(&dead->status, PL_DEAD, memory_order_release);
	assert(atomic_load(&pl2pp(dead)->link) != dead);
}

/* returns as htable_add().
 * TODO: why does this use @status instead of @share->status?
 */
bool add_share(uint32_t status, struct vp *share)
{
	/* TODO: handle other forms of vp->status as they appear. */
	assert(~status & 0x80000000);	/* in physical memory */
	struct pp *phys = get_pp(status);
	struct vp *prev_owner = NULL;
	if(atomic_compare_exchange_strong(&phys->owner, &prev_owner, share)) {
		/* primary ownership taken. */
		return true;
	} else {
		/* multiple ownership. */
		return htable_add(&share_table, hash_vp_by_phys(share, NULL), share);
	}
}

/* add_share() in reverse. caller should release the physical page if the last
 * share was removed in which case rm_share() returns true, otherwise false.
 */
bool rm_share(struct vp *vp)
{
	/* TODO: handle other forms of vp->status as they appear. */
	assert(~vp->status & 0x80000000);	/* in physical memory */
	size_t hash = hash_vp_by_phys(vp, NULL);
	struct pp *phys = get_pp(vp->status);
	struct vp *prev_owner = vp;
	if(!atomic_compare_exchange_strong(&phys->owner, &prev_owner, NULL)) {
		/* wasn't primary. */
		bool ok = htable_del(&share_table, hash, vp);
		assert(ok);
		if(prev_owner != NULL) return false;
	}
	return !has_shares(hash, vp->status, 1);
}

static bool cmp_vpg_addr(const void *cand, void *key) {
	assert((*(const size_t *)key & VPG_MASK) == 0);
	const struct vp_group *g = cand;
	return (g->addr & ~VPG_MASK) == *(const size_t *)key;
}

struct vp *add_vp(struct vm_space *sp, struct vp_iter *it, size_t addr)
{
	assert(it != NULL); assert((addr & PAGE_MASK) == 0);
	size_t key = addr & ~VPG_MASK, limb = (addr & VPG_MASK) >> PAGE_BITS;
	if(it->g == NULL || it->g->addr != key) it->g = htable_get(&sp->vpgs, int_hash(key), &cmp_vpg_addr, &key);
	if(it->g == NULL) {
		if(it->g = calloc(1, sizeof *it->g), it->g == NULL) return NULL; /* ENOMEM from calloc() */
		it->g->addr = key; assert(VP_COUNT(it->g) == 0);
		if(!htable_add(&sp->vpgs, hash_vpg(it->g, NULL), it->g)) { free(it->g); it->g = NULL; assert(errno == ENOMEM); return NULL; }
	}
	struct vp *r = &it->g->pages[limb];
	if(r->status != 0) { errno = EEXIST; return NULL; }
	else {
		assert(VP_COUNT(it->g) < VPG_SIZE);
		it->g->addr++;
		return r;
	}
}

struct vp *get_vp(struct vm_space *sp, struct vp_iter *it, size_t addr) {
	assert(it != NULL); assert((addr & PAGE_MASK) == 0);
	size_t key = addr & ~VPG_MASK, limb = (addr & VPG_MASK) >> PAGE_BITS;
	if(it->g == NULL || it->g->addr != key) it->g = htable_get(&sp->vpgs, int_hash(key), &cmp_vpg_addr, &key);
	return it->g == NULL || it->g->pages[limb].status == 0 ? NULL : &it->g->pages[limb];
}

uint32_t del_vp(uintptr_t *vaddr_out, struct vm_space *sp, struct vp_iter *it, size_t addr)
{
	assert(e_inside()); assert(vaddr_out != NULL); assert(it != NULL); assert((addr & PAGE_MASK) == 0);
	size_t key = addr & ~VPG_MASK, limb = (addr & VPG_MASK) >> PAGE_BITS;
	if(it->g == NULL || it->g->addr != key) it->g = htable_get(&sp->vpgs, int_hash(key), &cmp_vpg_addr, &key);
	uint32_t ret = 0;
	if(it->g != NULL) {
		struct vp *p = &it->g->pages[limb];
		assert(p->status != 0); assert(VP_COUNT(it->g) > 0);
		*vaddr_out = p->vaddr; ret = p->status;
		p->status = 0;
		if(--it->g->addr, VP_COUNT(it->g) == 0) {
			htable_del(&sp->vpgs, int_hash(key), it->g);
			e_free(it->g);
			it->g = NULL;
		}
	}
	assert(get_vp(sp, &(struct vp_iter){ }, addr) == NULL);
	return ret;
}

void remove_vp(struct vp *vp)
{
	assert(e_inside()); assert(vp->status != 0); assert(~vp->status & 0x80000000);
	struct pp *phys = get_pp(vp->status);
	struct pl *link0 = atomic_load_explicit(&phys->link, memory_order_acquire);
	if(PL_IS_PRIVATE(link0) && !VP_IS_COW(vp)) {
		assert(atomic_load(&phys->owner) == vp);	/* single owner */
		assert(!has_shares(int_hash(link0->page_num), link0->page_num, 1));
		struct pl *link = atomic_exchange(&phys->link, NULL);
		assert(link0 == link);	/* private, so won't have changed */
		struct vp *old = atomic_exchange(&phys->owner, NULL);
		assert(old == vp);	/* likewise */
		free_page(link0);
	} else if(rm_share(vp) && VP_IS_ANON(vp)) {
		/* eagerly release pages that're not in the page cache. */
		free_page(link0);
	}
	assert(atomic_load(&phys->owner) != vp);
}

/* TODO: this function needs a rename to clear_vps() or something. */
void remove_vp_range(struct vm_space *sp, size_t addr, size_t size)
{
	assert(e_inside()); assert((addr & PAGE_MASK) == 0);
	L4_Fpage_t fps[64];
	int n_fps = 0;
	struct vp_iter it = { };
	for(size_t pos = addr; pos < addr + size; pos += PAGE_SIZE) {
		struct vp *v = get_vp(sp, &it, pos);
		if(v == NULL) {
			if(it.g == NULL) pos = ((pos + PAGE_SIZE * VPG_SIZE) & ~VPG_MASK) - PAGE_SIZE;
			continue;
		}
		assert(~v->status & 0x80000000);
		L4_Fpage_t fp = L4_FpageLog2(v->status << PAGE_BITS, PAGE_BITS);
		L4_Set_Rights(&fp, L4_FullyAccessible);
		fps[n_fps++] = fp;
		if(n_fps == ARRAY_SIZE(fps)) {
			L4_UnmapFpages(ARRAY_SIZE(fps), fps);
			n_fps = 0;
		}
		remove_vp(v);
		del_vp(&(uintptr_t){ 0 }, sp, &it, pos);
	}
	if(n_fps > 0) L4_UnmapFpages(n_fps, fps);
}

static L4_Fpage_t pf_cow(struct vp *virt)
{
	assert(~VP_RIGHTS(virt) & L4_Writable);
	struct pp *phys = get_pp(virt->status);
	struct vp *primary = atomic_load_explicit(&phys->owner, memory_order_relaxed);
	size_t hash = int_hash(virt->status);
	if(primary != virt) { /* remove secondary share */
		if(!htable_del(&share_table, hash, virt)) {
			log_crit("expected share, wasn't found???");
			/* TODO: add curse */
		}
	}
	/* is @virt the sole owner of the page, which isn't in the page cache? */
	if((primary == NULL || primary == virt) && VP_IS_ANON(virt) && !has_shares(hash, virt->status, 1)) {
		/* it is. take ownership. */
		assert(primary == NULL || atomic_load(&phys->owner) == virt);
		if(primary == NULL) {
			/* take primary share. */
			atomic_store_explicit(&phys->owner, virt, memory_order_relaxed);
		}
		virt->vaddr &= ~VPF_COW;
		virt->vaddr |= L4_Writable;
	} else { /* it isn't; make a copy. */
		if(primary == virt) {
			/* drop primary share. */
			atomic_store_explicit(&phys->owner, NULL, memory_order_relaxed);
		}
		struct pl *newpl = get_free_pl();
		memcpy(pl2ptr(newpl), (void *)((uintptr_t)virt->status << PAGE_BITS), PAGE_SIZE);
		virt->status = newpl->page_num;
		virt->vaddr &= ~VPF_COW;
		virt->vaddr |= L4_Writable | VPF_ANON;
		atomic_store_explicit(&pl2pp(newpl)->owner, virt, memory_order_release);
		push_page(&page_active_list, newpl);
		e_free(newpl);
	}
	L4_Fpage_t map_page = L4_FpageLog2(virt->status << PAGE_BITS, PAGE_BITS);
	L4_Set_Rights(&map_page, VP_RIGHTS(virt));
	return map_page;
}

int fault_vp(L4_Fpage_t *map_page_p, struct vm_space *sp, size_t faddr, int rwx)
{
	struct vp *old = get_vp(sp, &(struct vp_iter){ }, faddr & ~PAGE_MASK);
	if(old == NULL) return 0;
	else if(VP_IS_COW(old) && (rwx & L4_Writable)) *map_page_p = pf_cow(old);
	else if((VP_RIGHTS(old) & rwx) != rwx) return -EFAULT;
	else {
		*map_page_p = L4_FpageLog2(old->status << PAGE_BITS, PAGE_BITS);
		L4_Set_Rights(map_page_p, VP_RIGHTS(old));
	}
	return 1;
}

int fork_pages(struct vm_space *src, struct vm_space *dest)
{
	L4_Fpage_t unmaps[64];
	int unmap_pos = 0, n = 0;
	struct htable_iter it;
	struct vp_iter jt = { };
	for(struct vp_group *g_src = htable_first(&src->vpgs, &it); g_src != NULL; g_src = htable_next(&src->vpgs, &it)) {
		for(struct vp *cur = g_src->pages; cur < &g_src->pages[VPG_SIZE]; cur++) {
			if(cur->status == 0) continue;
			assert(!VP_IS_COW(cur) || (~VP_RIGHTS(cur) & L4_Writable));
			if(!VP_IS_ANON(cur)) continue;
			struct vp *copy = add_vp(dest, &jt, (g_src->addr & ~VPG_MASK) + (cur - g_src->pages) * PAGE_SIZE); if(copy == NULL) { n = -errno; goto end; }
			assert(copy == &jt.g->pages[cur - g_src->pages]);
			*copy = (struct vp){ .status = cur->status, .vaddr = (cur->vaddr & ~L4_Writable) | ((VP_RIGHTS(cur) & L4_Writable) ? VPF_COW : 0) };
			if(!add_share(cur->status, copy)) goto Enomem;
			if(VP_RIGHTS(cur) & L4_Writable) {
				cur->vaddr |= VPF_COW;
				cur->vaddr &= ~L4_Writable;
				unmaps[unmap_pos] = L4_FpageLog2(cur->status << PAGE_BITS, PAGE_BITS);
				L4_Set_Rights(&unmaps[unmap_pos], L4_Writable);
				if(++unmap_pos == ARRAY_SIZE(unmaps)) {
					L4_UnmapFpages(ARRAY_SIZE(unmaps), unmaps);
					unmap_pos = 0;
				}
			}
		}
	}
end:
	if(unmap_pos > 0) L4_UnmapFpages(unmap_pos, unmaps);
	return n;
Enomem: n = -ENOMEM; goto end;
}
