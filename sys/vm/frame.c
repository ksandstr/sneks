#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <ccan/minmax/minmax.h>
#include <ccan/compiler/compiler.h>
#include <ccan/array_size/array_size.h>
#include <l4/types.h>
#include <l4/ipc.h>
#include <sneks/systask.h>
#include "defs.h"

size_t pp_first, pp_total;
struct rangealloc *pp_ra;

const struct __sysinfo *the_sip = NULL;

#ifdef DEBUG_ME_HARDER
#include <epoch.h>
#include <ccan/bitmap/bitmap.h>
#include <sneks/hash.h>
#include <sneks/invariant.h>
bool frame_invariants(INVCTX_ARG)
{
	assert(e_inside());
	/* every physical page should be found exactly once in the free list,
	 * active list, page cache, or buffer cache.
	 */
	bitmap *phys_seen = bitmap_alloc0(pp_total);
	int ll = 0;
	struct nbsl *list;
	while(list = next_page_list(&ll), list != NULL) {
		inv_push("list=%p (ll=%d)", list, ll);
		struct nbsl_iter it;
		for(struct nbsl_node *cur = nbsl_first(list, &it); cur != NULL; cur = nbsl_next(list, &it)) {
			struct pl *link = container_of(cur, struct pl, nn);
			if(atomic_load(&link->status) == PL_DEAD) continue;
			inv_push("link=%p, ->page_num=%u", link, link->page_num);
			struct pp *phys = pl2pp(link);
			inv_ok1(phys->link == link);

			inv_ok1(link->page_num >= pp_first);
			size_t pgix = link->page_num - pp_first;
			inv_ok1(!bitmap_test_bit(phys_seen, pgix));
			bitmap_set_bit(phys_seen, pgix);

			/* physical page ownership. */
			inv_imply1(list == &page_free_list, phys->owner == NULL);
			size_t pnhash = int_hash(link->page_num);
			inv_imply1(list == &page_active_list,
				phys->owner != NULL || has_shares(pnhash, link->page_num, 1));

			inv_pop();
		}
		inv_pop();
	}
	inv_ok(bitmap_full(phys_seen, pp_total), "pp_total=%u pp seen", (unsigned)pp_total);
	free(phys_seen);
	return true;
inv_fail: free(phys_seen); return false;
}
#endif

static void init_phys(L4_Fpage_t *phys, int n_phys)
{
	size_t p_min = SIZE_MAX, p_max = 0;
	for(int i=0; i < n_phys; i++) {
		p_min = min_t(size_t, p_min, L4_Address(phys[i]) >> PAGE_BITS);
		p_max = max_t(size_t, p_max, (L4_Address(phys[i]) + L4_Size(phys[i])) >> PAGE_BITS);
	}
	pp_first = p_min; pp_total = p_max - p_min;
	log_info("allocating %lu <struct pp> (first is %lu)", (unsigned long)pp_total, (unsigned long)p_min);
	pp_ra = RA_NEW(struct pp, pp_total);
	for(int i=0; i < n_phys; i++) {
		size_t base = L4_Address(phys[i]) >> PAGE_BITS; assert(base > 0);
		for(int o=0; o < L4_Size(phys[i]) >> PAGE_BITS; o++) {
			struct pp *pp = ra_alloc(pp_ra, base + o - pp_first);
			struct pl *link = malloc(sizeof *link); if(link == NULL) { log_crit("can't allocate pl"); abort(); }
			*link = (struct pl){ .page_num = base + o, .status = 1 };
			atomic_store(&pp->link, link);
			struct nbsl_node *top;
			do top = nbsl_top(&page_free_list); while(!nbsl_push(&page_free_list, top, &link->nn));
			assert(pl2pp(link) == pp); assert(pp->link == link);
		}
	}
	/* TODO: assert(invariants()); */
	log_info("physical memory tracking initialized.");
}

/* TODO: get rid of "addr_hi", here and on root */
static L4_Fpage_t next_frame(L4_ThreadId_t root, L4_Word_t *addr_hi)
{
	L4_Accept(L4_UntypedWordsAcceptor);
	if(L4_IpcFailed(L4_Receive(root))) goto ipcfail;
	L4_Fpage_t page; L4_StoreMR(1, &page.raw); L4_StoreMR(2, addr_hi);
	if(!L4_IsNilFpage(page)) {
		L4_Accept(L4_MapGrantItems(page));
		L4_LoadMR(0, 0);
		if(L4_IpcFailed(L4_Call(root))) goto ipcfail;
	}
	return page;
ipcfail: log_crit("ipc failed: %s", stripcerr(L4_ErrorCode())); abort();
}

COLD L4_Fpage_t *init_protocol(int *n_phys_p, L4_ThreadId_t *peer_tid_p)
{
	/* first handshake; get total_pages, max_addr concerning physical memory. */
	L4_Accept(L4_UntypedWordsAcceptor);
	if(L4_IpcFailed(L4_Wait(peer_tid_p))) {
		log_crit("can't get init message: %s", stripcerr(L4_ErrorCode()));
		abort();
	}
	L4_Word_t total_pages, max_phys;
	L4_StoreMR(1, &total_pages); L4_StoreMR(2, &max_phys);
	log_info("total_pages=%lu, max_phys=%#lx", total_pages, max_phys);
	L4_LoadMR(0, 0);
	L4_Reply(*peer_tid_p);
	/* get SIP. */
	static uint8_t sip_space[PAGE_SIZE] __attribute__((aligned(PAGE_SIZE)));
	L4_Accept(L4_MapGrantItems(L4_Fpage((uintptr_t)sip_space, sizeof sip_space)));
	if(L4_IpcFailed(L4_Receive(*peer_tid_p))) {
		log_crit("can't get second init message: %s", stripcerr(L4_ErrorCode()));
		abort();
	}
	L4_LoadMR(0, 0);
	L4_Reply(*peer_tid_p);
	the_sip = (struct __sysinfo *)sip_space;
	/* pump the rest of physical memory. */
	L4_Fpage_t pg, temp_phys[200]; /* goes up to 4G easily; after that, fingers crossed */
	L4_Word_t addr_hi, n_phys = 0;
	while(pg = next_frame(*peer_tid_p, &addr_hi), !L4_IsNilFpage(pg)) {
		if(n_phys == ARRAY_SIZE(temp_phys)) { log_crit("too many fpage frames!"); abort(); }
		temp_phys[n_phys++] = pg;
	}
	/* initialize sysmem heap after high physical address. */
	extern char _end;
	int n = brk(max((void *)max_phys + 1, (void *)&_end + 1));
	if(n != 0) { log_crit("brk failed: %s", strerror(errno)); abort(); }
	/* copy temp_phys for caller */
	*n_phys_p = n_phys;
	L4_Fpage_t *phys = memdup(temp_phys, sizeof *phys * n_phys); if(phys == NULL) { log_crit("memdup failed"); abort(); }
	init_phys(phys, n_phys);
	return phys;
}
