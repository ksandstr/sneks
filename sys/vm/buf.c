#include <stdlib.h>
#include <nbsl.h>
#include <epoch.h>
#include <ccan/compiler/compiler.h>
#include <ccan/container_of/container_of.h>
#include <sneks/systask.h>
#include "defs.h"

int bufcache_size_log2;
struct nbsl *bufcache = NULL;

static size_t bchash(int blkid, uint64_t offset) {
	return hash_cached_page(blkid, offset & 0xffffffff, offset >> 32);
}

static struct nbsl *bcbucket(size_t hash) {
	return &bufcache[hash & ((1ul << bufcache_size_log2) - 1)];
}

/* NOTE: this is very similar to find_cached_page(). */
struct pl *bufcache_push(struct nbsl_node *top, struct pl *page)
{
	assert(e_inside());
	struct nbsl *bucket = bcbucket(bchash(page->buf.blkid, page->buf.blkoffset));
	do {
		struct nbsl_node *newtop = nbsl_top(bucket);
		if(newtop != top) {
			/* examine nodes that appeared between @top and `newtop'. */
			struct nbsl_iter it;
			for(struct nbsl_node *cur = nbsl_first(bucket, &it); cur != NULL && cur != top; cur = nbsl_next(bucket, &it)) {
				struct pl *cand = container_of(cur, struct pl, nn);
				if(atomic_load_explicit(&cand->status, memory_order_relaxed) == PL_DEAD) continue;
				if(cand->buf.blkid == page->buf.blkid && cand->buf.blkoffset == page->buf.blkoffset) return cand;
			}
			top = newtop;
		}
	} while(!nbsl_push(bucket, top, &page->nn));
	return page;
}

struct pl *bufcache_get(struct nbsl_node **top_p, int blkid, uint64_t offset)
{
	assert(e_inside());
	size_t hash = bchash(blkid, offset);
	struct nbsl *bucket = bcbucket(hash);
	struct nbsl_iter it;
	struct nbsl_node *dummy; if(top_p == NULL) top_p = &dummy;
	for(struct nbsl_node *cur = *top_p = nbsl_first(bucket, &it); cur != NULL; cur = nbsl_next(bucket, &it)) {
		struct pl *cand = container_of(cur, struct pl, nn);
		if(atomic_load_explicit(&cand->status, memory_order_acquire) != PL_DEAD && cand->buf.blkid == blkid && cand->buf.blkoffset == offset) return cand;
	}
	return NULL;
}

void bufcache_purge(int blkid) {
	/* TODO: mark @blkid for immediate release on replacement, remove mark
	 * after full cycle
	 */
}

COLD void init_bufcache(size_t n_pages) {
	bufcache_size_log2 = size_to_shift(n_pages / 24);
	bufcache = malloc(sizeof *bufcache * bufcache_size); if(bufcache == NULL) { log_crit("bufcache malloc failed for size=%zu", bufcache_size); abort(); }
	for(int i=0; i < bufcache_size; i++) nbsl_init(&bufcache[i]);
}
