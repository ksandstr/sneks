/* TODO: needs a think about data access. */
#define BLOCKIMPL_IMPL_SOURCE
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <threads.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sysmacros.h>
#include <muidl.h>
#include <epoch.h>
#include <ccan/minmax/minmax.h>
#include <ccan/compiler/compiler.h>
#include <ccan/htable/htable.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <sneks/hash.h>
#include <sneks/systask.h>
#include <sneks/io.h>
#include <sneks/rollback.h>
#include <sneks/bitops.h>
#include <sneks/block.h>
#include <sneks/api/io-defs.h>
#include <sneks/sys/devno-defs.h>
#include <sneks/sys/info-defs.h>
#include "vm-impl-defs.h"
#include "defs.h"

#define SEC_MASK(inst) ((1ul << (inst)->sec_size_log2) - 1)

struct blk_inst {
	const struct blk_class *cls;
	blk_t *self;
	unsigned object, files, sec_size_log2, blkid;
	off_t num_sectors;
	bool detached;
	char priv[] __attribute__((aligned(16)));
};

struct io_file_impl {
	const struct blk_class *cls;
	blk_t *inst;
	off_t offset, prev_offset;
};

static void destroy_blk(blk_t *);
static size_t rehash_blk_inst(const void *, void *);

static mtx_t pubs_mtx;
static struct htable pubs = HTABLE_INITIALIZER(pubs, &rehash_blk_inst, NULL);
static L4_ThreadId_t block_tid;
static int next_blkid = 1;

static size_t rehash_blk_inst(const void *inst, void *priv UNUSED) {
	const blk_t *b = inst;
	assert(b->self == b);
	return int_hash(b->object);
}

static bool cmp_blk_to_object(const void *cand, void *key) {
	return ((const blk_t *)cand)->object == *(unsigned *)key;
}

blk_t *blk_new(struct blk_class *cls, off_t size)
{
	assert(size > 0);
	assert(cls->sec_size >= 512); assert((cls->sec_size & (cls->sec_size - 1)) == 0);
	struct blk_inst *inst = malloc(cls->priv_size + sizeof *inst);
	if(inst == NULL) return NULL;
	*inst = (blk_t){ .cls = cls, .self = inst, .num_sectors = size, .sec_size_log2 = size_to_shift(cls->sec_size) };
	memset(inst->priv, '\0', cls->priv_size);
	return inst;
}

void blk_undo_new(blk_t *b) {
	assert(b->object == 0);
	free(b);
}

const struct blk_class *blk_class_of(blk_t *b) {
	assert(b->self == b);
	return b->cls;
}

void *blk_priv_of(blk_t *b) {
	assert(b->self == b);
	return b->priv;
}

blk_t *blk_of_priv(void *priv) {
	return priv - offsetof(struct blk_inst, priv);
}

static int locked_publish(blk_t *b, unsigned object) {
	if(b->object != 0) return -EINVAL;
	b->object = object;
	if(next_blkid == INT_MAX) { log_crit("ran out of blkids!"); abort(); } /* 1/s for 68 years tho */
	b->blkid = next_blkid++;
	if(!htable_add(&pubs, rehash_blk_inst(b, NULL), b)) { b->object = 0; return -ENOMEM; }
	return 0;
}

int blk_publish(blk_t *b, dev_t object)
{
	mtx_lock(&pubs_mtx);
	int n = locked_publish(b, object);
	mtx_unlock(&pubs_mtx);
	object &= ~0xc0000000;
	if(n == 0 && (n = devnoctl('b', major(object), minor(object), block_tid), n != 0)) {
		log_err("devnoctl of %u:%u failed: %s", major(object), minor(object), stripcerr(n));
		mtx_lock(&pubs_mtx);
		b->object = 0;
		htable_del(&pubs, rehash_blk_inst(b, NULL), b);
		mtx_unlock(&pubs_mtx);
	}
	return n;
}

void blk_detach(blk_t *b)
{
	assert(!b->detached);
	if(b->files == 0) destroy_blk(b);
	else {
		mtx_lock(&pubs_mtx);
		htable_del(&pubs, rehash_blk_inst(b, NULL), b);
		b->detached = true;
		mtx_unlock(&pubs_mtx);
	}
}

static int call_destroy(void *param)
{
	blk_t *b = param;
	mtx_lock(&pubs_mtx);
	if(!b->detached) htable_del(&pubs, rehash_blk_inst(b, NULL), b);
	mtx_unlock(&pubs_mtx);
	int n = (*b->cls->close)(blk_priv_of(b));
	if(n != 0) log_err("closing %sblock device %#x=`%s' returned n=%d: %s", b->detached ? "detached " : "", b->object, b->cls->name, n, stripcerr(n));
	else {
		bufcache_purge(b->blkid);
		if(b->cls->destroy != NULL) (*b->cls->destroy)(blk_priv_of(b));
		free(b);
	}
	return n;
}

static void destroy_blk(blk_t *b) {
	/* TODO: do fancy dtor count controls here */
	thrd_t dtor;
	if(thrd_create(&dtor, &call_destroy, b) != thrd_success || thrd_detach(dtor) != thrd_success) {
		log_crit("destroy_blk: can't launch thread!"); abort();
	}
}

static int locked_open_blk(unsigned object, int flags, pid_t caller)
{
	blk_t *b = htable_get(&pubs, int_hash(object), &cmp_blk_to_object, &object);
	if(b == NULL) return -EINVAL;
	iof_t *f = iof_new(0); if(f == NULL) return -ENOMEM;
	*f = (struct io_file_impl){ .cls = b->cls, .inst = b };
	int fd = io_add_fd(caller, f, 0);
	if(fd < 0) iof_undo_new(f); else b->files++;
	return fd;
}

static int open_blk(int *fd_p, unsigned object, L4_Word_t cookie, int flags)
{
	sync_confirm();
	if(flags != 0) return -EINVAL; /* dirty? */
	pid_t caller = pidof_NP(muidl_get_sender());
	/* TODO: validate @cookie */
	mtx_lock(&pubs_mtx);
	int n = locked_open_blk(object, flags, caller);
	mtx_unlock(&pubs_mtx);
	if(n >= 0) *fd_p = n;
	return n;
}

static void undo_seek_blk(L4_Word_t unused, iof_t *f) {
	f->offset = f->prev_offset;
}

static int seek_blk(int fd, off_t *offset_ptr, int whence)
{
	sync_confirm();
	pid_t caller = pidof_NP(muidl_get_sender());
	iof_t *f = io_get_file(caller, fd); if(f == NULL) return -EBADF;
	switch(whence) {
		case SEEK_SET: break;
		case SEEK_CUR: *offset_ptr += f->offset; break;
		case SEEK_END: *offset_ptr = max_t(off_t, 0, (f->inst->num_sectors << f->inst->sec_size_log2) - *offset_ptr); break;
		default: return -EINVAL;
	}
	f->prev_offset = f->offset; f->offset = *offset_ptr;
	set_rollback(&undo_seek_blk, 0, f);
	return 0;
}

static int close_blk(iof_t *f) {
	assert(f->inst->files > 0);
	if(--f->inst->files == 0 && f->inst->detached) destroy_blk(f->inst);
	return 0;
}

/* always comfy chairin' */
static int fetch_page(struct pl **linkp, blk_t *blk, uint64_t pagenum, bool for_write)
{
	if(for_write) return -EROFS; /* TODO: mark pl2pp(link) dirty */
	struct nbsl_node *top;
	struct pl *cached = bufcache_get(&top, blk->blkid, pagenum);
	if(cached == NULL) {
		struct pl *alloc = get_free_pl(), *newlink;
		int n, secdiff = PAGE_BITS - blk->sec_size_log2;
		if(secdiff < 0) { log_crit("can't hack sectors above %d bytes", 1 << PAGE_BITS); abort(); } /* FIXME: learn hax */
		if(n = (*blk->cls->read)(pl2ptr(alloc), pagenum << secdiff, 1 << secdiff, blk_priv_of(blk)), n < 0) { unget_free_pl(alloc); return n; }
		if(newlink = malloc(sizeof *newlink), newlink == NULL) { unget_free_pl(alloc); return -ENOMEM; }
		*newlink = (struct pl){ .page_num = alloc->page_num, .status = PL_ALIVE, .buf.blkid = blk->blkid, .buf.blkoffset = pagenum };
		if(cached = bufcache_push(top, newlink), cached != newlink) { unget_free_pl(alloc); free(newlink); }
	}
	*linkp = cached;
	return 0;
}

static int cached_read(blk_t *blk, uint8_t *buf, off64_t pos, int count)
{
	int done = 0, n, eck = e_begin();
	do {
		struct pl *page; if(n = fetch_page(&page, blk, pos >> PAGE_BITS, false), n < 0) goto fail;
		int step = pos & PAGE_MASK, seg = min_t(int, count - done, PAGE_SIZE - step);
		memcpy(buf + done, pl2ptr(page) + step, seg);
		done += seg; pos += seg;
	} while(done < count);
	e_end(eck);
	return done;
fail: e_end(eck); return n;
}

/* TODO: handle reads past EOF */
static int read_blk(iof_t *f, uint8_t *ioseg, unsigned count, off_t offset) {
	return cached_read(f->inst, ioseg, offset < 0 ? f->offset : offset, min_t(unsigned, SNEKS_IO_IOSEG_MAX, count));
}

static int write_blk(iof_t *f, const uint8_t *buf, unsigned count, off_t offset) {
	return -ENOSYS;
}

static void confirm_blk(iof_t *f, unsigned count, off_t offset, bool writing) {
	if(offset < 0) f->offset += count;
}

int block_main(void *priv UNUSED)
{
	mtx_init(&pubs_mtx, mtx_plain);
	block_tid = L4_MyGlobalId();
	struct block_impl_vtable vtab = {
		/* Sneks::File */
		.open = &open_blk, .seek = &seek_blk,
		/* Sneks::DeviceControl */
		.ioctl_void = &enosys, .ioctl_int = &enosys,
	};
	FILL_SNEKS_IO(&vtab);
	io_read_func(&read_blk);
	io_write_func(&write_blk);
	io_confirm_func(&confirm_blk);
	io_close_func(&close_blk);
	io_dispatch_func(&_muidl_block_impl_dispatch, &vtab);
	return io_run(sizeof(struct io_file_impl), 1, (char *[]){ "block", NULL });
}
