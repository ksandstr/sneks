/* systemspace POSIX-like virtual memory server.
 *
 * note that this is currently a single-threaded implementation, with some
 * lf/wf primitives mixed into the design even when they're not being used.
 * the idea is that once mung gets MP and muidl becomes multithreaded,
 * reworking vm to behave regardless of concurrency will be that much of a
 * smaller job, and that using epochs before MP should expose problems with
 * collection speed, usage issues, etc.
 */

#define VMIMPL_IMPL_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <string.h>
#include <limits.h>
#include <assert.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <threads.h>
#include <sys/mman.h>
#include <nbsl.h>
#include <epoch.h>
#include <muidl.h>
#include <ccan/likely/likely.h>
#include <ccan/compiler/compiler.h>
#include <ccan/minmax/minmax.h>
#include <ccan/array_size/array_size.h>
#include <ccan/container_of/container_of.h>
#include <ccan/bitmap/bitmap.h>

#include <l4/types.h>
#include <l4/ipc.h>
#include <l4/space.h>
#include <l4/kip.h>

#include <ukernel/rangealloc.h>
#include <ukernel/memdesc.h>
#include <sneks/mm.h>
#include <sneks/bitops.h>
#include <sneks/rbtree.h>
#include <sneks/process.h>
#include <sneks/hash.h>
#include <sneks/sysinfo.h>
#include <sneks/sanity.h>
#include <sneks/systask.h>
#include <sneks/sys/info-defs.h>
#include <sneks/api/proc-defs.h>
#include <sneks/api/file-defs.h>
#include <sneks/api/io-defs.h>

#include "defs.h"
#include "space.h"
#include "vm-impl-defs.h"

static _Atomic unsigned long next_anon_ino = 1;

#ifdef DEBUG_ME_HARDER
#include <sneks/invariant.h>
extern bool all_invariants(void)
{
	INV_CTX;
	int eck = e_begin();
	inv_ok1(frame_invariants(INV_CHILD));
	inv_ok1(page_invariants(INV_CHILD));
	e_end(eck);
	return true;
inv_fail: e_end(eck); return false;
}
#endif

static inline int prot_to_l4_rights(int prot)
{
	int m = (prot & PROT_READ) << 2
		| (prot & PROT_WRITE)
		| (prot & PROT_EXEC) >> 2;
	assert(!!(m & L4_Readable) == !!(prot & PROT_READ));
	assert(!!(m & L4_Writable) == !!(prot & PROT_WRITE));
	assert(!!(m & L4_eXecutable) == !!(prot & PROT_EXEC));
	return m;
}


static int vm_mmap(
	pid_t target_pid, L4_Word_t *addr_ptr, L4_Word_t length,
	int prot, int flags,
	L4_Word_t fd_serv, int fd, off_t offset)
{
	assert(all_invariants());

	if(length == 0) return -EINVAL;
	if((*addr_ptr | offset) & PAGE_MASK) return -EINVAL;
	if((flags & MAP_PRIVATE) && (flags & MAP_SHARED)) return -EINVAL;
	if((~flags & MAP_PRIVATE) && (~flags & MAP_SHARED)) return -EINVAL;

	int sender_pid = pidof_NP(muidl_get_sender());
	if(target_pid == 0) target_pid = sender_pid;
	if(target_pid > SNEKS_MAX_PID || target_pid == 0) return -EINVAL;

	/* TODO: validate target_pid to be either userspace caller's PID, or that
	 * the caller is a systask (and actual sender).
	 */
	struct vm_space *sp = get_space(target_pid); if(sp == NULL) return -EINVAL;

	struct lazy_mmap *mm = malloc(sizeof *mm);
	if(mm == NULL) return -ENOMEM;
	int n;
	if((flags & MAP_ANONYMOUS) && (flags & MAP_SHARED)) {
		/* (error if fd_serv != nil?) */
		fd_serv = L4_MyGlobalId().raw;
		fd = atomic_fetch_add(&next_anon_ino, 1);
		offset = 0;
	} else if((~flags & MAP_ANONYMOUS) && fd_serv != L4_nilthread.raw) {
		/* touch-a ma spaghetti */
		n = __io_touch((L4_ThreadId_t){ .raw = fd_serv }, fd);
		if(n != 0) {
			free(mm);
			if(n > 0) n = -EIO;
			goto end;
		}
	}
	*mm = (struct lazy_mmap){
		.flags = prot_to_l4_rights(prot) << 16 | (flags & ~MAP_FIXED),
		.fd_serv.raw = fd_serv, .ino = fd, .offset = offset >> PAGE_BITS,
		.tailsz = length % PAGE_SIZE,
	};
	int eck = e_begin();
	n = reserve_mmap(mm, sp, *addr_ptr, PAGE_CEIL(length), !!(flags & MAP_FIXED));
	e_end(eck);
	if(n < 0) {
		free(mm);
		goto end;
	}

	assert(~mm->flags & MAP_FIXED);
	*addr_ptr = mm->addr;

end:
	assert(all_invariants());
	return n;
}


static int vm_brk(L4_Word_t addr)
{
	assert(all_invariants());

	int sender_pid = pidof_NP(muidl_get_sender());
	if(unlikely(sender_pid >= SNEKS_MAX_PID)) return -EINVAL;
	struct vm_space *sp = get_space(sender_pid); assert(sp != NULL);

	if(addr < 0x10000) addr = 0x10000;
	if(addr > user_addr_max) addr = user_addr_max;
	addr = PAGE_CEIL(addr);

	int eck = e_begin();
	if(sp->brk == 0) {
		/* do nothing! */
	} else if(sp->brk < addr) {
		/* add memory */
		struct lazy_mmap *mm = find_lazy_mmap(sp, sp->brk);
		if(mm != NULL && mm->addr + mm->length == sp->brk
			&& (mm->flags & MAP_ANONYMOUS) && (mm->flags & MAP_PRIVATE))
		{
			/* extend previous map iff there's no right-hand neighbour or the
			 * neighbour doesn't overlap.
			 */
			struct rb_node *next = __rb_next(&mm->rb);
			if(next != NULL && rb_entry(next, struct lazy_mmap, rb)->addr < addr) {
				mm = NULL;	/* nope. */
			} else {
				/* here's your brk fastpath */
				mm->length = addr - mm->addr;
				assert(find_lazy_mmap(sp, addr) == mm);
			}
		} else {
			mm = malloc(sizeof *mm);
			if(mm == NULL) return -ENOMEM;
			*mm = (struct lazy_mmap){
				.flags = L4_FullyAccessible << 16 | (MAP_ANONYMOUS | MAP_PRIVATE),
			};
			int n = reserve_mmap(mm, sp, sp->brk, addr - sp->brk, true);
			if(n < 0) { free(mm); return n; }
		}
	} else {
		/* remove memory */
		struct lazy_mmap *mm = find_lazy_mmap(sp, sp->brk - 1);
		if(mm != NULL && mm->addr < addr && mm->addr + mm->length == sp->brk
			&& (mm->flags & MAP_ANONYMOUS))
		{
			/* can reduce an existing map that looks enough like our own
			 * brk-heap map. here's your other brk fastpath.
			 */
			mm->length = addr - mm->addr;
			remove_vp_range(sp, addr, sp->brk - addr);
		} else {
			/* full unmap */
			munmap_space(sp, addr, sp->brk - addr);
		}
	}
	e_end(eck);

	sp->brk = addr;

	assert(all_invariants());
	return 0;
}

static int vm_erase(pid_t target_pid)
{
	assert(all_invariants());
	int sender_pid = pidof_NP(muidl_get_sender());
	if(unlikely(sender_pid < SNEKS_MIN_SYSID)) return -EPERM;
	struct vm_space *sp = get_space(target_pid); if(sp == NULL) return -EINVAL;
	int eck = e_begin(), n_fps = 0;
	L4_Fpage_t fps[64];
	struct htable_iter it;
	for(struct vp_group *g = htable_first(&sp->vpgs, &it); g != NULL; g = htable_next(&sp->vpgs, &it)) {
		for(int i = 0; i < VPG_SIZE; i++) {
			struct vp *v = &g->pages[i];
			if(v->status == 0) continue;
			assert(~v->status & 0x80000000);
			fps[n_fps] = L4_FpageLog2(v->status << PAGE_BITS, PAGE_BITS);
			L4_Set_Rights(&fps[n_fps], L4_FullyAccessible);
			if(++n_fps == ARRAY_SIZE(fps)) {
				L4_UnmapFpages(ARRAY_SIZE(fps), fps);
				n_fps = 0;
			}
			remove_vp(v);
			v->status = 0;
			if(--g->addr, VP_COUNT(g) == 0) {
				htable_delval(&sp->vpgs, &it);
				e_free(g);
				break;
			}
		}
	}
	if(n_fps > 0) L4_UnmapFpages(n_fps, fps);
	htable_clear(&sp->vpgs);
	free_space(sp);
	e_end(eck);
	assert(all_invariants());
	return 0;
}

static int vm_munmap(L4_Word_t addr, L4_Word_t size)
{
	pid_t sender_pid = pidof_NP(muidl_get_sender());
	if(sender_pid == 0 || sender_pid > SNEKS_MAX_PID) return -EINVAL;
	addr &= ~PAGE_MASK;
	size = PAGE_CEIL(size);
	if(!VALID_ADDR_SIZE(addr, size)) return -EINVAL;
	struct vm_space *sp = get_space(sender_pid); if(sp == NULL) return -EINVAL;
	int eck = e_begin(); munmap_space(sp, addr, size); e_end(eck);
	return 0;
}

static int vm_fork(pid_t srcpid, pid_t destpid)
{
	assert(all_invariants());
	if(srcpid < 0 || destpid < 0 || srcpid > SNEKS_MAX_PID || destpid > SNEKS_MAX_PID) return -EINVAL;
	if(pidof_NP(muidl_get_sender()) < SNEKS_MIN_SYSID) return -EPERM;

	struct vm_space *src = NULL;
	if(srcpid > 0 && (src = get_space(srcpid), src == NULL)) return -EINVAL;
	assert(src != NULL || srcpid == 0);

	struct vm_space *dest = ra_alloc(vm_space_ra, destpid); if(dest == NULL) return -EEXIST;
	*dest = (struct vm_space){ .vpgs = HTABLE_INITIALIZER(dest->vpgs, &hash_vpg, NULL), .maps = RB_ROOT, .as_free = RB_ROOT, .last_mmap = &no_last_mmap };
	if(src == NULL) {
		/* create an unconfigured address space for spawn. */
		dest->kip_area.raw = ~0ul;
		dest->utcb_area = L4_Nilpage;
		dest->brk = 0;
		dest->mmap_bot = user_addr_max;
	} else {
		/* fork from @src. */
		dest->kip_area = src->kip_area;
		dest->utcb_area = src->utcb_area;
		dest->sysinfo_area = src->sysinfo_area;
		dest->brk = src->brk;
		dest->mmap_bot = src->mmap_bot;
		int n = fork_maps(src, dest);
		if(n == 0) n = fork_pages(src, dest);
		if(n < 0) return n; /* FIXME: cleanup */
	}
	assert(all_invariants());
	return 0;
}

/* TODO: de-kludge SIP reception in init_protocol() so that there's exactly one
 * copy of the SIP in the system, and that's within vm. root can have a mapped
 * read-only view if necessary.
 */
static int repair_sip(struct vp *vp, const struct lazy_mmap *mm, size_t faddr, int rwx)
{
	assert(e_inside());
	vp->vaddr |= VPF_ANON | L4_Readable;
	static size_t sip_frame = 0;
	if(unlikely(sip_frame == 0)) {
		assert(the_sip->magic == SNEKS_SYSINFO_MAGIC);
		struct pl *sip_link = get_free_pl(); /* FIXME: pin the page so it's ignored by replacement; or have a callback to clear sip_frame when removed. */
		memcpy(pl2ptr(sip_link), the_sip, PAGE_SIZE);
		sip_frame = sip_link->page_num; assert(sip_frame != 0);
		vp->status = sip_frame;
		sip_link->fsid_ino = ~(uint64_t)0; /* for remove_vp() */
		atomic_store_explicit(&pl2pp(sip_link)->owner, vp, memory_order_relaxed);
		push_page(&page_active_list, sip_link);
		e_free(sip_link);
	} else {
		assert(get_pp(sip_frame)->owner != NULL || has_shares(int_hash(sip_frame), sip_frame, 1));
		vp->status = sip_frame;
		int n = add_share(sip_frame, vp);
		if(n < 0) return n;
	}
	assert(MMAP_RIGHTS(mm) == L4_Readable);
	return L4_Readable;
}

static int vm_configure(L4_Word_t *last_resv_p, pid_t pid, L4_Fpage_t utcb, L4_Fpage_t kip)
{
	assert(all_invariants());
	if(pid > SNEKS_MAX_PID) return -EINVAL;
	struct vm_space *sp = get_space(pid); if(sp == NULL) return -EINVAL;
	if(sp->kip_area.raw != ~0ul || !L4_IsNilFpage(sp->utcb_area)) return -EINVAL; /* TODO: should be "illegal state" */
	L4_Fpage_t si = L4_FpageLog2(L4_Address(kip) + L4_Size(kip), PAGE_BITS);
	if(fpage_overlap(kip, utcb) || fpage_overlap(si, utcb)) return -EINVAL;

	int eck = e_begin(), n;
	sp->kip_area = kip; sp->utcb_area = utcb; sp->sysinfo_area = si;
	/* SIP area */
	struct lazy_mmap *sipmm = malloc(sizeof *sipmm); if(sipmm == NULL) goto Enomem;
	*sipmm = (struct lazy_mmap){ .flags = L4_Readable << 16 | MAP_SHARED | MAP_ANONYMOUS | MAP_SYSTEM, .repair_fn = &repair_sip };
	if(n = reserve_mmap(sipmm, sp, L4_Address(sp->sysinfo_area), L4_Size(sp->sysinfo_area), true), n < 0) { free(sipmm); goto fail; }
	/* anonymous memory in [0x10000, min(kip.start, utcb.start, si.start)). */
	uintptr_t resv_low = min(L4_Address(sp->kip_area), min(L4_Address(sp->utcb_area), L4_Address(sp->sysinfo_area)));
	if(resv_low <= 0x10000) goto Einval;
	struct lazy_mmap *lowmem = malloc(sizeof *lowmem); if(lowmem == NULL) { /* FIXME: clean up! */ abort(); goto Enomem; }
	*lowmem = (struct lazy_mmap){ .flags = L4_FullyAccessible << 16 | MAP_PRIVATE | MAP_ANONYMOUS };
	if(n = reserve_mmap(lowmem, sp, 0x10000, resv_low - 0x10000, true), n < 0) { /* FIXME: clean up! */ abort(); goto fail; }
	assert(all_invariants());
	e_end(eck);

	*last_resv_p = max(L4_Address(si) + L4_Size(si), L4_Address(utcb) + L4_Size(utcb)) - 1;
	return 0;
fail: e_end(eck); return n;
Einval: n = -EINVAL; goto fail;
Enomem: n = -ENOMEM; goto fail;
}

static int vm_upload_page(pid_t target_pid, L4_Word_t addr, const uint8_t *data, unsigned data_len, int prot, int flags)
{
	assert(all_invariants());
	struct vm_space *sp = get_space(target_pid); if(sp == NULL) return -EINVAL;
	if((addr & PAGE_MASK) != 0) return -EINVAL;
	if(~flags & MAP_ANONYMOUS) return -EINVAL;	/* can't specify file */
	if(~flags & MAP_FIXED) return -EINVAL;		/* per spec */
	if(prot == 0) return -EINVAL;
	if(flags & MAP_SHARED) return -ENOSYS;	/* FIXME when required */
	if(~flags & MAP_PRIVATE) return -EINVAL;/* ^- wew lad */

	int rights = prot_to_l4_rights(prot);
	struct lazy_mmap *mm = malloc(sizeof *mm); if(mm == NULL) return -ENOMEM;
	*mm = (struct lazy_mmap){ .flags = rights << 16 | (flags & ~MAP_FIXED) };
	int eck = e_begin(), n;
	if(n = reserve_mmap(mm, sp, addr, PAGE_SIZE, true), n < 0) { free(mm); e_end(eck); return n; }
	assert(mm->addr == addr); assert(mm->length == PAGE_SIZE); assert(~mm->flags & MAP_FIXED);

	struct vp_iter it = { };
	struct vp *v = get_vp(sp, &it, addr);
	if(v != NULL) remove_vp(v); else if(v = add_vp(sp, &it, addr), v == NULL) { e_end(eck); return -ENOMEM; }
	*v = (struct vp){ .vaddr = addr | rights | VPF_ANON };
	assert(VP_RIGHTS(v) == prot_to_l4_rights(prot));

	struct pl *link = get_free_pl();
	memcpy(pl2ptr(link), data, data_len);
	if(data_len < PAGE_SIZE) memset(pl2ptr(link) + data_len, '\0', PAGE_SIZE - data_len);
	push_page(&page_active_list, link);
	e_free(link);
	v->status = (L4_Word_t)pl2ptr(link) >> PAGE_BITS;
	atomic_store_explicit(&get_pp(v->status)->owner, v, memory_order_relaxed);
	e_end(eck);

	assert(all_invariants());
	return 0;
}

static void vm_breath_of_life(L4_Word_t *rc_p, L4_Word_t target_raw, L4_Word_t sp, L4_Word_t ip) {
	L4_LoadMR(0, (L4_MsgTag_t){ .X.u = 2 }.raw); L4_LoadMR(1, ip); L4_LoadMR(2, sp);
	L4_MsgTag_t tag = L4_Reply((L4_ThreadId_t){ .raw = target_raw });
	*rc_p = L4_IpcFailed(tag) ? L4_ErrorCode() : 0;
}

static void vm_iopf(L4_Fpage_t fault, L4_Word_t fip, L4_MapItem_t *page_ptr)
{
	assert(all_invariants());
	/* userspace tasks can't have I/O ports. at all. it is verboten. */
	log_info("IO fault from pid=%d, ip=%#lx, port=%#lx:%#lx", pidof_NP(muidl_get_sender()), fip, L4_IoFpagePort(fault), L4_IoFpageSize(fault));
	muidl_raise_no_reply();
}

static void vm_pf(L4_Word_t faddr, L4_Word_t fip, L4_MapItem_t *map_out)
{
	L4_ThreadId_t sender = muidl_get_sender();
	pid_t pid = pidof_NP(sender);
	const int fault_rwx = L4_Label(muidl_get_tag()) & 7;
#if 0
	log_info("pid=%d, faddr=%#lx, fip=%#lx, [%c%c%c]", pid, faddr, fip,
		(fault_rwx & L4_Readable) ? 'r' : '-', (fault_rwx & L4_Writable) ? 'w' : '-', (fault_rwx & L4_eXecutable) ? 'x' : '-');
#endif
	if(unlikely(pid > SNEKS_MAX_PID)) {
		log_err("tid=%lu:%lu faulted into invalid space pid=%d", L4_ThreadNo(sender), L4_Version(sender), pid);
		goto noreply;
	}
	struct vm_space *sp = get_space(pid);
	if(unlikely(sp == NULL)) {
		log_err("tid=%lu:%lu faulted into uninitialized space pid=%d", L4_ThreadNo(sender), L4_Version(sender), pid);
noreply:muidl_raise_no_reply();
		return;
	}

	int eck = e_begin();
	assert(all_invariants());
	L4_Fpage_t map_page;
	int n = fault_vp(&map_page, sp, faddr, fault_rwx);
	if(n == 0) n = fault_map(&map_page, sp, faddr, fault_rwx);
	if(n > 0) {
		*map_out = L4_MapItem(map_page, faddr & ~PAGE_MASK);
	} else if(n < 0) {
		log_info("segfault in pid=%d at faddr=%#lx fip=%#lx", pid, faddr, fip);
		if(n = __proc_kill(__uapi_tid, pid, SIGSEGV), n != 0) log_err("Proc/kill failed: %s", stripcerr(n));
		muidl_raise_no_reply();
	}
	assert(all_invariants());
	e_end(eck);
}

static COLD int vm_initialize(L4_ThreadId_t *init_tid_p)
{
	int n_phys = 0, eck = e_begin();
	L4_Fpage_t *phys = init_protocol(&n_phys, init_tid_p);
	assert(all_invariants());
	size_t total_pages = 0;
	for(int i=0; i < n_phys; i++) total_pages += L4_Size(phys[i]) / PAGE_SIZE;
	init_pagecache(total_pages);
	assert(all_invariants());
	init_bufcache(total_pages);
	assert(all_invariants());
	free(phys);
	e_end(eck);

	init_space();
	assert(all_invariants());

	static const int (*subfns[])(void *) = { &block_main, &loop_ctl_main };
	for(int i=0; i < ARRAY_SIZE(subfns); i++) {
		thrd_t t; if(thrd_create(&t, subfns[i], NULL) != thrd_success || thrd_detach(t) != thrd_success) return -ENOMEM;
	}
	return 0;
}

/* TODO: deduplicate w/ the one in sys/io/io.c */
static void prepend_log_string(struct hook *h, void *strptr_ptr, uintptr_t level, const char *my_name) {
	char **spp = strptr_ptr, *new;
	if(asprintf(&new, "%s:%s", my_name, *spp) > 0) { free(*spp); *spp = new; }
}

int main(int argc, char *argv[])
{
	hook_push_back(log_hook(), &prepend_log_string, "vm");
	log_info("hello!");
	L4_ThreadId_t init_tid;
	int n = vm_initialize(&init_tid);
	if(n == 0) L4_LoadMR(0, 0);
	else {
		log_info("initialization failed: %s", stripcerr(n));
		L4_LoadMR(0, (L4_MsgTag_t){ .X.label = 1, .X.u = 1 }.raw);
		L4_LoadMR(1, n);
	}
	if(L4_IpcFailed(L4_Reply(init_tid))) {
		log_crit("reply to init_tid=%lu:%lu failed: %s", L4_ThreadNo(init_tid), L4_Version(init_tid), stripcerr(L4_ErrorCode()));
		abort();
	} else if(n != 0) {
		return EXIT_FAILURE;
	}

	static const struct vm_impl_vtable vtab = {
		/* Sneks::VM */
		.mmap = &vm_mmap, .munmap = &vm_munmap, .fork = &vm_fork, .brk = &vm_brk,
		.configure = &vm_configure, .upload_page = &vm_upload_page, .breath_of_life = &vm_breath_of_life, .erase = &vm_erase,
		/* L4X2::FaultHandler */
		.handle_fault = &vm_pf,
		/* L4X2::X86IOFaultHandler */
		.handle_x86_io_fault = &vm_iopf,
	};
	for(;;) {
		L4_Word_t status = _muidl_vm_impl_dispatch(&vtab);
		if(status != 0 && !MUIDL_IS_L4_ERROR(status)) {
			log_info("dispatch status %#lx (last tag %#lx)", status, muidl_get_tag().raw);
			assert(all_invariants());
		}
	}
	return 0;
}
