#include <stddef.h>
#include <stdint.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <epoch.h>
#include <ccan/array_size/array_size.h>
#include <ccan/container_of/container_of.h>
#include <ccan/compiler/compiler.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <ukernel/rangealloc.h>
#include <ukernel/memdesc.h>
#include <sneks/hash.h>
#include <sneks/rbtree.h>
#include <sneks/sanity.h>
#include <sneks/systask.h>
#include "space.h"
#include "defs.h"

/* chunks of free address space in vm_space.as_free */
struct as_free {
	struct rb_node rb;
	L4_Fpage_t fp;
};

struct rangealloc *vm_space_ra;
size_t user_addr_max;
struct lazy_mmap no_last_mmap = { };

/* similar but for ino of shared anonymous memory, i.e. PL_IS_ANON() of links
 * found in the page cache. mandatory for IS_ANON_MMAP(), forbidden to every
 * other lazy_mmap.
 */
struct htable anon_mmap_table = HTABLE_INITIALIZER(anon_mmap_table, &hash_lazy_mmap_by_ino, NULL);

size_t hash_lazy_mmap_by_ino(const void *ptr, void *priv) {
	const struct lazy_mmap *mm = ptr;
	return int_hash(mm->ino & 0xffffffff) ^ int_hash(mm->ino >> 32);
}

struct vm_space *get_space(pid_t pid)
{
#ifdef NDEBUG
	struct vm_space *sp = ra_id2ptr(vm_space_ra, pid);
	return L4_IsNilFpage(sp->kip_area) ? NULL : sp;
#else
	struct vm_space *sp = ra_id2ptr_safe(vm_space_ra, pid);
	assert(sp != NULL);
	return L4_IsNilFpage(sp->kip_area) ? NULL : sp;
#endif
}

#ifdef DEBUG_ME_HARDER
#include <sneks/invariant.h>

/* TODO: deduplicate w/ vm.c */
static int cmp_ptrs(const void *a, const void *b) {
	return *(const void **)a - *(const void **)b;
}

static bool space_invariants(INVCTX_ARG, struct vm_space *sp, struct vp *const *all_vps, size_t n_all_vps)
{
	assert(vm_space_ra != NULL);
	inv_push("sp=%d, brk=%#x", ra_ptr2id(vm_space_ra, sp), sp->brk);
	inv_log("kip_area=%#lx:%#lx, utcb_area=%#lx:%#lx, sysinfo_area=%#lx:%#lx",
		L4_Address(sp->kip_area), L4_Size(sp->kip_area),
		L4_Address(sp->utcb_area), L4_Size(sp->utcb_area),
		L4_Address(sp->sysinfo_area), L4_Size(sp->sysinfo_area));
	inv_ok1(!fpage_overlap(sp->kip_area, sp->utcb_area));
	inv_ok1(!fpage_overlap(sp->kip_area, sp->sysinfo_area));
	inv_ok1(!fpage_overlap(sp->utcb_area, sp->sysinfo_area));
	struct htable_iter it;
	for(const struct vp_group *g = htable_first(&sp->vpgs, &it); g != NULL; g = htable_next(&sp->vpgs, &it)) {
		inv_push("g=%p, ->addr=%#zx", g, g->addr);
		for(const struct vp *vp = g->pages; vp < &g->pages[VPG_SIZE]; vp++) {
			if(vp->status == 0) continue;
			inv_push("vp=%p, ->vaddr=%#zx, ->status=%zu", vp, (size_t)vp->vaddr, (size_t)vp->status);
			inv_ok(bsearch(&vp, all_vps, n_all_vps, sizeof(struct vp *), &cmp_ptrs) != NULL, "vp in all_vps");
			uintptr_t vaddr = vp->vaddr & ~PAGE_MASK;
			inv_log("vaddr=%#x", vaddr);
			inv_ok(vaddr == (g->addr & ~VPG_MASK) + (vp - g->pages) * PAGE_SIZE, "vaddr consistent with group address");
			inv_ok(find_lazy_mmap(sp, vaddr) != NULL, "vaddr within lazy_mmap");
			inv_ok(!fpage_overlap(sp->kip_area, L4_FpageLog2(vaddr, PAGE_BITS)), "doesn't overlap KIP area");
			inv_ok(!fpage_overlap(sp->utcb_area, L4_FpageLog2(vaddr, PAGE_BITS)), "doesn't overlap UTCB area");
			inv_pop();
		}
		inv_pop();
	}
	RB_FOREACH(rb_iter, &sp->maps) {
		struct lazy_mmap *mm = rb_entry(rb_iter, struct lazy_mmap, rb);
		inv_push("lazy_mmap addr=%#x length=%#x", mm->addr, mm->length);
		inv_ok1(mm->tailsz <= PAGE_SIZE);
		inv_pop();
	}
	inv_pop();
	return true;
inv_fail: return false;
}

bool all_space_invariants(INVCTX_ARG, struct vp *const *all_vps, size_t n_all_vps)
{
	assert(vm_space_ra != NULL);
	struct ra_iter space_iter;
	for(struct vm_space *sp = ra_first(vm_space_ra, &space_iter); sp != NULL; sp = ra_next(vm_space_ra, &space_iter)) {
		if(L4_IsNilFpage(sp->kip_area) || L4_IsNilFpage(sp->utcb_area)) continue; /* invalid or uninitialized */
		inv_ok1(space_invariants(INV_CHILD, sp, all_vps, n_all_vps));
	}
	return true;
inv_fail: return false;
}
#endif

static struct lazy_mmap *insert_lazy_mmap_helper(struct rb_root *root, struct lazy_mmap *mm)
{
	struct rb_node **p = &root->rb_node, *parent = NULL;
	uintptr_t mm_last = mm->addr + mm->length - 1;
	while(*p != NULL) {
		parent = *p;
		struct lazy_mmap *oth = rb_entry(parent, struct lazy_mmap, rb);
		uintptr_t oth_last = oth->addr + oth->length - 1;
		if(mm_last < oth->addr) p = &(*p)->rb_left;
		else if(mm->addr > oth_last) p = &(*p)->rb_right;
		else return oth;
	}
	__rb_link_node(&mm->rb, parent, p);
	return NULL;
}

static struct lazy_mmap *insert_lazy_mmap(struct vm_space *sp, struct lazy_mmap *mm) {
	struct lazy_mmap *dupe = insert_lazy_mmap_helper(&sp->maps, mm);
	if(dupe == NULL) __rb_insert_color(&mm->rb, &sp->maps);
	return dupe;
}

struct lazy_mmap *find_lazy_mmap(struct vm_space *sp, uintptr_t addr)
{
	if(sp->last_mmap->addr <= addr && addr - sp->last_mmap->addr < sp->last_mmap->length) return sp->last_mmap;
	struct rb_node *n = sp->maps.rb_node;
	while(n != NULL) {
		struct lazy_mmap *cand = rb_entry(n, struct lazy_mmap, rb);
		if(addr < cand->addr) n = n->rb_left;
		else if(addr >= cand->addr + cand->length) n = n->rb_right;
		else { sp->last_mmap = cand; return cand; }
	}
	return NULL;
}

/* the silly bruteforce edit. */
static struct lazy_mmap *first_lazy_mmap(struct vm_space *sp, uintptr_t addr, size_t sz)
{
	assert(VALID_ADDR_SIZE(addr, sz));
	struct rb_node *n = sp->maps.rb_node;
	struct lazy_mmap *cand = NULL;
	while(n != NULL) {
		cand = rb_entry(n, struct lazy_mmap, rb);
		if(addr < cand->addr) n = n->rb_left;
		else if(addr >= cand->addr + cand->length) n = n->rb_right;
		else return cand;
	}
	/* >> */
	while(cand != NULL && addr + sz <= cand->addr) {
		cand = container_of_or_null(__rb_next(&cand->rb), struct lazy_mmap, rb);
	}
	/* << */
	while(cand != NULL && addr >= cand->addr + cand->length) {
		cand = container_of_or_null(__rb_prev(&cand->rb), struct lazy_mmap, rb);
	}
	/* \o/ */
	return cand;
}

/* remove @mm's contents from the page cache that lie between [first, last).
 * to remove it entirely, pass first=0 ∧ last=0. @mm will not be removed from
 * anon_mmap_table.
 *
 * NOTE: this should go away along with all its callsites when page
 * replacement comes in. at that point anon+shared pages in the page cache
 * will be cleaned up lazily based on anon_mmap_table, which is so cool as
 * to permit an interim bruteforce solution for passing invariant checks.
 */
static void cut_anon_mmap(struct lazy_mmap *mm, size_t hash, uintptr_t first, uintptr_t last)
{
	assert(IS_ANON_MMAP(mm));
	assert(hash == hash_lazy_mmap_by_ino(mm, NULL));

	if(last == first) last = mm->addr + mm->length - 1;
	else last = min(mm->addr + mm->length - 1, last);
	first = max(mm->addr, first);

	/* brute force. 's cool */
	for(int page=0; page < (last - first + 1) >> PAGE_BITS; page++) {
		struct nbsl_node *top;
		struct pl *link = find_cached_page(&top, mm, mm->offset + page);
		if(link == NULL) continue;

		/* is this one covered by the remaining anon mmaps on this ino? */
		bool cover = false;
		struct htable_iter it;
		for(struct lazy_mmap *oth = htable_firstval(&anon_mmap_table, &it, hash);
			oth != NULL;
			oth = htable_nextval(&anon_mmap_table, &it, hash))
		{
			if(oth->ino != mm->ino || oth == mm) continue;
			if(OVERLAP_EXCL(first + page * PAGE_SIZE, PAGE_SIZE,
				oth->addr, oth->length))
			{
				cover = true;
				break;
			}
		}

		if(!cover) {
			assert(!has_shares(int_hash(link->page_num), link->page_num, 1));
			assert(pl2pp(link)->owner == NULL);
			free_page(link);
		}
	}
}

void free_space(struct vm_space *sp)
{
	assert(htable_count(&sp->vpgs) == 0);
	RB_FOREACH_SAFE(cur, &sp->maps) {
		struct lazy_mmap *mm = rb_entry(cur, struct lazy_mmap, rb);
		if(IS_ANON_MMAP(mm)) {
			/* anonymous shared memory. */
			size_t hash = hash_lazy_mmap_by_ino(mm, NULL);
			cut_anon_mmap(mm, hash, 0, 0);
			if(!htable_del(&anon_mmap_table, hash, mm)) log_err("anon shared mm=%p not in htable?", mm);
		}
		__rb_erase(&mm->rb, &sp->maps);
		free(mm);
	}
	RB_FOREACH_SAFE(cur, &sp->as_free) {
		__rb_erase(cur, &sp->as_free);
		free(rb_entry(cur, struct as_free, rb));
	}
	sp->kip_area = L4_Nilpage;
	ra_free(vm_space_ra, sp);
}

/* split and remove lazy mmaps and vpages covered by @addr:@size. */
void munmap_space(struct vm_space *sp, L4_Word_t addr, size_t size)
{
	assert(e_inside()); assert((addr & PAGE_MASK) == 0); assert((size & PAGE_MASK) == 0); assert(VALID_ADDR_SIZE(addr, size));

	remove_vp_range(sp, addr, size);

	/* carve up lazy mmaps */
	for(struct lazy_mmap *cur = first_lazy_mmap(sp, addr, size), *next; cur != NULL; cur = next) {
		next = container_of_or_null(__rb_next(&cur->rb), struct lazy_mmap, rb);
		assert(cur->addr >= addr || cur->addr + cur->length > addr);
		if(cur->addr >= addr + size) break;
		size_t hash = hash_lazy_mmap_by_ino(cur, NULL);
		if(cur->addr >= addr && cur->addr + cur->length <= addr + size) {
			/* lazy_mmap entirely within range. remove it outright. */
			if(IS_ANON_MMAP(cur)) {
				cut_anon_mmap(cur, hash, 0, 0);
				bool ok = htable_del(&anon_mmap_table, hash, cur);
				assert(ok);
			}
			__rb_erase(&cur->rb, &sp->maps);
			if(sp->last_mmap == cur) sp->last_mmap = &no_last_mmap;
			e_free(cur);
			continue;
		} else if(cur->addr < addr && cur->addr + cur->length > addr + size) {
			/* the other way around; shorten and add fragment for tail. */
			struct lazy_mmap *tail = malloc(sizeof *tail); if(tail == NULL) goto Enomem;
			*tail = *cur;
			tail->addr = addr + size;
			tail->length = cur->length - (tail->addr - cur->addr);
			tail->tailsz = cur->tailsz;
			cur->length = addr - cur->addr;
			cur->tailsz = 0;
			struct lazy_mmap *old = insert_lazy_mmap(sp, tail); assert(old == NULL);
			if(IS_ANON_MMAP(cur)) {
				cut_anon_mmap(cur, hash, cur->addr + cur->length, tail->addr - 1);
				if(!htable_add(&anon_mmap_table, hash, tail)) goto Enomem;
			}
		} else if(cur->addr < addr) {
			/* overlap on the left. shorten existing map. */
			assert(addr < cur->addr + cur->length);
			cur->length = addr - cur->addr;
			cur->tailsz = 0;
			if(IS_ANON_MMAP(cur)) cut_anon_mmap(cur, hash, cur->addr, cur->addr + cur->length - 1);
		} else {
			/* overlap on the right. move existing map up. */
			assert(cur->addr + cur->length > addr + size);
			assert(cur->addr < addr + size);
			if(IS_ANON_MMAP(cur)) cut_anon_mmap(cur, hash, cur->addr, addr - 1);
			cur->length -= addr + size - cur->addr;
			cur->addr = addr + size;
		}
	}
	assert(first_lazy_mmap(sp, addr, size) == NULL);
	return;
Enomem: log_crit("out of memory"); abort(); /* FIXME: idempotent, suspend and retry in vm.c */
}

static L4_Word_t get_as_free(struct vm_space *sp, size_t length)
{
	/* TODO: search through sp->as_free in reverse for a chunk that fits
	 * @length & carve it up.
	 */
	return 0;
}

int fork_maps(struct vm_space *src, struct vm_space *dest)
{
	RB_FOREACH(cur, &src->maps) {
		struct lazy_mmap *copy = malloc(sizeof *copy); if(copy == NULL) goto Enomem;
		*copy = *container_of(cur, struct lazy_mmap, rb);
		if(IS_ANON_MMAP(copy) && !htable_add(&anon_mmap_table, hash_lazy_mmap_by_ino(copy, NULL), copy)) { free(copy); goto Enomem; }
		void *dupe = insert_lazy_mmap(dest, copy); assert(dupe == NULL);
	}
	return 0;
Enomem:
	RB_FOREACH_SAFE(cur, &dest->maps) {
		struct lazy_mmap *copy = container_of(cur, struct lazy_mmap, rb);
		if(IS_ANON_MMAP(copy)) {
			bool found = htable_del(&anon_mmap_table, hash_lazy_mmap_by_ino(copy, NULL), copy);
			assert(found);
		}
		__rb_erase(cur, &dest->maps);
		free(cur);
	}
	return -ENOMEM;
}

static int share(struct pl *cached, struct vp *vp)
{
	if(atomic_compare_exchange_strong_explicit(&pl2pp(cached)->owner, &(struct vp *){ NULL }, vp, memory_order_release, memory_order_relaxed)) return 0;
	else {
		assert(pl2pp(cached)->owner != vp);
		return add_share(cached->page_num, vp);
	}
}

static int repair_shared(struct vp *vp, const struct lazy_mmap *mm, size_t faddr, int rwx)
{
	assert(e_inside()); assert(mm->flags & MAP_SHARED);
	int bump = ((faddr & ~PAGE_MASK) - mm->addr) >> PAGE_BITS, n;
	struct pl *cached = NULL; if(n = fetch_cached_page(&cached, mm, bump, vp), n < 0) return n;
	log_info("%s phys %p from cache", n == 0 ? "share" : "get", pl2ptr(cached));
	vp->status = cached->page_num;
	vp->vaddr |= MMAP_RIGHTS(mm);
	if(n == 0) n = share(cached, vp);
	return n < 0 ? n : rwx;
}

static int repair_private(struct vp *vp, const struct lazy_mmap *mm, size_t faddr, int rwx)
{
	assert(e_inside()); assert(~mm->flags & MAP_SHARED);
	int offset = ((faddr & ~PAGE_MASK) - mm->addr) >> PAGE_BITS;
	struct pl *link = NULL, *cached;
	if(mm->flags & MAP_ANONYMOUS) {
		link = get_free_pl();
		memset(pl2ptr(link), '\0', PAGE_SIZE);
	} else {
		int n = fetch_cached_page(&cached, mm, offset, vp); if(n < 0) return n;
		bool tailcase = mm->tailsz > 0 && (faddr & ~PAGE_MASK) == mm->addr + mm->length - PAGE_SIZE;
		if((rwx & L4_Writable) || tailcase) {
			/* copy on write */
			link = get_free_pl();
			if(tailcase) {
				/* copy only mm->tailsz bytes, clear the rest. */
				memcpy(pl2ptr(link), pl2ptr(cached), mm->tailsz);
				memset(pl2ptr(link) + mm->tailsz, '\0', PAGE_SIZE - mm->tailsz);
			} else {
				memcpy(pl2ptr(link), pl2ptr(cached), PAGE_SIZE);
			}
			if(n == 1) {
				/* drop ownership of a freshly loaded page. */
				struct vp *old = atomic_exchange_explicit(&pl2pp(cached)->owner, NULL, memory_order_relaxed);
				assert(old == vp);
			}
		} else {
			/* read-only mappings from page cache */
			vp->status = cached->page_num;
			if(MMAP_RIGHTS(mm) & L4_Writable) vp->vaddr |= VPF_COW | (MMAP_RIGHTS(mm) & ~L4_Writable);
			else vp->vaddr |= MMAP_RIGHTS(mm);
			assert(~VP_RIGHTS(vp) & L4_Writable);
			if(n == 0 && (n = share(cached, vp), n < 0)) { /* FIXME: clean up */ log_crit("share() failed: %s", stripcerr(n)); abort(); }
		}
		assert(cached == NULL || !PL_IS_PRIVATE(cached));
	}
	assert(link == NULL || PL_IS_PRIVATE(link));
	if(link != NULL) {
		*link = (struct pl){ .page_num = link->page_num, .status = PL_ALIVE };
		vp->status = link->page_num;
		vp->vaddr |= VPF_ANON | MMAP_RIGHTS(mm);
		atomic_store_explicit(&pl2pp(link)->owner, vp, memory_order_release);
		push_page(&page_active_list, link);
		e_free(link);
		rwx |= MMAP_RIGHTS(mm) & L4_Writable; /* avoid write fault */
	}
	return rwx;
}

/* TODO: expand existing mmaps even when @fixed is set. */
int reserve_mmap(struct lazy_mmap *mm, struct vm_space *sp, L4_Word_t address, size_t length, bool fixed)
{
	assert((length & PAGE_MASK) == 0);
	assert((address & PAGE_MASK) == 0);
	assert(e_inside());	/* for munmap_space() */
	/* NOTE: instead of checking these for every mmap, we could allow
	 * lazy_mmaps over sip, kip, and utcb but render them ineffective in
	 * vm_pf() (and therefore vm_munmap()). this saves us a _NP return
	 * from mmap(2).
	 */
	const L4_Fpage_t nogo[] = { sp->utcb_area, sp->kip_area, L4_FpageLog2(sp->brk - PAGE_SIZE, PAGE_BITS) };
	for(int i=0; i < ARRAY_SIZE(nogo); i++) {
		if(OVERLAP_EXCL(L4_Address(nogo[i]), L4_Size(nogo[i]), address, length)) {
			/* bad hint. bad! */
			if(fixed) return -EEXIST; else address = 0;
			break;
		}
	}
	if(IS_ANON_MMAP(mm) && !htable_add(&anon_mmap_table, hash_lazy_mmap_by_ino(mm, NULL), mm)) return -ENOMEM;
	struct lazy_mmap *old;
	mm->addr = address;
	mm->length = length;
	if(fixed) {
		if(old = insert_lazy_mmap(sp, mm), old != NULL) {
			if(old->flags & MAP_SYSTEM) {
				if(IS_ANON_MMAP(mm)) htable_del(&anon_mmap_table, hash_lazy_mmap_by_ino(mm, NULL), mm);
				return -EEXIST;
			}
			munmap_space(sp, mm->addr, mm->length);
			old = insert_lazy_mmap(sp, mm);
		}
	} else if(address == 0 || (old = insert_lazy_mmap(sp, mm), old != NULL)) {
		/* TODO: use @address hint to find something "nearby". */
		address = get_as_free(sp, mm->length);
		if(address != 0) mm->addr = address;
		else {
			/* carve up more fresh address space. mm, delicious. */
			/* FIXME: avoid utcb, kip, sysinfo */
			mm->addr = sp->mmap_bot - mm->length + 1;
			sp->mmap_bot -= mm->length;
		}
		old = insert_lazy_mmap(sp, mm);
	}
	assert(old == NULL);
	if(mm->repair_fn == NULL) mm->repair_fn = mm->flags & MAP_SHARED ? &repair_shared : &repair_private;
	return 0;
}

int fault_map(L4_Fpage_t *map_page_p, struct vm_space *sp, size_t faddr, int rwx)
{
	struct lazy_mmap *mm = find_lazy_mmap(sp, faddr); if(mm == NULL || (MMAP_RIGHTS(mm) & rwx) != rwx) return -EFAULT;
	struct vp_iter vi = { };
	struct vp *vp = add_vp(sp, &vi, faddr & ~PAGE_MASK); if(vp == NULL) return -errno;
	*vp = (struct vp){ .vaddr = faddr & ~PAGE_MASK };
	int n = (*mm->repair_fn)(vp, mm, faddr, rwx); if(n < 0) { del_vp(&(uintptr_t){ 0 }, sp, &vi, faddr & ~PAGE_MASK); return n; }
	assert((n & rwx) == rwx); assert((VP_RIGHTS(vp) & rwx) == rwx); assert(~vp->status & 0x80000000); assert(vp->status != 0); assert((vp->vaddr & ~PAGE_MASK) != 0);
	*map_page_p = L4_FpageLog2(vp->status << PAGE_BITS, PAGE_BITS);
	L4_Set_Rights(map_page_p, n);
	return 1;
}

static void find_max_addr(void)
{
	struct memdescbuf md = { .ptr = L4_MemoryDesc(__the_kip, 0), .len = __the_kip->MemoryInfo.n };
	L4_Word_t acc = 0;
	L4_Fpage_t fp;
	do {
		fp = mdb_query(&md, acc, ~0ul, true, false, L4_ConventionalMemoryType);
		if(!L4_IsNilFpage(fp) && L4_SizeLog2(fp) >= PAGE_BITS) acc = max(acc, L4_Address(fp) + L4_Size(fp));
	} while(!L4_IsNilFpage(fp));
	user_addr_max = acc - 1;
}

COLD void init_space(void) {
	find_max_addr();
	vm_space_ra = RA_NEW(struct vm_space, SNEKS_MAX_PID + 1);
}
