#ifndef _SYS_VM_DEFS_H
#define _SYS_VM_DEFS_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <assert.h>
#include <nbsl.h>
#include <l4/types.h>
#include <sneks/rbtree.h>
#include <sneks/sysinfo.h>
#include <sneks/block.h>
#include <ukernel/rangealloc.h>

/* virtual memory page. (there is no <struct vl>.)
 *
 * for now, ->status designates the physical page number used for this vpage.
 * its high bit should always be clear. there will be other formats for
 * swapspace slots, references to pagecache items, and so on. this arrangement
 * allows for as many as 2^31 physical pages in vm, or 8 TiB worth. if
 * ->status is 0, anonymous memory has not yet been attached to this page by
 *  the fault handler.
 *
 * flags are assigned in ->vaddr's low 12 bits as follows:
 *   - 2..0 are a mask of L4_Readable, L4_Writable, and L4_eXecutable,
 *     designating the access granted to the associated address space.
 *   - bit 3 indicates copy-on-write sharing.
 *   - bits 11..4 are not used and should be left clear.
 */
struct vp {
	uintptr_t vaddr;	/* vaddr in 31..12, flags in 11..0 */
	uint32_t status;	/* complex format, see comment. */
};

#define VP_RIGHTS(vp) ((vp)->vaddr & 7)
#define VP_IS_ANON(vp) !!((vp)->vaddr & VPF_ANON)
#define VP_IS_COW(vp) !!((vp)->vaddr & VPF_COW)

/* vp->vaddr's low 12 bits describing properties of the memory page as seen by
 * an userspace program.
 *   - L4 access bits in low 3, but see VPF COW.
 *   - VPF_ANON: not backed by page cache or buffer cache.
 *   - VPF_COW: write faults to this page are processed copy-on-write.
 *     exclusive with L4_Writable.
 */
#define VPF_ANON 0x8
#define VPF_COW 0x10

/* physical memory. has a short name because it's very common. this structure
 * has the read-only parts of the pp/pl split; the only field that may be
 * concurrently written is ->link, which may be swapped for NULL to take the
 * page off a list (depublication) once the corresponding <struct pl> has been
 * depublished.
 */
struct pp {
	/* frame number implied via pp_ra. */
	struct pl *_Atomic link;
	struct vp *_Atomic owner;	/* primary owner. see share_table. */
};

#define PL_DEAD 0
#define PL_ALIVE 1

#define PL_FSID(pl) ((pl)->fsid_ino >> 48)
#define PL_INO(pl) ((pl)->fsid_ino & ~0xffff000000000000ull)
#define PL_IS_PRIVATE(pl) ((pl)->fsid_ino == 0)

/* discarding link from one of the page lists to a physical page, referenced
 * by index in ->page_num. created by malloc, managed by epoch; an open bracket
 * is required by most every function dealing in these.
 */
struct pl {
	struct nbsl_node nn;
	uint32_t page_num;
	/* mask of all zeroes if the link is dead, at least PL_ALIVE otherwise, and
	 * PL_* according to status.
	 */
	_Atomic int status;
	/* for pagecache pages: those backed by files and either shared or
	 * copy-on-written, and anonymous pages part of a shared-memory mapping.
	 *
	 * fsid_ino has the filesystem ID in the top 16 bits and a per-filesystem
	 * value in the lower 48, typically an inode number. fsid is pidof_NP() of
	 * filesystem, or vm for anonymous memory.
	 *
	 * these fields may be written in the structure received from
	 * get_free_pl(), because that structure will only be phantom up the
	 * freelist where these fields are ignored.
	 */
	uint64_t fsid_ino;
	uint32_t offset;	/* limits files to 16 TiB each */
	/* buffer cache pages get a 31-bit block device identifier and a 64-bit
	 * offset of 4k pages, for up to 64 ZiB space per device. that's enough
	 * for everybody, surely.
	 */
	struct {
		int blkid;
		uint64_t blkoffset;
	} buf;
};

static inline void *pl2ptr(const struct pl *link) {
	return (void *)((uintptr_t)link->page_num << PAGE_BITS);
}

/* vm.c */
extern struct htable anon_mmap_table;

struct vm_space;
extern void remove_vp_range(struct vm_space *, size_t, size_t);
struct lazy_mmap;
extern bool has_shares(size_t, uint32_t, int);

/* dequeue one link from page_free_list and return it.
 * TODO: rejigger to support asynchronous replacement
 */
extern struct pl *get_free_pl(void);
extern void unget_free_pl(struct pl *link);
extern void free_page(struct pl *);
/* add @page to the buffer cache under @blkid:@offset, returning @page on
 * success or an existing pl if there was one. @top is the bucket top node from
 * bufcache_get(), or NULL.
 */
extern struct pl *bufcache_push(struct nbsl_node *top, struct pl *page);
/* sets *@top_p to the top node for bufcache_push() when @top_p != NULL. */
extern struct pl *bufcache_get(struct nbsl_node **top_p, int blkid, uint64_t offset);
extern void bufcache_purge(int blkid);

/* frame.c */
extern const struct __sysinfo *the_sip;
extern size_t pp_first, pp_total;
extern struct rangealloc *pp_ra;

static inline struct pp *get_pp(uint32_t page_num) {
	assert(page_num >= pp_first); assert(page_num < pp_first + pp_total);
	return ra_id2ptr(pp_ra, page_num - pp_first);
}

static inline struct pp *pl2pp(const struct pl *link) {
	return get_pp(link->page_num);
}

/* this leaves open an IPC chain to root, which caller should reply with MR0=0 to
 * *peer_tid_p when vm has completed initialization. an error message path
 * exists but isn't documented.
 */
extern L4_Fpage_t *init_protocol(int *n_phys_p, L4_ThreadId_t *peer_tid_p);

/* page.c */
#define VPG_SIZE_LOG2 9
#define VPG_SIZE ((size_t)1 << VPG_SIZE_LOG2)
#define VPG_MASK (((size_t)1 << (VPG_SIZE_LOG2 + PAGE_BITS)) - 1)
#define VP_COUNT(g) ((g)->addr & (VPG_SIZE - 1))
struct vp_group {
	size_t addr; /* low VPG_SIZE_LOG2 bits are VP_COUNT of valid pages; PAGE_BITS more remain */
	struct vp pages[VPG_SIZE];
};
struct vp_iter {
	struct vp_group *g;
};
extern struct nbsl page_free_list, page_active_list;
extern struct htable share_table;
extern void remove_vp(struct vp *vp);
extern struct vp *add_vp(struct vm_space *, struct vp_iter *, size_t); /* NULL+errno on failure */
extern struct vp *get_vp(struct vm_space *, struct vp_iter *, size_t);
extern uint32_t del_vp(uintptr_t *, struct vm_space *sp, struct vp_iter *, size_t); /* invalidates all vp_iter on @sp */
extern int fault_vp(L4_Fpage_t *, struct vm_space *, size_t, int);
extern int fork_pages(struct vm_space *, struct vm_space *);
extern bool add_share(uint32_t status, struct vp *share);
extern bool rm_share(struct vp *vp);
/* doesn't discard @oldlink, or mark it for disposal. */
extern void push_page(struct nbsl *list, struct pl *oldlink);
extern size_t hash_vpg(const void *, void *);

/* cache.c */
struct lazy_mmap;
extern struct nbsl *pc_buckets;
extern unsigned n_pc_buckets_log2;
#define n_pc_buckets (1u << n_pc_buckets_log2)
extern size_t hash_cached_page(uint32_t, uint32_t, uint32_t);
extern void init_pagecache(size_t);
extern struct pl *find_cached_page(struct nbsl_node **, const struct lazy_mmap *, int);
extern int fetch_cached_page(struct pl **, const struct lazy_mmap *, int, struct vp *);

/* buf.c */
extern int bufcache_size_log2;
extern struct nbsl *bufcache;
#define bufcache_size ((size_t)1 << bufcache_size_log2)
extern struct pl *bufcache_push(struct nbsl_node *top, struct pl *page);
extern struct pl *bufcache_get(struct nbsl_node **top_p, int blkid, uint64_t offset);
extern void bufcache_purge(int blkid);
extern void init_bufcache(size_t n_pages);

/* util.c */

/* whether [a, a+b) and [c, c+d) overlap. */
#define OVERLAP_EXCL(a, b, c, d) ({ typeof(a) __a = (a); typeof(c) __c = (c); !(__a + (b) <= __c || __a >= __c + (d)); })

extern int devnoctl(char kind, dev_t major, dev_t minor, L4_ThreadId_t tid);
extern int enosys();

/* block.c */
extern int block_main(void *);

/* loop_ctl.c, loop_dev.c */
#define MAX_LOOPDEV 15

struct loopdev {
	L4_ThreadId_t server;
	unsigned handle, log_sec, minor;
	off_t offset, size, sizelimit, dio;
	bool autoclear, ro;
	char *backing_path;
};

extern struct blk_class loop_class;
extern blk_t *devs[MAX_LOOPDEV + 1]; /* FIXME: too short, rename */
extern int max_attached;

extern int loop_ctl_main(void *);
extern bool loop_valid(int id);

/* invariant checks from all modules */
#ifndef DEBUG_ME_HARDER
#define all_invariants() true
#else
#include <sneks/invariant.h>
extern struct nbsl *next_page_list(int *);
extern bool all_invariants(void);
extern bool page_invariants(INVCTX_ARG);
extern bool frame_invariants(INVCTX_ARG);
extern bool pc_invariants(INVCTX_ARG, struct vp *const *, size_t);
#endif

#endif
