#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>
#include <unistd.h>
#include <sys/types.h>
#include <ukernel/hook.h>
#include <sneks/systask.h>

static tss_t log_hook_key;

static void hook_dtor(void *ptr) {
	struct hook *h = ptr;
	/* TODO: establish a "pls go" code, use hook_call_front() to dtor them fools */
	free(h);
}

static void log_hook_ctor(void) {
	if(tss_create(&log_hook_key, &hook_dtor) != thrd_success) abort();
}

struct hook *log_hook(void)
{
	static once_flag init_once = ONCE_FLAG_INIT;
	call_once(&init_once, &log_hook_ctor);
	struct hook *h = tss_get(log_hook_key);
	if(h == NULL) {
		if(h = malloc(sizeof *h), h == NULL) { perror("malloc in log_hook"); abort(); }
		*h = HOOK_INIT(*h, NULL);
		tss_set(log_hook_key, h);
	}
	return h;
}

void log_msgv(int level, const char *fmt, va_list args)
{
	char *str;
	int len = vasprintf(&str, fmt, args);
	if(len < 0) {
		fprintf(stderr, "%s[%d]: malloc failed (BULLSHIT IS AFOOT)\n", __func__, getpid());
		abort();
	}
	hook_call_front(log_hook(), &str, level);
	if(str != NULL) {
		len = strlen(str);
		while(len > 0 && str[len - 1] == '\n') str[--len] = '\0';
		/* TODO: use an actual logging system */
		fprintf(stderr, "%s\n", str);
		free(str);
	}
}

int vasprintf(char **sptr, const char *fmt, va_list al)
{
	va_list copy; va_copy(copy, al);
	int len = vsnprintf(NULL, 0, fmt, copy);
	va_end(copy);
	if(*sptr = malloc(len + 1), *sptr == NULL) return -1;
	return vsnprintf(*sptr, len + 1, fmt, al);
}
