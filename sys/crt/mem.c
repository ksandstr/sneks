#include <errno.h>
#include <sys/mman.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <sneks/mm.h>
#include <sneks/sys/sysmem-defs.h>

void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset) {
	errno = ENOSYS;
	return MAP_FAILED;
}

int munmap(void *addr, size_t length) {
	errno = ENOSYS;
	return -1;
}

int mlock(const void *addr, size_t length)
{
	size_t address, size_log2;
	for_page_range((uintptr_t)addr, (uintptr_t)addr + length, address, size_log2) {
		int n = __sysmem_alter_flags(L4_Pager(), L4_nilthread.raw, L4_FpageLog2(address, size_log2), SMATTR_PIN, ~0ul);
		if(n != 0) { errno = n < 0 ? -n : EIO; return -1; }
	}
	return 0;
}

int munlock(const void *addr, size_t length)
{
	size_t address, size_log2;
	for_page_range((uintptr_t)addr, (uintptr_t)addr + length, address, size_log2) {
		int n = __sysmem_alter_flags(L4_Pager(), L4_nilthread.raw, L4_FpageLog2(address, size_log2), 0, ~SMATTR_PIN);
		if(n != 0) { errno = n < 0 ? -n : EIO; return -1; }
	}
	return 0;
}
