#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <muidl.h>
#include <l4/types.h>
#include <l4/ipc.h>
#include <l4/kdebug.h>
#include <sneks/console.h>
#include <sneks/sys/abend-defs.h>
#include "defs.h"

static uint8_t muidl_supp_mem[64] __attribute__((aligned(16)));

void abort(void) {
	printf("sysmem: abort() called from %p!\n", __builtin_return_address(0));
	for(;;) L4_Sleep(L4_Never);
}

noreturn void panic(const char *msg) {
	printf("sysmem: PANIC! %s\n", msg);
	abort();
}

void abend(long class, const char *fmt, ...)
{
	va_list al;
	va_start(al, fmt);
	static char fail[300];
	vsnprintf(fail, sizeof fail, fmt, al);
	va_end(al);
/* TODO: restore once abend works again */
#if 1
	printf("sysmem: %s: class=%#lx: %s", __func__, class, fail);
	abort();
#else
	L4_LoadMR(0, (L4_MsgTag_t){ .X.label = PROXY_ABEND_LABEL, .X.u = 2 }.raw);
	L4_LoadMR(1, class);
	L4_LoadMR(2, (L4_Word_t)&fail[0]);
	L4_MsgTag_t tag = L4_Send(abend_helper_tid);
	if(L4_IpcFailed(tag)) {
		printf("%s: IPC to abend_helper failed, ec=%#lx\n",
			__func__, L4_ErrorCode());
	}
#endif
}

void con_putstr(const char *string) {
	L4_KDB_PrintString((char *)string);
}

int *__errno_location(void) {
	static int the_errno = 0;
	return &the_errno;
}

void __assert_failure(const char *cond, const char *file, unsigned int line, const char *fn) {
	printf("!!! assert(%s) failed in `%s' [%s:%u]\n", cond, fn, file, line);
	abort();
}

void muidl_supp_alloc_context(size_t length) {
	assert(length <= sizeof muidl_supp_mem);
	memset(muidl_supp_mem, '\0', length);
}

void *muidl_supp_get_context(void) {
	return muidl_supp_mem;
}

void *kmem_alloc_new_page(void) {
	struct p_page *p = get_free_page();
	list_add(&reserved_page_list, &p->link);
	return (void *)P_ADDRESS(p);
}

void kmem_free_page(void *ptr) {
	struct p_page *pp = addr2p((uintptr_t)ptr);
	if(pp == NULL) panic("tried to kmem_free_page() an unknown pointer?");
	list_del_from(&reserved_page_list, &pp->link);
	list_add(&free_page_list, &pp->link); num_free_pages++;
}

/* malloc and free using a tiny static pool of memory. */
static uint8_t malloc_pool[1 * 1024] __attribute__((aligned(4096)));

void *malloc(size_t sz)
{
	static size_t pos = 0;
	size_t aligned = (sz + 7) & ~7;
	if(pos + aligned >= sizeof malloc_pool) {
		printf("sysmem: malloc_pool can't satisfy sz=%zu (%zu) at pos=%zu\n", sz, aligned, pos);
		return NULL;
	}
	void *ptr = &malloc_pool[pos];
	pos += aligned;
	return ptr;
}

void *calloc(size_t nmemb, size_t size) {
	void *ptr = malloc(nmemb * size);
	if(ptr != NULL) memset(ptr, 0, nmemb * size);
	return ptr;
}

void free(void *ptr) {
	/* the goggles, they do nothing */
}
