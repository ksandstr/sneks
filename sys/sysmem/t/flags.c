#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <l4/message.h>
#include <l4/ipc.h>
#include <ccan/crc32c/crc32c.h>
#include <sneks/test.h>
#include <sneks/mm.h>
#include <sneks/sys/sysmem-defs.h>

/* set some flags with Sysmem::alter_flags, then clear them. this test aims
 * for a particular assertion in the sysmem fault handler; see changelog for
 * deets.
 */
START_TEST(set_and_clear_flags)
{
	const int brksize = 64 * 1024;
	diag("brksize=%d", brksize);
	plan_tests(3);

	void *brkpos = sbrk(brksize);
	diag("brkpos=%p", brkpos);
	int n = __sysmem_alter_flags(L4_Pager(), L4_nilthread.raw, L4_FpageLog2((L4_Word_t)brkpos, 12), SMATTR_PIN, ~0ul);
	if(!ok(n == 0, "alter_flags to set PIN")) diag("n=%d", n);
	/* then clear them. */
	n = __sysmem_alter_flags(L4_Pager(), L4_nilthread.raw, L4_FpageLog2((L4_Word_t)brkpos, 12), 0, ~(L4_Word_t)SMATTR_PIN);
	if(!ok(n == 0, "alter_flags to clear PIN")) diag("n=%d", n);
	/* now poke the memory. */
	memset(brkpos, 0xd5, brksize);
	pass("memset didn't die");

	sbrk(-brksize);
}
END_TEST

SYSTEST("mem:flags", set_and_clear_flags);


static void big_sysmem_grind(void)
{
	size_t sz = 32 * 1024 * 1024; /* TODO: figure out a properly smashy value */
	void *large = malloc(sz);
	if(large != NULL) {
		memset(large, 0xfd, sz);
		free(large);
	}
}

START_LOOP_TEST(mlock, iter, 0, 3)
{
	const bool do_lock = iter & 1, set_last = iter & 2;
	diag("do_lock=%s, set_last=%s", btos(do_lock), btos(set_last));
	plan_tests(2);

	char *mem = sbrk(PAGE_SIZE); diag("mem=%p", mem);
	if(!set_last) { for(int i=0; i < PAGE_SIZE; i++) mem[i] = "abcdefgh"[i % 8]; }
	if(do_lock && mlock(mem, PAGE_SIZE) < 0) {
		skip(2, "mlock failed: %s", strerror(errno));
		return;
	}
	if(set_last) { for(int i=0; i < PAGE_SIZE; i++) mem[i] = "abcdefgh"[i % 8]; }
	diag("reference sum is %#x", crc32c(0, mem, PAGE_SIZE));

	char server[32]; snprintf(server, sizeof server, "SERVER=%#lx", L4_Myself().raw);
	char *argv[] = { "initrd_spawn_partner", "pinned-memory", NULL }, *envp[] = { server, NULL };
	int cpid = spawn_NP("/initrd/systest/sys/test/initrd_spawn_partner", argv, envp);
	skip_start(!ok(cpid > 0, "spawn"), 1, "no spawn partner: %s", strerror(errno)) {
		L4_ThreadId_t ctid; L4_MsgTag_t tag;
		do tag = L4_Wait_Timeout(L4_TimePeriod(10 * 1000), &ctid); while(L4_IpcSucceeded(tag) && pidof_NP(ctid) != cpid);
		skip_start(L4_IpcFailed(tag), 1, "spawn partner didn't call, ec=%lu", L4_ErrorCode()) {
			L4_Fpage_t p = L4_Fpage((L4_Word_t)mem, PAGE_SIZE); L4_Set_Rights(&p, L4_Readable);
			L4_LoadMR(0, (L4_MsgTag_t){ .X.t = 2 }.raw);
			L4_LoadMRs(1, 2, L4_MapItem(p, 0).raw);
			tag = L4_Reply(ctid);
			skip_start(L4_IpcFailed(tag), 1, "spawn partner didn't receive, ec=%lu", L4_ErrorCode()) {
				big_sysmem_grind();
				tag = L4_Receive_Timeout(ctid, L4_TimePeriod(10 * 1000));
				skip_start(L4_IpcFailed(tag), 1, "spawn partner didn't sync, ec=%lu", L4_ErrorCode()) {
					L4_LoadMR(0, 0); tag = L4_Call_Timeouts(ctid, L4_ZeroTime, L4_TimePeriod(10 * 1000));
					skip_start(L4_IpcFailed(tag), 1, "spawn partner didn't return crc, ec=%lu", L4_ErrorCode()) {
						L4_Word_t sum; L4_StoreMR(1, &sum); diag("sum=%#lx", sum);
						iff_ok1(sum == crc32c(0, mem, PAGE_SIZE), do_lock);
						L4_LoadMR(0, 0); L4_Reply(ctid);
					} skip_end;
				} skip_end;
			} skip_end;
		} skip_end;
	} skip_end;
}
END_TEST

SYSTEST("mem:flags", mlock);
