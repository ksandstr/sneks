#include <assert.h>
#include <sneks/rbtree.h>
#include <sneks/process.h>
#include <ukernel/slab.h>
#include "defs.h"

struct kmem_cache *systask_slab;

static struct rb_root systask_tree = RB_ROOT;

static struct systask *put_task_helper(struct systask *t)
{
	struct rb_node **p = &systask_tree.rb_node, *parent = NULL;
	while(*p != NULL) {
		parent = *p;
		struct systask *oth = rb_entry(parent, struct systask, rb);
		if(t->pid < oth->pid) p = &(*p)->rb_left;
		else if(t->pid > oth->pid) p = &(*p)->rb_right;
		else return oth;
	}
	rb_link_node(&t->rb, parent, p);
	return NULL;
}

static void put_task(struct systask *t) {
	struct systask *dupe = put_task_helper(t);
	if(dupe != NULL) panic("duplicate in put_task()!");
	rb_insert_color(&t->rb, &systask_tree);
	assert(find_task(t->pid) == t);
}

struct systask *find_task(int pid)
{
	if(pid < SNEKS_MIN_SYSID) return NULL;
	struct rb_node *n = systask_tree.rb_node;
	while(n != NULL) {
		struct systask *t = rb_entry(n, struct systask, rb);
		if(pid < t->pid) n = n->rb_left;
		else if(pid > t->pid) n = n->rb_right;
		else return t;
	}
	return NULL;
}

struct systask *get_task(int pid)
{
	assert(pid >= SNEKS_MIN_SYSID);
	struct systask *t = find_task(pid);
	if(t == NULL) {
		t = kmem_cache_alloc(systask_slab);
		*t = (struct systask){ .lpgs = RB_ROOT, .brk = 0x10000, .min_fault = ~PAGE_MASK, .pid = pid };
		assert(L4_IsNilFpage(t->kip_area)); assert(L4_IsNilFpage(t->utcb_area));
		put_task(t);
	}
	return t;
}

void systask_dtor(struct systask *t) {
	rb_erase(&t->rb, &systask_tree);
	kmem_cache_free(systask_slab, t);
}
