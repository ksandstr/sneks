#ifndef SYSMEM_DEFS_H
#define SYSMEM_DEFS_H

#include <stdnoreturn.h>
#include <ccan/list/list.h>
#include <ccan/compiler/compiler.h>
#include <l4/types.h>
#include <l4/ipc.h>
#include <sneks/rbtree.h>
#include <sneks/mm.h>

struct l_page {
	size_t p_addr; /* bits 11 thru 0 are LF_*, rest per bits */
};

#define LP_FLAGS(lp) ((lp)->p_addr & PAGE_MASK)
#define LP_PHYS(lp) ((lp)->p_addr & ~PAGE_MASK)
#define LF_PIN SMATTR_PIN
#define LF_SWAP 2	/* p_addr{..12} designates a c_page. */

struct p_page
{
	struct list_node link;	/* in one or zero of {free,active,reserved}_page_list */
	struct rb_node phys_rb; /* in phys_to_pp_tree by addr_flags & ~PAGE_MASK */
	L4_Word_t addr_flags;	/* low 12 bits are PF_* */
	struct l_page *owner;	/* single owner iff mapped, otherwise NULL */
	int age;
};

#define P_ADDRESS(pp) ((pp)->addr_flags & ~PAGE_MASK)
#define PF_INCOMPRESSIBLE 1	/* skipped by replacement until written */

struct systask
{
	struct rb_node rb;		/* in systask_tree by ->pid asc */
	struct rb_root lpgs;	/* of <struct lp_group> via rb_addr */
	uintptr_t brk;			/* program break (inclusive) */
	uintptr_t min_fault;
	L4_Fpage_t utcb_area, kip_area;
	uint16_t pid;			/* systask ID per pidof_NP() */
};

/* sysmem.c */
extern unsigned num_free_pages;
extern struct list_head free_page_list, active_page_list, reserved_page_list;

extern struct p_page *get_free_page(void);
extern struct p_page *addr2p(size_t);

/* task.c */
extern struct kmem_cache *systask_slab;
extern struct systask *find_task(int);
extern struct systask *get_task(int);
extern void systask_dtor(struct systask *);

/* comp.c */
extern bool compress(struct p_page *);
extern struct p_page *decompress(struct l_page *);
extern void unswap(struct l_page *);

/* info.c */
#define PROXY_ABEND_LABEL 0xbaa6
struct sysmem_impl_vtable;
extern void set_sysinfo_vtab(struct sysmem_impl_vtable *);
extern void sysinfo_init_msg(L4_MsgTag_t, const L4_Word_t [static 64]);

/* rt.c */
extern noreturn void panic(const char *);
extern noreturn void abend(long, const char *, ...) PRINTF_FMT(2, 3);

#endif
