#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <ccan/list/list.h>
#include <sneks/lz4.h>
#include <sneks/mm.h>
#include "defs.h"

#define C_MAGIC 0x740b1d8a
#define MAX_COMPRESSED_SIZE (PAGE_SIZE - sizeof(struct c_hdr) - 26)

struct c_hdr
{
	uint32_t magic;	/* C_MAGIC */
	struct p_page *phys;
	struct list_node buddy_link;
	uint16_t a_len, b_len;	/* compressed content length */
	struct l_page *a, *b;
};

static char lz4_state[LZ4_STREAMSIZE] __attribute__((aligned(8))); /* hueg */
static struct list_head buddy_list = LIST_HEAD_INIT(buddy_list);

bool compress(struct p_page *p)
{
	char outbuf[LZ4_COMPRESSBOUND(PAGE_SIZE)];
	int c_size = LZ4_compress_fast_extState(lz4_state, (void *)P_ADDRESS(p), outbuf, PAGE_SIZE, sizeof outbuf, 1);
	if(c_size >= MAX_COMPRESSED_SIZE) return false;
	if(c_size == 0) panic("c_size=0??");

	/* TODO: rework this to not be a silly brute force loop. */
	struct c_hdr *best = NULL, *cand;
	int waste = PAGE_SIZE - sizeof(struct c_hdr) - c_size;
	list_for_each(&buddy_list, cand, buddy_link) {
		assert(cand->magic == C_MAGIC);
		int left = PAGE_SIZE - sizeof(struct c_hdr) - (cand->a != NULL ? cand->a_len : cand->b_len);
		if(left < c_size) continue;
		if(left - c_size < waste) {
			waste = left - c_size;
			best = cand;
		}
	}

	list_del_from(&active_page_list, &p->link);
	if(best == NULL) {
		best = (void *)P_ADDRESS(p);
		*best = (struct c_hdr){ .magic = C_MAGIC, .phys = p, .a_len = c_size, .a = p->owner };
		memcpy(&best[1], outbuf, c_size);
		list_add(&buddy_list, &best->buddy_link);
		p->owner->p_addr |= LF_SWAP;
		p->owner = NULL;
	} else {
		list_del_from(&buddy_list, &best->buddy_link);
		void *addr;
		if(best->a == NULL) {
			addr = &best[1];
			best->a = p->owner;
			best->a_len = c_size;
		} else {
			addr = (void *)best + PAGE_SIZE - c_size;
			assert(addr >= (void *)&best[1]);
			best->b = p->owner;
			best->b_len = c_size;
		}
		memcpy(addr, outbuf, c_size);

		p->owner->p_addr = ((L4_Word_t)best & ~PAGE_MASK) | LF_SWAP;
		p->owner = NULL;
		list_add(&free_page_list, &p->link);
		num_free_pages++;
	}
	return true;
}

struct p_page *decompress(struct l_page *lp)
{
	struct c_hdr *ch = (struct c_hdr *)(lp->p_addr & ~PAGE_MASK); assert(ch->magic == C_MAGIC);
	void *data;
	int n, len;
	bool done;
	if(ch->a == lp) {
		data = &ch[1];
		len = ch->a_len;
		ch->a = NULL;
		done = ch->b == NULL;
	} else {
		assert(ch->b == lp);
		data = (void *)ch + PAGE_SIZE - ch->b_len;
		len = ch->b_len;
		ch->b = NULL;
		done = ch->a == NULL;
	}
	struct p_page *phys;
	if(!done) {
		phys = get_free_page();
		list_add(&buddy_list, &ch->buddy_link);
		n = LZ4_decompress_safe(data, (void *)P_ADDRESS(phys), len, PAGE_SIZE);
	} else {
		/* expand into the same page via a buffer. */
		list_del_from(&buddy_list, &ch->buddy_link);
		phys = ch->phys;
		char buf[len]; memcpy(buf, data, len);
		n = LZ4_decompress_safe(buf, (void *)P_ADDRESS(phys), len, PAGE_SIZE);
	}
	if(n != PAGE_SIZE) {
		printf("%s: n=%d\n", __func__, n);
		panic("invalid decompressed length!");
	}
	return phys;
}

void unswap(struct l_page *lp)
{
	struct c_hdr *ch = (struct c_hdr *)(lp->p_addr & ~PAGE_MASK); assert(ch->magic == C_MAGIC);
	bool done;
	if(ch->a == lp) {
		done = ch->b == NULL;
		ch->a = NULL;
	} else {
		assert(ch->b == lp);
		done = ch->a == NULL;
		ch->b = NULL;
	}
	if(done) {
		/* implies membership in buddy list */
		list_del_from(&buddy_list, &ch->buddy_link);
		assert(ch->phys->owner == NULL);
		list_add(&free_page_list, &ch->phys->link);
		num_free_pages++;
	} else {
		/* otherwise, it should go in there. */
		list_add(&buddy_list, &ch->buddy_link);
	}
}
