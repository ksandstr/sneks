#define SYSMEMIMPL_IMPL_SOURCE
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <ccan/str/str.h>
#include <ccan/array_size/array_size.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <l4/ipc.h>
#include <sneks/sys/info-defs.h>
#include <sneks/sys/abend-defs.h>
#include "sysmem-impl-defs.h"
#include "defs.h"

static struct sneks_kmsg_info kmsg_info;
static struct sneks_abend_info abend_info;
static struct sneks_uapi_info uapi_info;
static struct sneks_rootfs_info rootfs_info;
static struct sneks_sysapi_info sysapi_info;
static struct sneks_posix_info posix_info;

static int impl_lookup(L4_Word_t *info_tid) { *info_tid = L4_MyGlobalId().raw; return 0; }
static int impl_kmsg_block(struct sneks_kmsg_info *kmsg) { *kmsg = kmsg_info; return 0; }
static int impl_abend_block(struct sneks_abend_info *it) { *it = abend_info; return 0; }
static int impl_uapi_block(struct sneks_uapi_info *it) { *it = uapi_info; return 0; }
static int impl_rootfs_block(struct sneks_rootfs_info *it) { *it = rootfs_info; return 0; }
static int impl_sysapi_block(struct sneks_sysapi_info *it) { *it = sysapi_info; return 0; }
static int impl_posix_block(struct sneks_posix_info *it) { *it = posix_info; return 0; }

#if 0
static void abend_helper_thread(void)
{
	for(;;) {
		L4_ThreadId_t sender;
		L4_MsgTag_t tag = L4_Ipc(L4_nilthread, L4_anylocalthread,
			L4_Timeouts(L4_Never, L4_Never), &sender);
		if(L4_IpcFailed(tag) || L4_Label(tag) != PROXY_ABEND_LABEL) {
			printf("sysmem: %s: ipc failed, tag=%#lx, ec=%#lx\n",
				__func__, tag.raw, L4_ErrorCode());
			continue;
		}

		L4_Word_t cls, ptr;
		L4_StoreMR(1, &cls);
		L4_StoreMR(2, &ptr);
		const char *fail = (void *)ptr;
		if(abend_info.service != 0) {
			L4_ThreadId_t serv = { .raw = abend_info.service };
			int n = __abend_long_panic(serv, cls, fail);
			printf("sysmem: Abend::long_panic() returned n=%d\n", n);
		} else {
			printf("[Sneks::Abend not available! fail=`%s']\n", fail);
		}
	}
}
#endif

static void start_abend_helper(void)
{
/* disabled for being fundamentally unworkable. should be replaced with
 * explicit creation of helper threads by root during sysmem initialization,
 * rather than this circular dependency shite.
 *
 * FIXME: do so, fool
 */
#if 0
	L4_ThreadId_t uapi_tid = { .raw = uapi_info.service };
	assert(!L4_IsNilThread(uapi_tid));

	/* breathing manually.
	 *
	 * FIXME: this is hacky as fuck. we need like a millisecond timeout on the
	 * Call sendphase to unfuck lazily starting UAPI, or they'll wind up in a
	 * send-send deadlock. and that machinery don't belong outside a possible
	 * future super interim_fault(). for now, root's UAPI starting bit holds
	 * our dick for us.
	 */
	L4_Accept(L4_UntypedWordsAcceptor);
	L4_LoadMR(0, (L4_MsgTag_t){ .X.label = 0xe801, .X.u = 1 }.raw);
	L4_LoadMR(1, 0x1234);
	L4_MsgTag_t tag = L4_Call(uapi_tid);
	while(interim_fault(tag, uapi_tid)) {
		tag = L4_Call_Timeouts(uapi_tid, L4_ZeroTime, L4_Never);
	}
	int n;
	if(L4_IpcFailed(tag)) n = L4_ErrorCode();
	else if(L4_Label(tag) == 1) {
		L4_Word_t err;
		L4_StoreMR(1, &err);
		n = -err;
	} else if(L4_Label(tag) != 0) {
		n = -EINVAL;
	} else {
		n = 0;
	}
	if(n != 0) {
		printf("%s: Proc::create_thread failed, n=%d\n", __func__, n);
		abort();
	}

	struct p_page *stkpage = get_free_page();
	L4_Start_SpIp(abend_helper_tid, stkpage->address + PAGE_SIZE - 16,
		(L4_Word_t)&abend_helper_thread);
#endif
}

/* see lib/string.c comment for same fn */
static inline unsigned long haszero(unsigned long x) {
	return (x - 0x01010101ul) & ~x & 0x80808080ul;
}

void sysinfo_init_msg(L4_MsgTag_t tag, const L4_Word_t mrs[static 64])
{
	/* decode the string part. */
	const int u = L4_UntypedWords(tag);
	char buffer[u * 4 + 1];
	int pos = 0;
	do {
		/* keep strict aliasing intact */
		memcpy(&buffer[pos * sizeof(L4_Word_t)], &mrs[pos], sizeof(L4_Word_t));
	} while(!haszero(mrs[pos++]) && pos < u);
	buffer[pos * sizeof(L4_Word_t)] = '\0';
	const char *name = buffer;
	bool found = false;
	if(strends(name, ":tid")) {
		static const struct {
			const char *blockname;
			L4_Word_t *tid_raw_p;
			void (*call)(void);
		} tids[] = {
			{ "kmsg", &kmsg_info.service },
			{ "rootserv", &abend_info.service },
			{ "uapi", &uapi_info.service, &start_abend_helper },
			{ "uapi:vm", &uapi_info.vm },
			{ "rootfs", &rootfs_info.service },
			{ "sys:sysmsg", &sysapi_info.sysmsg },
			{ "sys:devno", &sysapi_info.devno },
			{ "posix:pipe", &posix_info.pipe },
		};
		char *sep = strrchr(name, ':');
		*sep = '\0';
		L4_ThreadId_t tid = { .raw = mrs[pos++] };
		assert(L4_IsGlobalId(tid));
		for(int i=0; i < ARRAY_SIZE(tids); i++) {
			if(streq(name, tids[i].blockname)) {
				found = true;
				*tids[i].tid_raw_p = tid.raw;
				if(tids[i].call != NULL && !L4_IsNilThread(tid)) {
					(*tids[i].call)();
				}
			}
		}
	}
	if(!found) printf("%s: name=`%s' unrecognized\n", __func__, name);
}

void set_sysinfo_vtab(struct sysmem_impl_vtable *vtab)
{
	vtab->lookup = &impl_lookup;
	vtab->kmsg_block = &impl_kmsg_block;
	vtab->abend_block = &impl_abend_block;
	vtab->uapi_block = &impl_uapi_block;
	vtab->rootfs_block = &impl_rootfs_block;
	vtab->sysapi_block = &impl_sysapi_block;
	vtab->posix_block = &impl_posix_block;
}
