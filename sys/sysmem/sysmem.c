#define SYSMEMIMPL_IMPL_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <ccan/list/list.h>
#include <ccan/minmax/minmax.h>
#include <ccan/array_size/array_size.h>
#include <ccan/container_of/container_of.h>
#include <l4/types.h>
#include <l4/kip.h>
#include <l4/ipc.h>
#include <l4/space.h>
#include <l4/thread.h>
#include <l4/kdebug.h>
#include <ukernel/slab.h>
#include <sneks/rbtree.h>
#include <sneks/mm.h>
#include <sneks/process.h>
#include <sneks/rootserv.h>
#include <sneks/console.h>
#include <sneks/sys/info-defs.h>
#include "muidl.h"
#include "sysmem-impl-defs.h"
#include "defs.h"

#define NUM_INIT_PAGES 12	/* traditionally enough. */
#define LPG_SIZE_LOG2 10
#define LPG_SIZE ((size_t)1 << LPG_SIZE_LOG2)
#define LPG_BITS (PAGE_BITS + LPG_SIZE_LOG2)
#define LPG_MASK (((size_t)1 << LPG_BITS) - 1)

/* group of LPG_SIZE l_pages. */
struct lp_group {
	struct rb_node rb_addr, rb_phys;
	size_t address; /* LPG_MASK always zero */
	short count;
	struct l_page *pages;
};

struct fpbuf {
	int n_fp;
	L4_Fpage_t fp[64];
};

L4_KernelInterfacePage_t *the_kip;

unsigned num_free_pages = 0;
struct list_head free_page_list = LIST_HEAD_INIT(free_page_list),
	active_page_list = LIST_HEAD_INIT(active_page_list),
	reserved_page_list = LIST_HEAD_INIT(reserved_page_list);

static bool repl_enable = false;

static struct kmem_cache *pp_slab, *lp_group_slab;
static struct rb_root lpg_phys_tree = RB_ROOT, phys_to_pp_tree = RB_ROOT;

static uint8_t first_mem[PAGE_SIZE * NUM_INIT_PAGES] __attribute__((aligned(PAGE_SIZE)));

static L4_ThreadId_t s0_tid;

static inline struct lp_group *put_lpg_helper(struct systask *task, struct lp_group *g)
{
	struct rb_node **p = &task->lpgs.rb_node, *parent = NULL;
	size_t addr = g->address & ~LPG_MASK, oth_addr;
	while(*p != NULL) {
		parent = *p;
		struct lp_group *oth = rb_entry(parent, struct lp_group, rb_addr);
		oth_addr = oth->address & ~LPG_MASK;
		if(addr < oth_addr) p = &(*p)->rb_left;
		else if(addr > oth_addr) p = &(*p)->rb_right;
		else return oth;
	}
	rb_link_node(&g->rb_addr, parent, p);
	return NULL;
}

static inline struct lp_group *put_lpg_phys_helper(struct lp_group *g)
{
	struct rb_node **p = &lpg_phys_tree.rb_node, *parent = NULL;
	while(*p != NULL) {
		parent = *p;
		struct lp_group *oth = rb_entry(parent, struct lp_group, rb_phys);
		if(g->pages < oth->pages) p = &(*p)->rb_left;
		else if(g->pages > oth->pages) p = &(*p)->rb_right;
		else return oth;
	}
	rb_link_node(&g->rb_phys, parent, p);
	return NULL;
}

static void put_lpg(struct systask *task, struct lp_group *g) {
	if(put_lpg_helper(task, g) != NULL) panic("duplicate in put_lpg() by vaddr!");
	rb_insert_color(&g->rb_addr, &task->lpgs);
	if(put_lpg_phys_helper(g) != NULL) panic("duplicate in put_lpg() by phys!");
	rb_insert_color(&g->rb_phys, &lpg_phys_tree);
}

static struct lp_group *get_lpg(struct systask *task, size_t addr)
{
	assert((addr & LPG_MASK) == 0);
	struct rb_node *n = task->lpgs.rb_node;
	while(n != NULL) {
		struct lp_group *cand = rb_entry(n, struct lp_group, rb_addr);
		size_t cand_addr = cand->address & ~LPG_MASK;
		if(addr < cand_addr) n = n->rb_left;
		else if(addr > cand_addr) n = n->rb_right;
		else return cand;
	}
	return NULL;
}

static struct l_page *get_lpage(struct systask *task, size_t addr)
{
	assert((addr & PAGE_MASK) == 0);
	struct lp_group *g = get_lpg(task, addr & ~LPG_MASK);
	if(g == NULL) return NULL;
	struct l_page *ret = &g->pages[(addr & LPG_MASK) >> PAGE_BITS];
	return ret->p_addr != 0 ? ret : NULL;
}

static struct l_page *put_lpage(struct systask *task, size_t addr)
{
	assert((addr & PAGE_MASK) == 0);
	struct lp_group *g = get_lpg(task, addr & ~LPG_MASK);
	if(g == NULL) {
		struct p_page *bulk = get_free_page();
		memset((void *)P_ADDRESS(bulk), 0, PAGE_SIZE);
		static_assert(PAGE_SIZE / sizeof(struct l_page) == LPG_SIZE);
		list_add_tail(&reserved_page_list, &bulk->link);
		g = kmem_cache_alloc(lp_group_slab);
		*g = (struct lp_group){ .address = addr & ~LPG_MASK, .pages = (struct l_page *)P_ADDRESS(bulk) };
		assert((uintptr_t)&g->pages[LPG_SIZE - 1] - (uintptr_t)g->pages < PAGE_SIZE);
		put_lpg(task, g);
	}
	struct l_page *lp = &g->pages[(addr & LPG_MASK) >> PAGE_BITS];
	if(lp->p_addr == 0) { assert(g->count < LPG_SIZE); g->count++; }
	assert(g->count <= LPG_SIZE);
	return lp;
}

static void lpg_dtor(struct systask *t, struct lp_group *g)
{
	rb_erase(&g->rb_addr, &t->lpgs);
	rb_erase(&g->rb_phys, &lpg_phys_tree);
	struct p_page *phys = addr2p((uintptr_t)g->pages); if(phys == NULL) panic("phys not found in lpg_dtor??");
	list_del_from(&reserved_page_list, &phys->link);
	list_add(&free_page_list, &phys->link);
	num_free_pages++;
	kmem_cache_free(lp_group_slab, g);
}

static void del_lpage(struct systask *task, size_t addr)
{
	assert((addr & PAGE_MASK) == 0);
	struct lp_group *g = get_lpg(task, addr & ~LPG_MASK); if(g == NULL) return; /* idempotence */
	struct l_page *lp = &g->pages[(addr & LPG_MASK) >> PAGE_BITS]; if(lp->p_addr == 0) return; /* ^ */
	assert(g->count > 0);
	if(--g->count == 0) lpg_dtor(task, g); else lp->p_addr = 0;
	assert(get_lpage(task, addr & ~PAGE_MASK) == NULL);
}

static struct p_page *put_frame_helper(struct p_page *pp)
{
	struct rb_node **p = &phys_to_pp_tree.rb_node, *parent = NULL;
	size_t addr = pp->addr_flags & ~PAGE_MASK, oth_addr;
	while(*p != NULL) {
		parent = *p;
		struct p_page *oth = rb_entry(parent, struct p_page, phys_rb);
		oth_addr = oth->addr_flags & ~PAGE_MASK;
		if(addr < oth_addr) p = &(*p)->rb_left;
		else if(addr > oth_addr) p = &(*p)->rb_right;
		else return oth;
	}
	rb_link_node(&pp->phys_rb, parent, p);
	return NULL;
}

static void put_frame(struct p_page *pp) {
	if(put_frame_helper(pp) != NULL) panic("duplicate in put_frame()?");
	rb_insert_color(&pp->phys_rb, &phys_to_pp_tree);
}

struct p_page *addr2p(size_t addr)
{
	assert((addr & PAGE_MASK) == 0);
	struct rb_node *n = phys_to_pp_tree.rb_node;
	while(n != NULL) {
		struct p_page *cand = rb_entry(n, struct p_page, phys_rb);
		if(addr < (cand->addr_flags & ~PAGE_MASK)) n = n->rb_left;
		else if(addr > (cand->addr_flags & ~PAGE_MASK)) n = n->rb_right;
		else return cand;
	}
	return NULL;
}

static bool replace_active_page(struct p_page *p) {
	L4_Fpage_t fp = L4_FpageLog2(P_ADDRESS(p), PAGE_BITS);
	L4_Set_Rights(&fp, L4_FullyAccessible);
	L4_UnmapFpage(fp);
	return compress(p);
}

static int replace_pages(struct p_page **hand_p, bool *looped_p)
{
	assert(looped_p != NULL);

	struct p_page *hand = hand_p != NULL ? *hand_p : NULL;
	if(hand == NULL) hand = list_top(&active_page_list, struct p_page, link);
	if(hand == NULL) { *looped_p = false; return 0; }

	/* examine one Unmap syscall's worth of pages per call. */
	L4_Fpage_t unmaps[64];
	struct p_page *ps[ARRAY_SIZE(unmaps)];
	int n_seen = 0;
	struct p_page *start = hand;
	*looped_p = false;
	do {
		if(hand == NULL && list_empty(&active_page_list)) panic("active_page_list was drained. woe is me");
		assert(hand != NULL); assert(hand->owner != NULL);
		ps[n_seen] = hand;
		unmaps[n_seen] = L4_FpageLog2(P_ADDRESS(hand), PAGE_BITS);
		L4_Set_Rights(&unmaps[n_seen], 0);
		hand = list_next(&active_page_list, hand, link);
		if(hand == NULL) {
			hand = list_top(&active_page_list, struct p_page, link);
			*looped_p = true;
		}
	} while(++n_seen < ARRAY_SIZE(unmaps) && start != hand);
	L4_UnmapFpages(ARRAY_SIZE(unmaps), unmaps);
	int n_freed = 0;
	for(int i=0; i < n_seen; i++) {
		if(L4_Rights(unmaps[i]) != 0) {
			ps[i]->age++;
			if(L4_Rights(unmaps[i]) & L4_Writable) ps[i]->addr_flags &= ~PF_INCOMPRESSIBLE;
		} else if(ps[i]->age > 0) {
			ps[i]->age >>= 1;
		} else if((ps[i]->addr_flags & PF_INCOMPRESSIBLE) || (LP_FLAGS(ps[i]->owner) & LF_PIN)) {
			/* age pinned and incompressible pages, but don't replace them. */
		} else {
			/* actual replacement. */
			struct p_page *next = list_next(&active_page_list, ps[i], link);
			if(!replace_active_page(ps[i])) {
				ps[i]->addr_flags |= PF_INCOMPRESSIBLE;
			} else {
				assert(~ps[i]->addr_flags & PF_INCOMPRESSIBLE);
				if(ps[i] == hand) hand = next != NULL ? next : list_top(&active_page_list, struct p_page, link);
				n_freed++;
			}
		}
	}

	if(hand_p != NULL) *hand_p = hand;
	return n_freed;
}

struct p_page *get_free_page(void)
{
	struct p_page *p = list_pop(&free_page_list, struct p_page, link);
	if(p == NULL) panic("no free pages in get_free_page()!");
	num_free_pages--;
	p->age = 1;
	/* static low and high watermarks. sixteen is likely a bit on the high
	 * side; replacement shouldn't allocate any memory on its own.
	 */
	if(repl_enable && num_free_pages < 16) {
		int loops = 0, loop_freed = 0;
		struct p_page *hand = NULL;
		while(num_free_pages < 48) {
			bool looped;
			int n_freed = replace_pages(&hand, &looped);
			loop_freed += n_freed;
			if(looped) {
				if(loops > 1 && loop_freed == 0) break;
				loop_freed = 0;
				loops++;
			}
			//if(n_freed > 0) printf("sysmem: replaced %d pages (loops=%d)\n", n_freed, loops);
		}
		if(num_free_pages < 16) {
			/* TODO: have a protocol w/ vm to get more memory. */
			panic("couldn't replace memory in get_free_page()!");
		}
	}
	return p;
}

/* pagefault handling. this supports two use cases; normal fault service via
 * impl_handle_fault(), and in-band faults happening during interactions with
 * systasks such as may arise in e.g. Proc::create_thread().
 */
static bool handle_pf(L4_MapItem_t *page_ptr, L4_ThreadId_t tid, L4_Word_t faddr, L4_Word_t fip)
{
#if 0
	printf("pf in %lu:%lu, faddr=%#lx, fip=%#lx\n",
		L4_ThreadNo(tid), L4_Version(tid), faddr, fip);
#endif
	if((faddr & ~PAGE_MASK) == 0) {
		printf("sysmem: zero page access in %lu:%lu, fip=%#lx\n", L4_ThreadNo(tid), L4_Version(tid), fip);
		return false;
	}
	struct systask *task = get_task(pidof_NP(tid));
	if(task == NULL) {
		if(pidof_NP(tid) < SNEKS_MIN_SYSID) {
			printf("sysmem: illegal pf from non-systask tid=%lu:%lu (pid=%u)\n", L4_ThreadNo(tid), L4_Version(tid), pidof_NP(tid));
		} else {
			printf("sysmem: unknown tid=%lu:%lu (pid=%u)\n", L4_ThreadNo(tid), L4_Version(tid), pidof_NP(tid));
		}
		return false;
	}
	struct l_page *lp = get_lpage(task, faddr & ~PAGE_MASK);
	if(lp == NULL && (faddr & ~PAGE_MASK) > task->brk) {
		/* segfault */
		abend(PANIC_EXIT | PANICF_SEGV, "systask=%lu:%lu segv; faddr=%#lx fip=%#lx\n", L4_ThreadNo(tid), L4_Version(tid), faddr, fip);
		return false;
	} else if(lp == NULL || lp->p_addr == 0) {
		/* lazy allocation within brk range */
		if(lp == NULL) {
			lp = put_lpage(task, faddr & ~PAGE_MASK);
			*lp = (struct l_page){ .p_addr = faddr & ~PAGE_MASK };
		}
		struct p_page *phys = get_free_page();
		lp->p_addr = P_ADDRESS(phys);
		phys->owner = lp;
		list_add_tail(&active_page_list, &phys->link);
	} else if(lp->p_addr & LF_SWAP) {
		/* restore from swaps */
		struct p_page *phys = decompress(lp);
		lp->p_addr = (lp->p_addr & PAGE_MASK & ~LF_SWAP) | (P_ADDRESS(phys) & ~PAGE_MASK);
		phys->owner = lp;
		list_add_tail(&active_page_list, &phys->link);
	}
	task->min_fault = min_t(L4_Word_t, task->min_fault, faddr & ~PAGE_MASK);
	assert((lp->p_addr & ~PAGE_MASK) != 0);
	L4_Fpage_t fp = L4_FpageLog2(lp->p_addr & ~PAGE_MASK, PAGE_BITS);
	L4_Set_Rights(&fp, L4_FullyAccessible);
	*page_ptr = L4_MapItem(fp, faddr & ~PAGE_MASK);
	return true;
}

/* Sneks::Sysmem */
static void impl_handle_fault(L4_Word_t faddr, L4_Word_t fip, L4_MapItem_t *page_ptr) {
	if(!handle_pf(page_ptr, muidl_get_sender(), faddr, fip)) muidl_raise_no_reply();
}

/* see comment for handle_pf(). */
static bool interim_fault(L4_MsgTag_t tag, L4_ThreadId_t peer)
{
	if(L4_IpcFailed(tag) || tag.X.label >> 4 != 0xffe || tag.X.u != 2 || tag.X.t != 0) return false;
	L4_MapItem_t map;
	L4_Word_t faddr, fip;
	L4_StoreMR(1, &faddr); L4_StoreMR(2, &fip);
	L4_Acceptor_t acc = L4_Accepted();
	// printf("sysmem: interim fault addr=%#lx, ip=%#lx\n", faddr, fip);
	if(!handle_pf(&map, peer, faddr, fip)) { printf("%s: pager segfault!\n", __func__); abort(); }
	L4_Accept(acc);
	L4_LoadMR(0, (L4_MsgTag_t){ .X.t = 2 }.raw);
	L4_LoadMRs(1, 2, map.raw);
	return true;
}

static void unmap_page(L4_Word_t address, struct fpbuf *buf)
{
	L4_Fpage_t p = L4_FpageLog2(address, PAGE_BITS);
	L4_Set_Rights(&p, L4_FullyAccessible);
	if(buf == NULL) L4_UnmapFpages(1, &p);
	else {
		buf->fp[buf->n_fp++] = p;
		if(buf->n_fp == 64) { L4_UnmapFpages(64, buf->fp); buf->n_fp = 0; }
	}
}

static void remove_l_page(struct systask *task, struct l_page *lp, size_t addr, struct fpbuf *fp)
{
	if(LP_FLAGS(lp) & LF_SWAP) unswap(lp);
	else {
		struct p_page *phys = addr2p(lp->p_addr & ~PAGE_MASK);
		if(phys != NULL) { /* (NULL for unallocated pages such as from alter_flags) */
			unmap_page(lp->p_addr & ~PAGE_MASK, fp);
			assert(phys->owner == lp);
			list_del_from(&active_page_list, &phys->link);
			phys->owner = NULL;
			list_add(&free_page_list, &phys->link);
			num_free_pages++;
		}
	}
	assert(get_lpage(task, addr) == lp);
	del_lpage(task, addr);
}

static void remove_lp_range(struct systask *task, size_t first, size_t last)
{
	size_t addr = first;
	struct lp_group *g = (first & ~LPG_MASK) == 0 ? container_of_or_null(rb_first(&task->lpgs), struct lp_group, rb_addr) : NULL;
	while(g == NULL && addr < last) {
		g = get_lpg(task, addr & ~LPG_MASK);
		if(g == NULL) addr = (addr + LPG_SIZE) & ~LPG_MASK;
	}
	struct fpbuf fp = { };
	while(g != NULL && addr < last) {
		size_t next = (addr + LPG_SIZE) & ~LPG_MASK;
		struct lp_group *next_g = container_of_or_null(rb_next(&g->rb_addr), struct lp_group, rb_addr);
		for(int i = addr == first ? (first & LPG_MASK) / sizeof g->pages[0] : 0, lim = last < next ? (last - addr + 1) / sizeof g->pages[0] : LPG_SIZE; i < lim; i++) {
			int ct = g->count;
			if(g->pages[i].p_addr != 0) {
				remove_l_page(task, &g->pages[i], g->address + i * PAGE_SIZE, &fp);
				if(ct == 1) break;
			}
		}
		g = next_g;
		if(g != NULL) addr = g->address;
	}
	if(fp.n_fp > 0) L4_UnmapFpages(fp.n_fp, fp.fp);
}

static void impl_rm_task(int32_t pid)
{
	if(pid < SNEKS_MIN_SYSID) return;
	struct systask *t = find_task(pid); if(t == NULL) return;
	struct fpbuf fps = { };
	for(struct rb_node *cur = rb_first(&t->lpgs), *next; cur != NULL; cur = next) {
		next = rb_next(cur);
		struct lp_group *g = container_of(cur, struct lp_group, rb_addr);
		for(int i = 0, ct = g->count; ct > 0 && i < LPG_SIZE; i++) {
			if(g->pages[i].p_addr == 0) continue;
			ct--; remove_l_page(t, &g->pages[i], (g->address & ~LPG_MASK) + i * PAGE_SIZE, &fps);
		}
	}
	assert(rb_first(&t->lpgs) == NULL);
	if(fps.n_fp > 0) L4_UnmapFpages(fps.n_fp, fps.fp);
	systask_dtor(t);
}

static void add_free_page(struct p_page *pg)
{
	/* "any old RAM" mode */
	struct list_head *list;
	/* recognize sysmem's own pages, add them to the reserved list. */
	extern char _start, _end;
	L4_Word_t top = ((L4_Word_t)&_end + PAGE_MASK) & ~PAGE_MASK, phys_addr = P_ADDRESS(pg);
	if(phys_addr >= (L4_Word_t)&_start && phys_addr < top) {
		/* exclude early memory from add_first_mem(), since it's already
		 * tracked.
		 */
		if(phys_addr >= (L4_Word_t)&first_mem[0] && phys_addr < (L4_Word_t)&first_mem[sizeof first_mem]) {
			list = NULL;
		} else {
			/* early memory (program image) not within first_mem[]. */
			list = &reserved_page_list;
		}
	} else {
		list = &free_page_list;
	}
	if(list != NULL) {
		list_add_tail(list, &pg->link);
		if(list == &free_page_list && ++num_free_pages > 60 && !repl_enable) repl_enable = true;
	}
}

/* idempotent maps for ELF loads of boot modules */
static void add_task_page(struct systask *task, struct p_page *pg)
{
	L4_Word_t phys_addr = P_ADDRESS(pg);
	struct l_page *lp = put_lpage(task, phys_addr);
	*lp = (struct l_page){ .p_addr = phys_addr };
	pg->owner = lp;
	pg->age = 1;
	list_add_tail(&active_page_list, &pg->link);
	task->min_fault = min_t(L4_Word_t, task->min_fault, phys_addr & ~PAGE_MASK);
	task->brk = max_t(L4_Word_t, task->brk, phys_addr | PAGE_MASK);
}

static void impl_send_phys(L4_Word_t dest_raw, L4_Word_t frame_num, int size_log2)
{
	L4_ThreadId_t dest_tid = { .raw = dest_raw };
	assert(L4_IsNilThread(dest_tid) || L4_IsGlobalId(dest_tid));
	struct systask *task = NULL;
	if(!L4_IsNilThread(dest_tid)) {
		int dest_pid = pidof_NP(dest_tid);
		if(dest_pid < SNEKS_MIN_SYSID) {
			printf("%s: dest_pid=%d is a no-go\n", __func__, dest_pid);
			return;
		}
		task = get_task(dest_pid);
	}
	L4_ThreadId_t sender = muidl_get_sender();
	L4_Accept(L4_MapGrantItems(L4_FpageLog2(frame_num << PAGE_BITS, size_log2)));
	L4_MsgTag_t tag = L4_Receive_Timeout(sender, L4_TimePeriod(5 * 1000));
	while(interim_fault(tag, sender)) {
		/* NOTE: this refreshes the timeout. */
		tag = L4_Call_Timeouts(sender, L4_ZeroTime, L4_TimePeriod(5 * 1000));
	}
	L4_Accept(L4_UntypedWordsAcceptor);
	if(L4_IpcFailed(tag)) {
		printf("failed send_phys second transaction, ec=%lu\n", L4_ErrorCode());
		return;
	} else if(L4_Label(tag) != 0 || L4_UntypedWords(tag) != 0 || L4_TypedWords(tag) != 2) {
		printf("out-of-band message in send_phys; tag=%#lx!\n", tag.raw);
		return;
	}
	int n_pages = 1 << (size_log2 - PAGE_BITS);
	for(int i=0; i < n_pages; i++) {
		size_t addr = (frame_num + i) << PAGE_BITS;
		if(addr2p(addr) != NULL) { assert(task == NULL); continue; } /* sysmem's initial pages, already listed */
		struct p_page *pg = kmem_cache_alloc(pp_slab);
		*pg = (struct p_page){ .addr_flags = addr };
		if(task == NULL) add_free_page(pg); else add_task_page(task, pg);
		put_frame(pg);
	}
}

static uint16_t impl_send_virt(L4_Word_t src_addr, L4_Word_t dest_tid_raw, L4_Word_t dest_addr)
{
	int src_pid = pidof_NP(muidl_get_sender()); if(src_pid < SNEKS_MIN_SYSID) return EINVAL;
	struct systask *src = get_task(src_pid); if(src == NULL) return ENOENT;
	struct l_page *lp = get_lpage(src, src_addr); if(lp == NULL) return EFAULT;
	struct fpbuf fps = { };
	L4_ThreadId_t dest_tid = { .raw = dest_tid_raw };
	if(!L4_IsNilThread(dest_tid)) {
		int dest_pid = pidof_NP(dest_tid); if(dest_pid < SNEKS_MIN_SYSID) return EINVAL;
		struct systask *dest = get_task(dest_pid);
		struct p_page *phys = addr2p(LP_PHYS(lp)); if(phys == NULL) return EINVAL;
		struct l_page *dstp = put_lpage(dest, dest_addr);
		if(dstp->p_addr != 0) {
			remove_l_page(dest, dstp, dest_addr, &fps);
			dstp = put_lpage(dest, dest_addr);
		}
		unmap_page(lp->p_addr & ~PAGE_MASK, &fps);
		*dstp = *lp; del_lpage(src, src_addr);
		assert(phys->owner == lp); phys->owner = dstp;
		dest->min_fault = min_t(L4_Word_t, dest->min_fault, dest_addr & ~PAGE_MASK);
		dest->brk = max_t(L4_Word_t, dest->brk, dest_addr | PAGE_MASK);
	} else if(L4_IsNilThread(dest_tid)) {
		/* just toss the page. */
		remove_l_page(src, lp, src_addr, &fps);
	} else {
		return ENOENT;
	}
	if(fps.n_fp > 0) L4_UnmapFpages(fps.n_fp, fps.fp);
	return 0;
}

static void impl_brk(L4_Word_t new_brk)
{
	L4_ThreadId_t sender = muidl_get_sender();
	int pid = pidof_NP(sender); if(pid < SNEKS_MIN_SYSID) { printf("sysmem: %s: invalid pid=%d\n", __func__, pid); return; }
	struct systask *task = get_task(pid); if(task == NULL) { printf("sysmem: %s: no such task\n", __func__); return; }
	if(new_brk < task->brk) remove_lp_range(task, new_brk, task->brk);
	task->brk = new_brk;
}

static unsigned short impl_breath_of_life(L4_Word_t tid_raw, L4_Word_t ip, L4_Word_t sp) {
	L4_ThreadId_t tid = { .raw = tid_raw };
	L4_LoadMR(0, (L4_MsgTag_t){ .X.u = 2 }.raw); L4_LoadMR(1, ip); L4_LoadMR(2, sp);
	L4_MsgTag_t tag = L4_Send_Timeout(tid, L4_TimePeriod(2 * 1000));
	return L4_IpcFailed(tag) ? L4_ErrorCode() : 0;
}

static int impl_get_shape(L4_Word_t *low_p, L4_Word_t *high_p, L4_Word_t task_raw)
{
	L4_ThreadId_t task = { .raw = task_raw };
	struct systask *t = find_task(pidof_NP(task)); if(t == NULL) return ENOENT;
#define FP_EDGES(p) L4_Address((p)), L4_Address((p)) + L4_Size((p)) - 1
	L4_Word_t edges[] = { t->brk, t->min_fault, FP_EDGES(t->utcb_area), FP_EDGES(t->kip_area) };
#undef FP_EDGES
	*low_p = ~PAGE_MASK; *high_p = 0;
	for(int i=0; i < ARRAY_SIZE(edges); i++) {
		*low_p = min(*low_p, edges[i]);
		*high_p = max(*high_p, edges[i]);
	}
	return 0;
}

static int impl_set_kernel_areas(L4_Word_t task_raw, L4_Fpage_t utcb_area, L4_Fpage_t kip_area)
{
	L4_ThreadId_t task = { .raw = task_raw };
	struct systask *t = find_task(pidof_NP(task)); if(t == NULL) return ENOENT;
	if(L4_IsNilFpage(utcb_area) || L4_IsNilFpage(kip_area)) return EINVAL;
	/* FIXME: check utcb_area and kip_area for being out of userspace VM
	 * range.
	 */
	if(!L4_IsNilFpage(t->utcb_area) || !L4_IsNilFpage(t->kip_area)) return EEXIST;
	t->utcb_area = utcb_area; L4_Set_Rights(&t->utcb_area, L4_ReadWriteOnly);
	t->kip_area = kip_area; L4_Set_Rights(&t->kip_area, L4_ReadWriteOnly);
	return 0;
}

static int impl_alter_flags(L4_Word_t task_raw, L4_Fpage_t range, L4_Word_t or_mask, L4_Word_t and_mask)
{
	if((or_mask & ~and_mask) != 0) return -EINVAL;
	if(L4_Size(range) < PAGE_BITS) return -EINVAL;
	L4_ThreadId_t sender = muidl_get_sender(), task = { .raw = task_raw };
	if(!L4_IsNilThread(task) && pidof_NP(sender) == pidof_NP(task)) return -EINVAL;
	if(L4_IsNilThread(task)) task = muidl_get_sender();
	struct systask *t = find_task(pidof_NP(task));
	if(t == NULL) return -EINVAL;
	if(fpage_overlap(t->utcb_area, range) || fpage_overlap(t->kip_area, range)) return -EINVAL;
	/* bruteforce wrt the page tree. doesn't matter until it does.
	 * (and <struct l_page> could well sit in a hash table and not rbtree with
	 * its three-word nodes of irreplaceable memory.)
	 */
	for(L4_Word_t addr = L4_Address(range); addr < L4_Address(range) + L4_Size(range); addr += PAGE_SIZE) {
		struct l_page *lp = put_lpage(t, addr);
		if(lp->p_addr == 0) *lp = (struct l_page){ .p_addr = or_mask & PAGE_MASK };
		else {
			size_t new_addr = (lp->p_addr & ~PAGE_MASK) | ((lp->p_addr | or_mask) & and_mask & PAGE_MASK);
			if(new_addr == 0) del_lpage(t, addr); else lp->p_addr = new_addr;
		}
	}
	return 0;
}

static void add_first_mem(void)
{
	for(int i=0; i < NUM_INIT_PAGES; i++) {
		struct p_page *phys = malloc(sizeof *phys);
		*phys = (struct p_page){ .addr_flags = (L4_Word_t)&first_mem[i * PAGE_SIZE] };
		assert((phys->addr_flags & PAGE_MASK) == 0);
		list_add_tail(&free_page_list, &phys->link);
		num_free_pages++;
		put_frame(phys);
	}
}

static void handle_iopf(L4_ThreadId_t sender, L4_MsgTag_t tag, L4_Fpage_t iofp, L4_Word_t eip)
{
	assert(L4_IsIoFpage(iofp));
	assert(!L4_SameThreads(sender, L4_Pager()));
	assert(!L4_IsNilThread(L4_Pager()));
	//printf("iopf in %lu:%lu, port=%#lx:%#lx, eip=%#lx\n", L4_ThreadNo(sender), L4_Version(sender), L4_IoFpagePort(iofp), L4_IoFpageSize(iofp), eip);
	/* forward to sigma0. */
	L4_LoadMR(0, tag.raw);
	L4_LoadMR(1, iofp.raw);
	L4_LoadMR(2, 0xdeadbeef);
	L4_Accept(L4_MapGrantItems(L4_IoFpageLog2(0, 16)));
	L4_MsgTag_t tt = L4_Call(s0_tid);
	if(L4_IpcFailed(tt)) { printf("sysmem: can't forward iopf: ec=%lu\n", L4_ErrorCode()); return; }
	L4_MapItem_t map = L4_MapItem(iofp, 0);
	L4_Accept(L4_UntypedWordsAcceptor);
	L4_LoadMR(0, (L4_MsgTag_t){ .X.t = 2 }.raw);
	L4_LoadMRs(1, 2, map.raw);
	tt = L4_Reply(sender);
	if(L4_IpcFailed(tt)) printf("sysmem: can't send iopf reply: ec=%lu\n", L4_ErrorCode());
}

static int initialize(void)
{
	int n; if(n = sneks_setup_console_stdio(), n != 0) { con_putstr("can't setup console stdio in sysmem!"); return n; }
	systask_slab = KMEM_CACHE_NEW("systask_slab", struct systask);
	pp_slab = KMEM_CACHE_NEW("pp_slab", struct p_page);
	lp_group_slab = KMEM_CACHE_NEW("lp_group_slab", struct lp_group);
	return 0;
}

int main(void)
{
	the_kip = L4_GetKernelInterface();
	s0_tid = L4_GlobalId(the_kip->ThreadInfo.X.UserBase, 1);
	add_first_mem();
	if(initialize() != 0) abort();
	static struct sysmem_impl_vtable vtab = {
		/* L4.X2 */
		.handle_fault = &impl_handle_fault,
		/* Sysmem proper */
		.rm_task = &impl_rm_task,
		.send_phys = &impl_send_phys,
		.send_virt = &impl_send_virt,
		.brk = &impl_brk,
		.breath_of_life = &impl_breath_of_life,
		.get_shape = &impl_get_shape,
		.set_kernel_areas = &impl_set_kernel_areas,
		.alter_flags = &impl_alter_flags,
	};
	set_sysinfo_vtab(&vtab);
	for(;;) {
		L4_Word_t status = _muidl_sysmem_impl_dispatch(&vtab);
		if(status == MUIDL_UNKNOWN_LABEL) {
			/* special sysinfo initialization stuff. */
			L4_ThreadId_t sender = muidl_get_sender();
			L4_MsgTag_t tag = muidl_get_tag();
			/* I/O faults */
			if((tag.X.label & 0xfff0) == 0xff80 && tag.X.u == 2 && tag.X.t == 0) {
				L4_Fpage_t iofp; L4_StoreMR(1, &iofp.raw);
				L4_Word_t eip; L4_StoreMR(2, &eip);
				handle_iopf(sender, tag, iofp, eip);
				continue;
			}
			L4_Word_t mrs[64]; L4_StoreMRs(1, tag.X.u + tag.X.t, mrs);
			if(L4_ThreadNo(sender) < L4_ThreadNo(L4_Myself()) && L4_Label(tag) == 0xbaaf) {
				sysinfo_init_msg(tag, mrs);
			} else {
				/* do nothing. */
				printf("sysmem: unknown message label=%#lx, u=%lu, t=%lu\n", L4_Label(tag), L4_UntypedWords(tag), L4_TypedWords(tag));
			}
		} else if(status != 0 && !MUIDL_IS_L4_ERROR(status)) {
			printf("sysmem: dispatch status %#lx (last tag %#lx)\n", status, muidl_get_tag().raw);
		}
	}
	return 0;
}
