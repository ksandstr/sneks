/* how to fork in userspace.
 *
 * mung's testbench uses the older method of starting a helper thread and popping
 * an exception to it because the exception message captures the 32-bit x86
 * integer context completely. however, on architectures with more registers,
 * L4.X2's exception messages only carry some of the registers back and forth,
 * so that'd not work outside ia32.
 *
 * instead of that, we'll store the processor context in RAM that gets forked
 * just like any other, and restore the context using an accessory stack
 * (allocated on the fork() caller's stack because of restrictions on malloc).
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdatomic.h>
#include <sys/types.h>
#include <unistd.h>
#include <alloca.h>
#include <ucontext.h>
#include <ccan/array_size/array_size.h>

#include <l4/types.h>
#include <l4/thread.h>

#include <sneks/thrd.h>
#include <sneks/process.h>
#include <sneks/sysinfo.h>
#include <sneks/api/proc-defs.h>

#include "private.h"

typedef void (*atfork_fn)(void);

struct atfork {
	struct atfork *next;
	atfork_fn prepare, parent, child;
};

static struct atfork *_Atomic atfstack = NULL;

static struct atfork *alloc_atfork(void) {
	static struct atfork first[4];
	static int count = 0;
	if(count < ARRAY_SIZE(first)) return &first[count++];
	else return count++, malloc(sizeof(struct atfork));
}

int __thrd_atfork(void (*prepare)(void), void (*parent)(void), void (*child)(void)) {
	struct atfork *it = alloc_atfork(); if(it == NULL) return -1;
	*it = (struct atfork){ .prepare = prepare, .parent = parent, .child = child, .next = atfstack };
	while(!atomic_compare_exchange_strong(&atfstack, &it->next, it)) /* keep on trucking */ ;
	return 0;
}

pid_t fork(void)
{
	struct atfork *rev = NULL;
	for(struct atfork *cur = atfstack; cur != NULL; cur = cur->next) {
		struct atfork *copy = alloca(sizeof *copy); *copy = *cur;
		copy->next = rev; rev = copy;
		if(cur->prepare != NULL) (*cur->prepare)();
	}

	/* TODO: generate file descriptor buffers */

	volatile L4_ThreadId_t parent_tid = L4_MyGlobalId();
	ucontext_t child_ctx;
	getcontext(&child_ctx);
	if(parent_tid.raw != L4_MyGlobalId().raw) {
		/* CHILD SIDE. */
		__thrd_init();
		for(struct atfork *cur = rev; cur != NULL; cur = cur->next) {
			if(cur->child != NULL) (*cur->child)();
		}
		return 0;
	} else {
		/* PARENT SIDE. launch the child process. */
		char tmpstack[128] __attribute__((aligned(16)));
		volatile L4_Word_t *stktop = (L4_Word_t *)&tmpstack[sizeof tmpstack];
		*(--stktop) = (L4_Word_t)&child_ctx;
		*(--stktop) = 0xfee7dead; /* return to fucksville, kernel range */
		L4_ThreadId_t child_tid;
		int n = __proc_fork(__the_sysinfo->api.proc, &child_tid.raw, (L4_Word_t)stktop, (L4_Word_t)&setcontext);
		pid_t child_pid = n == 0 ? pidof_NP(child_tid) : -1;
		errno = -n;
		for(struct atfork *cur = rev; cur != NULL; cur = cur->next) {
			if(cur->parent != NULL) (*cur->parent)();
		}
		return child_pid;
	}
}
