/* userspace compat for lib/thrd.c etc.
 *
 * current userspace doesn't support multithreaded user programs. instead
 * thrd_*() is provided for internal use; so far these threads are like POSIX
 * threads, but can't receive signals and can't safely interact with portable
 * subprograms or subsystems. that'll change, but not today.
 */
#include <stdlib.h>
#include <threads.h>
#include <l4/types.h>
#include <l4/thread.h>
#include <sneks/thrd.h>
#include <sneks/sysinfo.h>
#include <sneks/api/proc-defs.h>
#include "private.h"

const int __thrd_stksize_log2 = 21;	/* bofa deez */

int __thrd_new(L4_ThreadId_t *res) {
	return __proc_create_thread(__the_sysinfo->api.proc, &res->raw);
}

int __thrd_destroy(L4_ThreadId_t tid) {
	return __proc_remove_thread(__the_sysinfo->api.proc, tid.raw, L4_LocalIdOf(tid).raw);
}
