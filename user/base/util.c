#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdnoreturn.h>
#include <stdlib.h>
#include "util.h"

noreturn void die(const char *fmt, ...)
{
	va_list ap; va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if(fmt[0] != '\0' && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
	exit(EXIT_FAILURE);
}

noreturn void dien(int n, const char *fmt, ...)
{
	va_list ap; va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if(fmt[0] != '\0' && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror_ipc_NP(NULL, n);
	} else {
		fputc('\n', stderr);
	}
	exit(EXIT_FAILURE);
}
