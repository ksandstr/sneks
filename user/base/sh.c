/* not a proper sh(1), but enough to resolve a relative hashbang interpreter
 * as required by execve.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <ccan/str/str.h>
#include <ccan/array_size/array_size.h>
#include "util.h"

int main(int argc, char *argv[])
{
	if(argc < 1 || !streq(argv[1], "-c")) die("sh: you must be looking for a smarter /bin/sh");
	if(argc < 2) die("sh: must have command_file");
	const char *cmdfile = argv[2], *args[argc];
	int nargs = 0;
	args[nargs++] = argc >= 3 ? argv[3] : cmdfile;
	for(int i=4; i <= argc; i++) args[nargs++] = argv[i];
	args[nargs++] = NULL;
	assert(nargs <= ARRAY_SIZE(args));
	execvp(cmdfile, (char **)args);
	die("sh: can't execute `%s':", cmdfile);
	return EXIT_FAILURE;
}
