#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <l4/types.h>
#include <ccan/str/str.h>
#include <ccan/opt/opt.h>
#include <ccan/minmax/minmax.h>
#include <ccan/typesafe_cb/typesafe_cb.h>
#include <ccan/compiler/compiler.h>
#include <sneks/crtprivate.h>
#include <sneks/api/io-defs.h>
#include <sneks/api/loopback-defs.h>
#include "util.h"

typedef int (*loopdev_each_fn)(unsigned, struct sneks_loop_control_cfg *, void *);

static bool do_version = false, do_list = false, list_all = false, do_find = false, ro_arg = false, do_partscan = false, show_arg = false, detach_all = false;
static char *detach_arg = NULL, *resize_arg = NULL, *list_assoc = NULL;
static unsigned long long offset_arg = 0, sizelimit_arg = 0;
static unsigned long sec_size_arg = 0;
static int (*op_fn)(char *[]) = NULL;

static PURE_FUNCTION L4_ThreadId_t ctlserver(void)
{
	static L4_ThreadId_t val = { .raw = 0 };
	if(L4_IsNilThread(val)) {
		int ctldev = open("/dev/loop-control", O_RDONLY);
		if(ctldev < 0) die("can't open /dev/loop-control:");
		val = __fdbits(ctldev)->server;
		assert(!L4_IsNilThread(val));
	}
	return val;
}

static int _loopdev_for_each(loopdev_each_fn fn, void *priv)
{
	int rc = 0, newmax = -1, n;
	for(int i = 0, max_id = 0; i <= max_id; i++) {
		struct sneks_loop_control_cfg lc; bool got = false;
		if(n = __loopctl_get_info(ctlserver(), &got, i, &newmax, &lc), n != 0) dien(n, "LoopControl/get_info failed:");
		if(got) rc |= (*fn)(i, &lc, priv);
		max_id = max(max_id, newmax);
	}
	return rc;
}
#define loopdev_for_each(fn, priv) \
	_loopdev_for_each(typesafe_cb_cast3(loopdev_each_fn, \
			int (*)(unsigned, struct sneks_loop_control_cfg *, typeof(*(priv)) *), \
			int (*)(unsigned, struct sneks_loop_control_cfg *, const typeof(*(priv)) *), \
			int (*)(unsigned, struct sneks_loop_control_cfg *, const void *), (fn)), \
		(priv))

static bool match_argv(unsigned minor, char *argv[]) {
	char name[20]; snprintf(name, sizeof name, "/dev/loop%u", minor);
	for(int i=0; argv[i] != NULL; i++) {
		if(streq(argv[i], name)) return true;
	}
	return false;
}

static int list_dev(unsigned minor, struct sneks_loop_control_cfg *lc, char *argv[])
{
	/* TODO: filter by pair of backing file and offset */
	if(!list_all && !match_argv(minor, argv)) return EXIT_SUCCESS;
	static bool first = true;
	if(first) {
		printf("NAME BACK-FILE OFFSET SIZELIMIT DIO LOG_SEC AUTOCLEAR RO\n");
		first = false;
	}
	char backpath[SNEKS_PATH_PATH_MAX + 1]; bool got = false;
	int n = __loopctl_get_back_file(ctlserver(), &got, minor, backpath); if(n != 0) dien(n, "LoopControl/get_back_file failed:");
	if(streq(backpath, "-")) strscpy(backpath, "\\-", sizeof backpath); /* pedancius maximus */
	else if(!got || backpath[0] == '\0') strscpy(backpath, "-", sizeof backpath);
	printf("/dev/loop%u %s %lld %lld %lld %lld %d %d\n", minor, backpath, lc->offset, lc->sizelimit, lc->dio, lc->log_sec, lc->autoclear, lc->ro);
	return EXIT_SUCCESS;
}

static int list(char *argv[]) {
	return loopdev_for_each(&list_dev, argv);
}

static int detach_if(unsigned minor, struct sneks_loop_control_cfg *lc, char *argv[])
{
	if(detach_all || match_argv(minor, argv)) {
		int n = __loopctl_detach(ctlserver(), minor);
		if(n != 0) dien(n, "LoopControl/detach failed:");
	}
	return EXIT_SUCCESS;
}

static int detach(char *argv[]) {
	return loopdev_for_each(&detach_if, ((char *[]){ detach_arg, NULL }));
}

static int resize(char *argv[]) {
	die("resize not implemented");
}

static int attach(char *argv[])
{
	if(argv[0] == NULL || (!do_find && argv[1] == NULL)) die("needs device (or -f) and backing file");
	const char *dev = do_find ? NULL : argv[0], *src = argv[do_find ? 0 : 1];
	int n, reqid, gotid = -1, backfd, duph, flags = 0;
	if(do_find) reqid = -1;
	else {
		struct stat st;
		if(n = stat(dev, &st), n < 0) die("stat %s:", dev);
		if(!S_ISBLK(st.st_mode) || major(st.st_rdev) != 7) die("`%s' not a loop device", dev);
		reqid = minor(st.st_rdev);
	}
	if(backfd = open(src, O_RDONLY), backfd < 0) die("can't open backing file `%s':", src);
	struct fd_bits *bb = __fdbits(backfd);
	if(n = __io_dup_to(bb->server, &duph, bb->handle, pidof_NP(ctlserver())), n != 0) dien(n, "IO/dup_to failed:");
	struct sneks_loop_control_cfg cfg = { }; /* TODO: compose opt args into cfg, flags */
	if(n = __loopctl_attach(ctlserver(), &gotid, reqid, flags, &cfg, bb->server.raw, duph), n != 0) dien(n, "LoopControl/attach failed:");
	if(show_arg && gotid >= 0) printf("/dev/loop%d", gotid);
	close(backfd);
	return EXIT_SUCCESS;
}

static int version(char *argv[]) {
	printf("losetup from sneks vN.N\n"); /* TODO: proper version */
	return EXIT_SUCCESS;
}

static void mode(int (*fn)(char *[])) {
	if(op_fn != NULL) die("can only specify one operation");
	op_fn = fn;
}

static bool is_loop(const char *name) {
	return strstarts(name, "/dev/loop");	/* TODO: or stat if relative */
}

int main(int argc, char *argv[])
{
	static const struct opt_table opts[] = { /* table opts, opts on a table */
		OPT_WITHOUT_ARG("-V|--version", &opt_set_bool, &do_version, "show version"),
		OPT_WITHOUT_ARG("-l|--list", &opt_set_bool, &do_list, "list loop device information"),
		OPT_WITHOUT_ARG("-a|--all", &opt_set_bool, &list_all, "list all loop devices"),
		OPT_WITH_ARG("-j <file>|--associated=<file>", &opt_set_charp, &opt_show_charp, &list_assoc, "list devices associated with <file>"),
		OPT_WITHOUT_ARG("-f|--find", &opt_set_bool, &do_find, "find an unattached loop device"),
		OPT_WITHOUT_ARG("--show", &opt_set_bool, &show_arg, "show path to newly attached loop device"),
		OPT_WITHOUT_ARG("-r|--read-only", &opt_set_bool, &ro_arg, "set up a read-only loop device"),
		OPT_WITHOUT_ARG("-P|--partscan", &opt_set_bool, &do_partscan, "force partition table scan"),
		OPT_WITH_ARG("-d <arg>|--detach=<arg>", &opt_set_charp, &opt_show_charp, &detach_arg, "detach <arg>"),
		OPT_WITHOUT_ARG("-D|--detach-all", &opt_set_bool, &detach_all, "detach all"),
		OPT_WITH_ARG("-c <arg>|--resize=<arg>", &opt_set_charp, &opt_show_charp, &resize_arg, "resize <arg>"),
		OPT_WITH_ARG("-o <offset>", &opt_set_ulonglongval_bi, &opt_show_ulonglongval_bi, &offset_arg, "offset within <file>"),
		OPT_WITH_ARG("--sizelimit=<limit>", &opt_set_ulonglongval_bi, &opt_show_ulonglongval_bi, &sizelimit_arg, "size limit within <file>"),
		OPT_WITH_ARG("-b <sec-size>|--sector-size=<sec-size>", &opt_set_ulongval_bi, &opt_show_ulongval_bi, &sec_size_arg, "logical sector size"),
		OPT_ENDTABLE
	};
	opt_register_table(opts, NULL);
	if(!opt_parse(&argc, argv, &opt_log_stderr)) return EXIT_FAILURE;
	if(do_version) mode(&version);
	if(do_list || list_assoc != NULL) mode(&list);
	if(detach_arg != NULL || detach_all) mode(&detach);
	if(resize_arg != NULL) mode(&resize);
	if(do_find || (argc == 3 && is_loop(argv[1]))) mode(&attach);
	if(op_fn == NULL && argc == 2 && is_loop(argv[1])) mode(&list);
	return op_fn != NULL ? (*op_fn)(argv + 1) : EXIT_SUCCESS;
}
