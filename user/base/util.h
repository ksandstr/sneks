#ifndef _USER_BASE_UTIL_H
#define _USER_BASE_UTIL_H

#include <stdnoreturn.h>
#include <ccan/compiler/compiler.h>

extern noreturn void die(const char *fmt, ...) PRINTF_FMT(1, 2);
extern noreturn void dien(int n, const char *fmt, ...) PRINTF_FMT(2, 3);

#endif
