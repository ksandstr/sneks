
/* tests of various utility things provided in the sneks and mung trees. */

#include <sneks/bitops.h>
#include <sneks/test.h>


START_TEST(msb_bitness)
{
	plan_tests(3);

	ok1(MSB(1) == 0);
	ok1(MSB(1 << 15) == 15);
	ok1(MSB(1 << 31) == 31);
}
END_TEST

DECLARE_TEST("self:util", msb_bitness);


START_TEST(msbll_bitness)
{
	plan_tests(5);

	ok1(MSBLL(1) == 0);
	ok1(MSBLL(1 << 15) == 15);
	ok1(MSBLL(1ull << 31) == 31);
	ok1(MSBLL(1ull << 32) == 32);
	ok1(MSBLL(1ull << 63) == 63);
}
END_TEST

DECLARE_TEST("self:util", msbll_bitness);


/* TODO: start using the c11thrd.c etc. test sets for userspace threads, then
 * remove this old crud.
 */
#ifdef __sneks__
#include <threads.h>

static int set_int_fn(void *ptr) { *(int *)ptr = 1; return 0; }

START_TEST(crt_thread_basic)
{
	plan_tests(3);
	int *value = malloc(sizeof *value); fail_if(value == NULL, "malloc"); *value = 0;
	thrd_t t;
	skip_start(!ok(thrd_create(&t, &set_int_fn, value) == thrd_success, "create"), 2, "no thread") {
		diag("tid=%lu:%lu", L4_ThreadNo(tidof(t)), L4_Version(tidof(t)));
		ok(thrd_join(t, NULL) == thrd_success, "join");
		if(!ok(*value != 0, "value was altered")) diag("*value=%d", *value);
	} skip_end;
	free(value);
}
END_TEST

DECLARE_TEST("self:crt", crt_thread_basic);
#endif
