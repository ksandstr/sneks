/* tests on the loopback facility. */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/mount.h>
#include <ccan/array_size/array_size.h>
#include <ccan/pipecmd/pipecmd.h>
#include <ccan/darray/darray.h>
#include <ccan/compiler/compiler.h>
#include <ccan/str/str.h>
#include <sneks/test.h>

struct loopcfg {
	char *name, *back_file;
	off_t offset, sizelimit, autoclear, ro, dio, log_sec;
};

static int losetup_rc;

/* TODO: move these into a <sneks/strvec.h> or something? */
static void strfreev(char **vec) {
	if(vec == NULL) return;
	for(int i=0; vec[i] != NULL; i++) free(vec[i]);
	free(vec);
}

static size_t strvlen(char *const *vec) {
	size_t i = 0;
	while(vec[i] != NULL) i++;
	return i;
}

static char **strsplit(const char *input, char sep)
{
	darray(char *) pieces = darray_new();
	char *mark;
	do {
		mark = strchrnul(input, sep);
		darray_append(pieces, strndup(input, mark - input));
		while(*mark == sep) mark++;
		input = mark;
	} while(*mark != '\0');
	darray_append(pieces, NULL);
	return pieces.item;
}

static LAST_ARG_NULL char **run_losetup(int flags, ...)
{
	assert(flags == 0);
	va_list al; va_start(al, flags);
	int outfd, child = pipecmdv(NULL, &outfd, &pipecmd_preserve, "losetup", al);
	if(child < 0) {
		diag("failed to run losetup: %s", strerror(errno));
		losetup_rc = -errno;
		return NULL;
	}
	FILE *f = fdopen(outfd, "r");
	char line[200];
	darray(char *) lines = darray_new();
	while(fgets(line, sizeof line, f) != NULL) {
		while(line[strlen(line) - 1] == '\n') line[strlen(line) - 1] = '\0';
		darray_append(lines, strdup(line));
		diag("losetup: %s", line);
	}
	fclose(f);
	int st, dead = waitpid(child, &st, 0);
	if(dead < 0) {
		diag("waitpid failed: %s", strerror(errno));
		losetup_rc = -1;
	} else {
		fail_unless(dead == child);
		if(!WIFEXITED(st)) diag("abnormal exit status=%#x", st);
		losetup_rc = !WIFEXITED(st) ? -666 : WEXITSTATUS(st);
	}
	darray_append(lines, NULL);
	return lines.item;
}

/* demonstrate that losetup exists, and reports its version number. */
START_TEST(losetup_basic)
{
	plan_tests(4);
	char **ls = run_losetup(0, NULL);
	if(!ok(losetup_rc == 0, "losetup with no args")) diag("rc=%d", losetup_rc);
	ok(ls != NULL && ls[0] == NULL, "... returned no lines");
	strfreev(ls); ls = run_losetup(0, "--version", NULL);
	skip_start(!ok(losetup_rc == 0, "responds to --version"), 1, "--version failed") {
		char **v = strsplit(ls[0], ' ');
		int len = strvlen(v);
		ok(len >= 4 && streq(v[0], "losetup") && streq(v[1], "from"), "version format");
		if(len > 2) diag("v[2]=`%s'", v[2]);
		strfreev(v);
	} skip_end;
	strfreev(ls);
}
END_TEST

DECLARE_TEST("block:loop", losetup_basic);

/* TODO: one day, hostcheck may be able to run in a little root environment
 * for itself. that day is not now, so the rest of loop.c will be restricted
 * to sneks. there's no reason it wouldn't work in hostcheck but only if run
 * as root, and then it may not clean itself up until reboot.
 */
#if defined(__sneks__)
static ino_t ino_of(const char *path) {
	struct stat st;
	int n = stat(path, &st);
	return n == 0 ? st.st_ino : -1;
}

static struct loopcfg **parse_list(char *const *lines)
{
	if(lines == NULL || lines[0] == NULL) return NULL;
	char **heads = strsplit(lines[0], ' ');
	for(int i=0; heads[i] != NULL; i++) {
		char *s = heads[i];
		while(*s) {
			if(*s == '-') *s = '_'; else *s = tolower(*s);
			s++;
		}
	}
	static const struct {
		const char *name;
		size_t offset;
		bool is_int;
	} fields[] = {
#define _I(n) { #n, offsetof(struct loopcfg, n), true }
#define _S(n) { #n, offsetof(struct loopcfg, n), false }
		_I(offset), _I(sizelimit), _I(autoclear), _I(ro), _I(dio), _I(log_sec),
		_S(name), _S(back_file),
#undef _I
#undef _S
	};
	darray(struct loopcfg *) result = darray_new();
	for(int i=1; lines[i] != NULL; i++) {
		char **vals = strsplit(lines[i], ' ');
		if(vals == NULL) continue;
		struct loopcfg *it = malloc(sizeof *it);
		*it = (struct loopcfg){ };
		for(int j=0; vals[j] != NULL && heads[j] != NULL; j++) {
			int hit = 0;
			while(hit < ARRAY_SIZE(fields) && !streq(heads[j], fields[hit].name)) hit++;
			if(hit < ARRAY_SIZE(fields)) {
				void *vp = (void *)it + fields[hit].offset;
				if(fields[hit].is_int) *(off_t *)vp = strtol(vals[j], NULL, 10); else *(char **)vp = strdup(vals[j]);
			} else {
				diag("unrecognized head `%s'", heads[j]);
			}
		}
		darray_append(result, it);
	}
	darray_append(result, NULL);
	return result.item;
}

static const char test_image_path[] = TESTDIR "/user/test/block/loop/test-image.bin";
#define TEST_IMAGE_SIZE (128 * 1024)

static bool loop_valid(const char *path)
{
	int fd = open(path, O_RDONLY), n, err;
	if(fd < 0) return false;
	/* TODO: fstat fd, confirm S_ISBLK() and minor=7 */
	char c; n = read(fd, &c, 1); err = n < 0 ? errno : 0;
	close(fd);
	errno = err;
	return n == 1 && c == '0';
}

/* attach image to device, examine default settings, detach */
START_LOOP_TEST(create_default, iter, 0, 3)
{
	const bool use_find = iter & 1, detach_all = iter & 2;
	diag("use_find=%s, detach_all=%s", btos(use_find), btos(detach_all));
	diag("test_image_path=`%s'", test_image_path);
	plan_tests(14);
	char **ls = run_losetup(0, "--show", use_find ? "-f" : "/dev/loop0", test_image_path, NULL), *devname = NULL;
	skip_start(!ok(losetup_rc == 0, "losetup ran [attach]"), 2, "losetup failed, rc=%d", losetup_rc) {
		imply_ok1(use_find, ls[0] != NULL && strstarts(ls[0], "/dev/loop"));
		imply_ok1(!use_find, ls[0] != NULL && streq(ls[0], "/dev/loop0"));
	} skip_end;
	devname = strdup(ls != NULL && ls[0] != NULL ? ls[0] : "");
	strfreev(ls);
	ls = run_losetup(0, "--list", devname, NULL);
	skip_start(!ok(losetup_rc == 0, "losetup ran [list]"), 7, "losetup failed, rc=%d", losetup_rc) {
		struct loopcfg **cfgs = parse_list(ls);
		skip_start(!ok(cfgs != NULL && cfgs[0] != NULL, "one config"), 6, "no config") {
			ok(cfgs[0]->name != NULL && streq(cfgs[0]->name, devname), "query name");
			ok(cfgs[0]->back_file != NULL && ino_of(cfgs[0]->back_file) == ino_of(test_image_path), "query back-file path");
			ok(cfgs[0]->offset == 0, "default offset");
			ok(cfgs[0]->sizelimit == 0, "default sizelimit");
			ok(cfgs[0]->log_sec == 512, "default log_sec");
			ok(cfgs[0]->autoclear == 0 && cfgs[0]->ro == 0 && cfgs[0]->dio == 0, "default misc");
		} skip_end;
		for(int i=0; cfgs != NULL && cfgs[i] != NULL; i++) {
			free(cfgs[i]->name); free(cfgs[i]->back_file); free(cfgs[i]);
		}
		free(cfgs);
	} skip_end;
	strfreev(ls);
	skip_start(devname == NULL || streq(devname, ""), 3, "no device") {
		if(!ok(loop_valid(devname), "valid")) diag("%s", strerror(errno));
		strfreev(detach_all ? run_losetup(0, "-D", NULL) : run_losetup(0, "-d", devname, NULL));
		if(!ok(losetup_rc == 0, "losetup ran [detach]")) diag("rc=%d", losetup_rc);
		ok(!loop_valid(devname), "not valid");
		free(devname);
	} skip_end;
}
END_TEST

DECLARE_TEST("block:loop", create_default);

/* configure a loopback device, access its data thru either the loopback
 * device or backing file using either read or mmap, then do the opposite,
 * then do the first thing again, and verify result consistency.
 * varying:
 *   - for both side A and B,
 *   - [use_map] use memory mapped access instead of read
 *   - [use_file] access file instead of block device
 */
START_LOOP_TEST(cross_access, iter, 0, 15)
{
	const bool use_map[2] = { iter & 1, iter & 2 }, use_file[2] = { iter & 4, iter & 8 };
	const size_t offset = 0x1011;
	diag("a{use_map=%s, use_file=%s}, b{use_map=%s, use_file=%s}", btos(use_map[0]), btos(use_file[0]), btos(use_map[1]), btos(use_file[1]));
	diag("offset=%zu", offset);
	plan_tests(9);
	/* TODO: this should be in a fixture for nicer cleanup... */
	char **ls = run_losetup(0, "--show", "-f", test_image_path, NULL);
	fail_unless(losetup_rc == 0, "losetup failed, rc=%d", losetup_rc);
	char *devname = strdupa(ls != NULL && ls[0] != NULL ? ls[0] : "");
	int fd[2];
	void *map[2] = { NULL, NULL };
	for(int i=0; i < 2; i++) {
		fd[i] = open(use_file[i] ? test_image_path : devname, O_RDONLY);
		skip_start(!ok(fd[i] >= 0, "open fd[i=%d]", i), 1, "didn't open") {
			map[i] = use_map[i] ? mmap(NULL, TEST_IMAGE_SIZE, PROT_READ, MAP_PRIVATE | MAP_FILE, fd[i], 0) : MAP_FAILED;
			if(!iff_ok1(use_map[i], map[i] != MAP_FAILED)) diag("%s", strerror(errno));
			if(use_map[i]) diag("map[%d]=%p", i, map[i]);
		} skip_end;
	}
	char a[15], b[sizeof a], c[sizeof a], *bufs[3] = { a, b, c };
	for(int t, i=0; i < 3; i++) {
		if(use_map[t = i % 2]) {
			if(map[t] != MAP_FAILED) memcpy(bufs[i], map[t] + offset, sizeof a);
			ok(map[t] != MAP_FAILED, "memcpy for t=%d", t);
		} else {
			lseek(fd[t], offset, SEEK_SET);
			ok(read(fd[t], bufs[i], sizeof a) == sizeof a, "read");
		}
	}
	ok(memcmp(a, b, sizeof a) == 0 && memcmp(b, c, sizeof b) == 0, "same data");
	if(!ok(memcmp(a, "0123456789abcdef0123" + offset % 16, 4) == 0, "data ok")) diag("data for a=`%.*s'", (int)sizeof a, a);
	for(int i=0; i < 2; i++) {
		if(map[i] != MAP_FAILED) munmap(map[i], TEST_IMAGE_SIZE);
		if(fd[i] >= 0) close(fd[i]);
	}
	strfreev(ls);
	strfreev(run_losetup(0, "-d", devname, NULL));
}
END_TEST

DECLARE_TEST("block:loop", cross_access);

/* set a squashfs image up as a loopback device, mount squashfs on that, and
 * access metadata and file content therein.
 */
START_TEST(mount_squashfs)
{
	/* copypasta'd from path/mount.c */
	const char *mountpoint = TESTDIR "/user/test/path/mount/sub",
		*image = TESTDIR "/user/test/path/mount/subfs.img",
		*notmount = TESTDIR "/user/test/path/mount/sub/not-mounted-yet";
	plan_tests(6);
	char **ls = run_losetup(0, "--show", "-f", image, NULL), *devname = ls != NULL && ls[0] != NULL ? strdup(ls[0]) : NULL;
	skip_start(!ok(losetup_rc == 0, "losetup attach"), 5, "losetup failed, rc=%d", losetup_rc) {
		diag("devname=`%s'", devname);
		ok(test_e(notmount), "pre-mount file seen");
		int n = mount(devname, mountpoint, "squashfs", 0, NULL);
		skip_start(!ok(n == 0, "mount"), 2, "mount failed: %s", strerror(errno)) {
			ok(stat(notmount, &(struct stat){ }) < 0 && errno == ENOENT, "pre-mount file not seen");
			char testpath[100]; snprintf(testpath, sizeof testpath, "%s/test-file", mountpoint);
			ok(test_e(testpath), "test file exists");
		} skip_end;
		if(!ok(umount(mountpoint) == 0, "umount")) diag("umount failed: %s", strerror(errno));
	} skip_end;
	strfreev(run_losetup(0, "-D", NULL));
}
END_TEST

DECLARE_TEST("block:loop", mount_squashfs);

#endif
