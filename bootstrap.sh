#!/bin/sh
set -e
$TORSOCKS git submodule update --init
for i in ext/*; do (cd $i; test -x bootstrap.sh && ./bootstrap.sh) & done; wait
